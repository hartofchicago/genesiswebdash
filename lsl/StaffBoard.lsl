string profilepicurl = "http://localhost/profile.php?api=lsl"; // address to the ZMW profile page
string loginuri = "localhost:8002"; // loginuri, same as whats in ur Opensim.ini or Robust.ini
string onlinetexture = "online"; // onlinetexture and offlinetexture can be customizable in the prim's inventory.
string offlinetexture = "offline"; // just please make sure you put your own Online Offline textures into the prim and named as they are here

string staffname; // First Last names.
string profilepic; // leave this empty, the system will detect it automaticly if the profile pic is in the same prim as this script

key staffkey;
key request;
key primkey;
key owner;
key http_getpic;

integer isonline = FALSE;
integer localchan;
integer textboxchan;
integer glisten;
integer tglisten;
integer profpicface = 1;
integer onlineface = 6;
integer istextbox = FALSE;

integer picwaiter = 30;
integer picping;
default
{
    on_rez(integer s)
    {
        llResetScript();
    }
    state_entry()
    {
        staffkey = (key)llGetObjectDesc();
        if (staffkey != "") {
            staffname = osKey2Name(staffkey);
            request = llRequestAgentData(staffkey, DATA_ONLINE);
            http_getpic = llHTTPRequest(profilepicurl+"&u="+staffname, [HTTP_METHOD, "GET"], "");
            llSetObjectName(staffname+" Staff Board");
        }else{
            llSetObjectName("Staff Board");
            llSetTexture(TEXTURE_BLANK, profpicface);
        }
        llSetTimerEvent(1.0);
        llSetTexture(offlinetexture, onlineface);
        owner = llGetOwner();
        primkey = llGetKey();
        localchan = (integer)("0x80000000"+llGetSubString((string)primkey,-8,-1));
        textboxchan = (integer)("0x80000000"+llGetSubString((string)primkey,-4,-1));
    }
    
    changed(integer change)
    {
        if (change & CHANGED_INVENTORY) {
            profilepic = llGetInventoryName(INVENTORY_TEXTURE, 0);
            llSetTexture(profilepic, 1);
        }
    }
    
    dataserver(key queryid, string data)
    {
        if (queryid == request)
        {
            if (data == "1") { // online
                llSetTexture(onlinetexture, onlineface);
                isonline = TRUE;
            }else if (data == "0") { // offline
                llSetTexture(offlinetexture, onlineface);
                isonline = FALSE;
            }
            llSetTimerEvent(60.0);
        }
    }

    http_response(key r, integer s, list m, string body)
    {
        if (r == http_getpic) {
            list apilist = llParseString2List(llUnescapeURL(body), ["="], []);
            profilepic = llList2String(apilist, 3);
            llSetTexture(profilepic, profpicface);
            llSetTimerEvent(1.0);
        }
    }
    
    touch_end(integer n)
    {
        integer touchedface = llDetectedTouchFace(0);
        key toucher = llDetectedKey(0);
        string touchername = llDetectedName(0);
        if (toucher == owner && touchedface == onlineface) {
            glisten = llListen(localchan, "", owner, "");
            string staffbutton;
            if (staffname != "") {
                staffbutton = "Del Staff";
            }else{
                staffbutton = "Add Staff";
            }
            llDialog(owner, "Hello "+llKey2Name(owner)+". How may I help you?", [staffbutton, "RESET", "EXIT"], localchan);
        }
        if (toucher == staffkey && toucher != owner && touchedface == onlineface) {
            glisten = llListen(localchan, "", staffkey, "");
            llDialog(staffkey, "Hello "+llKey2Name(staffkey)+". How may I help you?", ["RESET", "EXIT"], localchan);
        }
        if (toucher != owner && toucher != staffkey && touchedface == profpicface) {
            if (isonline) {
                string sim = llGetRegionName();
                vector touchedpos = llDetectedPos(0);
                integer px = llRound(touchedpos.x);
                integer py = llRound(touchedpos.y);
                integer pz = llRound(touchedpos.z);
                llInstantMessage(toucher, "Notifing "+staffname+". He/she will IM you as soon as possible.");
                llInstantMessage(staffkey, touchername+" is requesting for your assistance from hop://"+loginuri+"/"+sim+"/"+(string)px+"/"+(string)py+"/"+(string)px);
            }else{
                llInstantMessage(toucher, staffname+" is not in world at this time. Please check back later.");
            }
        }
    }

    listen(integer chan, string name, key id, string msg)
    {
        if (chan == localchan && id == staffkey && id != owner) {
            llListenRemove(glisten);
            if (msg == "RESET") {
                llResetScript();
            }else if (msg == "EXIT") {
                // do nothing.
            }
        }
        if (chan == textboxchan && id == owner && istextbox == TRUE) {
            staffname = msg;
            list namelist = llParseString2List(llUnescapeURL(msg), [" "], []);
            string first = llList2String(namelist, 0);
            string last = llList2String(namelist, 1);
            staffkey = osAvatarName2Key(first, last);
            http_getpic = llHTTPRequest(profilepicurl+"&u="+staffname, [HTTP_METHOD, "GET"], "");
            llSetObjectDesc((string)staffkey);
            llSetObjectName(staffname+" Staff Board");
            llListenRemove(tglisten);
            llSetTimerEvent(1.0);
            istextbox = FALSE;
        }
        if (chan == localchan && id == owner && istextbox == FALSE) {
            llListenRemove(glisten);
            if (msg == "EXIT") {
                // do nothing and just kill the listen
            }else if (msg == "RESET") {
                llResetScript();
            }else if (msg == "Add Staff") {
                tglisten = llListen(textboxchan, "", owner, "");
                llTextBox(owner, "Please enter the first and last name of the avatar you like on this board", textboxchan);
                istextbox = TRUE;
            }else if (msg == "Del Staff") {
                llSetTexture(TEXTURE_BLANK, profpicface);
                llSetTimerEvent(0.0);
                llSetObjectName("Staff Board");
                llSetObjectDesc("");
                staffkey = "";
                staffname = "";
                picping = 0;
            }
        }
    }
    
    timer()
    {
        request = llRequestAgentData(staffkey, DATA_ONLINE);
        picping++;
        if (picping == picwaiter) {
            http_getpic = llHTTPRequest(profilepicurl+"&u="+staffname, [HTTP_METHOD, "GET"], "");
            picping = 0;
        }
    }
}