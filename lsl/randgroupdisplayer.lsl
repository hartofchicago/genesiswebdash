string url = "http://localhost/api/grouplist.php?api=lsl";
integer signface = 1; // face where you like the texture of the group to display on
key httper;
key groupid = NULL_KEY;
string groupname = "";
default
{
	on_rez(integer s)
	{
		llResetScript();
	}
	state_entry()
	{
		httper = llHTTPRequest(url, [HTTP_METHOD, "GET"], "");
        llSetTimerEvent(300.0); // refreshes every 5 minutes to reduce lag. 5 minutes = 300.0 sec
	}
	touch_end(integer n)
    {
    	key toucher = llDetectedKey(0);
		llInstantMessage(toucher, "Join secondlife:///app/group/" + (string)groupid + "/about");
    }
    http_response(key request_id, integer status, list metadata, string body) {
        if (request_id == httper) {
            list apilist = llParseString2List(llUnescapeURL(body), ["="], []);
            string uuid = llList2String(apilist, 0);
            string name = llList2String(apilist, 1);
            if (uuid != "GroupID" && name != "Empty") {
            	groupid = (key)uuid;
            	groupname = name;
            	string texture = llList2String(apilist, 2);
	            llSetTexture(texture, signface);
	            llSetObjectName(groupname+" - Group Displayer");
            }
        }
    }
    timer() {
        httper = llHTTPRequest(url, [HTTP_METHOD, "GET"], "");
    }
}