string url = "http://localhost/ossl/hipchat.php?";
float timetorefresh = 300.0; // refreshes every 5 minutes to reduce lag. 5 minutes = 300.0 sec
string supporturl;
key httperroom;
key httpermsg;
integer onlineindicatorface = 1; // face where you like the colour to change when someone is available in chat
integer isroompublic = FALSE;
integer issupporton = FALSE;
default
{
	on_rez(integer s)
	{
		llResetScript();
	}
	state_entry()
	{
        llSetTimerEvent(1.0);
    }
    touch_end(integer n)
    {
    	key toucher = llDetectedKey(0);
    	if (toucher == llGetOwner()) {
    		llOwnerSay("Reseting...");
    		llResetScript();
    	}else if (toucher != llGetOwner()) {
    		if (isroompublic) {
                if (issupporton) {
                    string touchername = llKey2Name(toucher);
                    llLoadURL(toucher, "Please visit our live support chat by visiting this link\n"+supporturl+"\nPowered by HipChat.", supporturl+"name="+touchername);
                }else{
                    httpermsg = llHTTPRequest(url+"t=supportrequest&uuid="+(string)toucher+"&sim="+llGetRegionName(), [HTTP_METHOD, "GET"], "");
                    llInstantMessage(toucher, "Sorry! No support staff is currently in the room. Please check back later.");
                }
			}else{
				llInstantMessage(toucher, "Sorry but the HipChat chatroom is currently not public to guests. Please inform the grid's admins.");
			}
    	}
    }
    http_response(key request_id, integer status, list metadata, string body) {
        if (request_id == httperroom) {
        	if (body == "Room Not Public") {
        		llSetColor(<1.0,0.0,0.0>, onlineindicatorface);
        		isroompublic = FALSE;
        	}else{
        		isroompublic = TRUE;
	            list apilist = llParseString2List(llUnescapeURL(body), ["="], []);
	            supporturl = llList2String(apilist, 0)+"?";
	            issupporton = llList2Integer(apilist, 1);
	            if (issupporton) {
	            	// support staff is currently in the room
	            	llSetColor(<0.0,1.0,0.0>, onlineindicatorface);
	            }else{
	            	// support staff is not in the room
	            	llSetColor(<1.0,0.0,0.0>, onlineindicatorface);
	            }
	        }
        }
        if (request_id == httpermsg) {
            list apilist = llParseString2List(llUnescapeURL(body), ["="], []);
            string toucherid = llList2String(apilist, 0);
            string msg = llList2String(apilist, 1);
            llInstantMessage((key)toucherid, msg);
        }
    }
    timer() {
        httperroom = llHTTPRequest(url+"t=fetch", [HTTP_METHOD, "GET"], "");
        llSetTimerEvent(timetorefresh);
    }
}