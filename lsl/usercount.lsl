string url = "http://localhost/api/usercount.php?api=lsl";
key httper;
default
{
    on_rez(integer s) {
        llResetScript();
    }
    state_entry() {
        httper = llHTTPRequest(url, [HTTP_METHOD, "GET"], "");
        llSetTimerEvent(300.0); // refreshes every 5 minutes to reduce lag. 5 minutes = 300.0 sec
    }
    http_response(key request_id, integer status, list metadata, string body) {
        if (request_id == httper) {
            list apilist = llParseString2List(llUnescapeURL(body), ["="], []);
            string apimsg0 = llList2String(apilist, 0);
            string apimsg1 = llList2String(apilist, 1);
            string apimsg2 = llList2String(apilist, 2);
            string apimsg3 = llList2String(apilist, 3);
            string text = "User Count\n\nUsers Online:"+apimsg0+"\nHG Visitors:"+apimsg1+"\nRegistered Users:"+apimsg2+"\nMost Users On:"+apimsg3+"\n\nRefreshes Every 5 Minutes";
            string CommandList = "";
            CommandList = osMovePen(CommandList, 10, 10);
            CommandList = osDrawText(CommandList, text);
            osSetDynamicTextureData("", "vector", CommandList, "width:256,height:256", 0);
        }
    }
    timer() {
        httper = llHTTPRequest(url, [HTTP_METHOD, "GET"], "");
    }
}