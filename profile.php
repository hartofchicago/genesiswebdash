<?php
$page_title = "News";
$hide_sidebars = true;
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');

$u = $zw->Security->make_safe($_GET['u']);
$uuid = $zw->Security->make_safe($_GET['uuid']);
$issearch = $zw->Security->make_safe($_GET['issearch']);

if ($u) {
	$u = $u;
	$u = str_replace(" ", ".", $u);
	$u = str_replace("%20", ".", $u);
	$uexplode = explode(".", $u);
	$firstname = $uexplode[0];
	$lastname = $uexplode[1];
	if (!$lastname) {
		$lastname = "Resident";
	}
	$uq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE FirstName = '$firstname' AND LastName = '$lastname'");
}else if ($uuid) {
	$uq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$uuid'");
}

$ur = $zw->SQL->fetch_array($uq);
if (!$uuid) {
	$uuid = $ur['PrincipalID'];
}else{
	$firstname = $ur['FirstName'];
	$lastname = $ur['LastName'];
	if (!$lastname) {
		$lastname = "Resident";
	}
}

$reztime = $ur['Created'];
$usertitle = $ur['UserTitle'];
$rezday = $zw->site->time2date($reztime);

if (!$usertitle) {
	$usertitle = "Resident";
}

$q = $zw->SQL->query("SELECT * FROM `{$zw->config['profile_db']}`.userprofile WHERE useruuid = '$uuid'");
$r = $zw->SQL->fetch_array($q);

$partner = $r['profilePartner'];
$url = $r['profileURL'];
$want2mask = $r['profileWantToMask'];
$want2text = $r['profileWantToText'];
$skillsmask = $r['profileSkillsMask'];
$skillstext = $r['profileSkillsText'];
$lang = $r['profileLanguages'];
$flpic = $r['profileImage'];
$fakeaboutme = $r['profileAboutText'];
$rlpic = $r['profileFirstImage'];
$realaboutme = $r['profileFirstText'];
$profileallowpublish = $r['profileAllowPublish'];

if ($profileallowpublish == "1") {
	echo "This user does not want their profile to be viewable to the public.";
	exit;
}else if ($profileallowpublish == "0") {
	// do nothing and allow the script to continue to be processed
}

if ($lastname == "Resident") {
	$displayname = $firstname;
}else{
	$displayname = $firstname." ".$lastname;
}

if ($user_uuid) {
	$approvalq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.Friends WHERE PrincipalID = '$user_uuid' AND Friend = '$uuid'");
	$approvaln = $zw->SQL->num_rows($approvalq);
	if ($approvaln) {
		$friendbutton = "";
	}else{
		$friendbutton = "<form method='post' action='friendslist.php' class='form' role='form'>
		<input type='hidden' name='friend' value='".$uuid."'>
		<input type='submit' name='addrequest' value='Add Friend' class='btn btn-success'>
		</form>";
	}
}else{
	$friendbutton = "";
}

$online = $zw->grid->online($uuid);
if ($online) {
$onoff = "onlinedot.png";
}else{
$onoff = "offlinedot.png";
}
$isOnline = "<img src='".$site_address."/img/".$onoff."' border='0'>";

if (!$partner || $partner == $nullkey) {
	$partnername = "None";
}else{
	$partnerq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$partner'");
	$partnerr = $zw->SQL->fetch_array($partnerq);
	$partnerFirstName = $partnerr['FirstName'];
	$partnerLastName = $partnerr['LastName'];
	if ($partnerLastName == "Resident") {
		$pname = $partnerFirstName;
		$pdname = $partnerFirstName;
	}else{
		$pname = $partnerFirstName.".".$partnerLastName;
		$pdname = $partnerFirstName." ".$partnerLastName;
	}
	$partnername = "<a href='".$site_address."/profile.php?u=".$pname."'>".$pdname."</a>";
}

if (!$flpic || $flpic == $nullkey) {
$picflpic = "<a href='' data-toggle='modal' data-target='#flpicmodal'><img src='".$ip2webassets."/asset.php?id=".$notextureuuid."&format=JPG' class='img-responsive' width='100' height='100'></a>";
$mpicflpic = "<img src='".$ip2webassets."/asset.php?id=".$notextureuuid."&format=JPG' class='img-responsive' width='512' height='512'>";
}else{
$picflpic = "<a href='' data-toggle='modal' data-target='#flpicmodal'><img src='".$ip2webassets."/asset.php?id=".$flpic."&format=JPG' class='img-responsive' width='100' height='100'></a>";
$mpicflpic = "<img src='".$ip2webassets."/asset.php?id=".$flpic."&format=JPG' class='img-responsive' width='512' height='512'>";
}

if (!$rlpic || $rlpic == $nullkey) {
$picrlpic = "<a href='' data-toggle='modal' data-target='#rlpicmodal'><img src='".$ip2webassets."/asset.php?id=".$notextureuuid."&format=JPG' class='img-responsive' width='100' height='100'></a>";
$mpicrlpic = "<img src='".$ip2webassets."/asset.php?id=".$notextureuuid."&format=JPG' class='img-responsive' width='512' height='512'>";
}else{
$picrlpic = "<a href='' data-toggle='modal' data-target='#rlpicmodal'><img src='".$ip2webassets."/asset.php?id=".$rlpic."&format=JPG' class='img-responsive' width='100' height='100'></a>";
$mpicrlpic = "<img src='".$ip2webassets."/asset.php?id=".$rlpic."&format=JPG' class='img-responsive' width='512' height='512'>";
}

$about = "<B>Title</B><br>".$usertitle."<hr>
<B>Biography</B><br>".$fakeaboutme."<hr>
<B>Partner</B><br>".$partnername."<hr>
<B>".$gridname." Birthday</B><br>".$rezday."<hr>
<B>Real Life Picture</B><br>".$picrlpic."<hr>
<B>Real Life</B><br>".$realaboutme;

	$displaypicks = "";
	$pickq = $zw->SQL->query("SELECT * FROM `{$zw->config['profile_db']}`.userpicks WHERE creatoruuid = '$uuid' ORDER BY 'sortorder' ASC LIMIT 0,100");
	$pickn = $zw->SQL->num_rows($pickq);
	if ($pickn) {
		while ($pickr = $zw->SQL->fetch_array($pickq)) {
			$pickuuid = $pickr['pickuuid'];
			$toppick = $pickr['toppick'];
			$parceluuid = $pickr['parceluuid'];
			$pickname = $pickr['name'];
			$pickinfo = $pickr['description'];
			$pickpic = $pickr['snapshotuuid'];
			if (!$pickpic || $pickpic == $nullkey) {
				$pickpic = "<img src='".$ip2webassets."/asset.php?id=".$notextureuuid."&format=JPG' class='img-responsive pull-right' width='100' height='100'>";
			}else{
				$pickpic = "<img src='".$ip2webassets."/asset.php?id=".$pickpic."&format=JPG' class='img-responsive pull-right' width='100' height='100'>";
			}
			$displaypicks .= "<div class='row'><div class='col-xs-6 col-sm-3 col-md-2'>".$pickpic."</div><div class='col-xs-12 col-sm-9 col-md-10'><B>".$pickname."</B><br>".$pickinfo."</div></div><hr>";
		}
	}else{
		$displaypicks = $displayname." currently has no picks";
	}

	$displaygroups = "";
	$groupmq = $zw->SQL->query("SELECT * FROM `{$zw->config['group_db']}`.os_groups_membership WHERE PrincipalID = '$uuid' AND ListInProfile = '1' ORDER BY 'Contribution' ASC LIMIT 0,100");
	$groupmn = $zw->SQL->num_rows($groupmq);
	if ($groupmn) {
		while ($groupmr = $zw->SQL->fetch_array($groupmq)) {
			$gid = $groupmr['GroupID'];
			$groupgq = $zw->SQL->query("SELECT * FROM `{$zw->config['group_db']}`.os_groups_groups WHERE GroupID = '$gid'");
			$groupgr = $zw->SQL->fetch_array($groupgq);
			$groupname = $groupgr['Name'];
			$groupinfo = $groupgr['Charter'];
			$grouppic = $groupgr['InsigniaID'];
			$showinlist = $groupgr['ShowInList'];
			if ($showinlist == "1") {
				if (!$grouppic || $grouppic == $nullkey) {
					$grouppic = "<img src='".$ip2webassets."/asset.php?id=".$notextureuuid."&format=JPG' class='img-responsive pull-right' width='100' height='100'>";
				}else{
					$grouppic = "<img src='".$ip2webassets."/asset.php?id=".$grouppic."&format=JPG' class='img-responsive pull-right' width='100' height='100'>";
				}
				$displaygroups .= "<div class='row'><div class='col-xs-6 col-sm-3 col-md-2'>".$grouppic."</div><div class='col-xs-12 col-sm-9 col-md-10'><a href='secondlife:///app/group/".$gid."/about'><B>".$groupname."</B></a><br>".$groupinfo."</div></div><hr>";
			}else if ($showinlist == "0") {
				$displaygroups .= "";
			}
		}
	}else{
		$displaygroups = $displayname." does not currently belong to any groups";
	}

	$displayclassifieds = "";
	$classifiedsq = $zw->SQL->query("SELECT * FROM `{$zw->config['profile_db']}`.classifieds WHERE creatoruuid = '$uuid' ORDER BY 'creationdate' ASC LIMIT 0,100");
	$classifiedsn = $zw->SQL->num_rows($classifiedsq);
	if ($classifiedsn) {
		while ($classifiedsr = $zw->SQL->fetch_array($classifiedsq)) {
			$cname = $classifiedsr['name'];
			$cdesc = $classifiedsr['description'];
			$csnap = $classifiedsr['snapshotuuid'];
			if (!$csnap || $csnap == $nullkey) {
				$csnap = "<img src='".$ip2webassets."/asset.php?id=".$notextureuuid."&format=JPG' class='img-responsive pull-right' width='100' height='100'>";
			}else{
				$csnap = "<img src='".$ip2webassets."/asset.php?id=".$csnap."&format=JPG' class='img-responsive pull-right' width='100' height='100'>";
			}
			$displayclassifieds .= "<div class='row'><div class='col-xs-6 col-sm-3 col-md-2'>".$csnap."</div><div class='col-xs-12 col-sm-9 col-md-10'><B>".$cname."</B><br>".$cdesc."</div></div><hr>";
		}
	}else{
		$displayclassifieds = $displayname." have not posted any classifieds";
	}

if (!$api) {
	if ($lastname == "Resident") {
		$smallname = $firstname;
	}else{
		$smallname = $firstname.".".$lastname;
	}
?>
<style>
#header
{
  color: #ffffff;
  font-size: 20px;
  background-color: #404040;
}
</style>
</head>
<body>
<div class="container-fluid">

	<div class="row" id="header">
		<div class="col-xs-18  col-md-12">

			<span class="pull-left"><?php echo $picflpic; ?></span>
  			<B><?php echo $displayname; ?></B> <?php echo $isOnline; ?><br>
  			<small><?php echo $smallname; ?></small><br>
  			 <?php
  			 	if ($uuid != $user_uuid) {
  			 		echo "<a href='secondlife:///app/agent/".$uuid."/about' class='btn btn-success'>In World Profile</a>";
  			 		echo $friendbutton;
  			 	}else if ($uuid == $user_uuid) {
  			 		echo "<a href='".$site_address."/usersettings.php' class='btn btn-success'>Edit Profile</a>";
  			 	}
  			 	if ($issearch == "true") {
  			 		echo "<a href='javascript:history.back()' class='btn btn-success'>Back to search</a>";
  			 	}
  			 ?>
  		</div>
	</div>
	<div class="row">
		<div class="col-xs-18 col-md-12">
			<ul class="nav nav-pills nav-justified">
	  			<li class="active"><a href="#about" data-toggle="tab">About</a></li>
	  			<li><a href="#picks" data-toggle="tab">Picks</a></li>
	  			<li><a href="#groups" data-toggle="tab">Groups</a></li>
	  			<li><a href="#classifieds" data-toggle="tab">Classifieds</a></li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-18 col-md-12">
			<div class="tab-content">
	  			<div class="tab-pane active" id="about">
	  				<?php echo $about; ?>
	  			</div>
	  			<div class="tab-pane" id="picks">
	  				<?php echo $displaypicks; ?>
	  			</div>
	  			<div class="tab-pane" id="groups">
	  				<?php echo $displaygroups; ?>
	  			</div>
	  			<div class="tab-pane" id="classifieds">
	  				<?php echo $displayclassifieds; ?>
	  			</div>
			</div>
		</div>
	</div>

</div>

<!-- Fake Pic Modal -->
<div class="modal fade" id="flpicmodal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $zw->config['GridName']; ?>LifePicModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $zw->config['GridName']; ?> Life Picture</h4>
      </div>
      <div class="modal-body">
      	<p>
        <a data-dismiss="modal"><?php echo $mpicflpic; ?></a>
    	</p>
        <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Real Pic Modal -->
<div class="modal fade" id="rlpicmodal" tabindex="-1" role="dialog" aria-labelledby="RealLifePicModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Real Life Picture</h4>
      </div>
      <div class="modal-body">
      	<p>
        <a data-dismiss="modal"><?php echo $mpicrlpic; ?></a>
        </p>
        <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php
}else if ($api) {

$apiarray = array("FirstName" => $firstname,
	"LastName" => $lastname,
	"UUID" => $uuid,
	"ProfilePicture" => $flpic,
	"AboutMe" => $fakeaboutme,
	"Partner" => $partner,
	"URL" => $url,
	"RealLifePicture" => $rlpic,
	"RLAboutMe" => $realaboutme);

	$api = $_GET['api'];
	if ($api == "xmlrpc") {
		echo xmlrpc_encode($apiarray);
	}else if ($api == "json") {
		echo json_encode($apiarray);
	}else if ($api == "lsl") {
		echo $firstname."=".$lastname."=".$uuid."=".$flpic."=".$fakeaboutme."=".$partner."=".$url."=".$rlpic."=".$realaboutme;
	}
}

include ('inc/footer.php');
?>