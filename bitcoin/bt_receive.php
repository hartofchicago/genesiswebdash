<?php
$page_title = "Buy Money";
define('ZW_IN_SYSTEM', true);
require_once('../inc/header.php');
$BitCoinAddress = $zw->config['BitCoinAddress'];
$moneyip = $zw->config['MoneyIP'];
$amount = $zw->Security->make_safe($_POST['amount']);
if ($user_uuid && $BitCoinAddress && $moneyip && $amount) {
	$converter = $zw->config['BTC2Currency'] / 100000000;
	$btcamount = $amount / $converter;
	$invoice_id = $zw->site->randcode("20");;
	$secret = $zw->site->randcode("20");
	$my_address = $BitCoinAddress;
	$my_callback_url = $site_address.'/bitcoin/bt_receive_callback.php?invoice_id='.$invoice_id.'&secret='.$secret;
	$root_url = 'https://blockchain.info/api/receive';
	$parameters = 'method=create&address=' . $my_address .'&callback='. urlencode($my_callback_url);
	$response = file_get_contents($root_url . '?' . $parameters);
	$receivedaddress = json_decode(array('input_address' => $response->input_address));
	echo 'Please Send Payment To : '.$receivedaddress.' in the BTC amount of '.$btcamount;
	echo "<p>";
	$qrbtclabel = "BTC2".$zw->config['GridMoney'];
	echo $zw->api->qr("http://www.btcfrog.com/qr/bitcoinPNG.php?address=".$my_address."&label=".$qrbtclabel."&amount=".$btcamount);
	$zw->SQL->query("INSERT INTO invoice_payments (invoice_id, user_uuid, secret) VALUES ('$invoice_id', '$user_uuid', '$secret')");
}else{
	echo $zw->site->displayalert("ERROR:<br>
	1. You may not be logged in.<br>
	2. No bitcoin address set.<br>
	3. This grid does not have a money module installed yet or have not set its IP in the ZMW settings.<br>
	4. The amount you entered was empty.", "danger");
}
include ('../inc/footer.php');
?>