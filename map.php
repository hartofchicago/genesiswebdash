<?php
$page_title = "Grid Map";
$hide_sidebars = true;
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');
?>
<iframe src='<?php echo $zw->config['WebMap']; ?>' frameborder='0' name='gridmap' width="100%" height="600px" scrolling='no' seamless>
If you can see this instead of the map then you really need to get a new computer that can handle a better browser like Google Chrome.
</iframe>
<?php
include ('inc/footer.php');
?>