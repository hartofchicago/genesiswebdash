<?php
define('ZW_IN_SYSTEM', true);
require_once('inc/headerless.php');
$search = $zw->Security->make_safe($_GET['search']);
$totalrecords = $zw->Security->make_safe($_GET['totalrecords']);
$offset = $zw->Security->make_safe($_GET['offset']);
$type = $zw->Security->make_safe($_GET['type']);
$m = $zw->Security->make_safe($_GET['m']);

if ($totalrecords) {
  $totalrecords = intval($totalrecords);
}else{
  $totalrecords = 25;
}

if ($search) {
  $search = $search;
  $search = strtoupper($search);
  $search = strip_tags($search);
  $search = trim ($search);
  $searchlink = $search;
}

if ($offset) {
  $offset = intval($offset) * $totalrecords;
  $button_offset = $offset + 1;
}else{
  $offset = 1;
  $button_offset = "1";
}

if (!$type) {
  $type = "";
}

if (!$m) {
  $m = "3";
}

if ($search) {
  $placeholder = "Searching for $search";
}else if (!$search) {
  $placeholder = "Search";
}

$select = "selected";

if ($m == "1") {
  $wordm = "PG";
}else if ($m == "2") {
  $wordm = "Mature";
}else if ($m == "3") {
  $wordm = "Adult";
}

$PG = "<span class='label label-success'>PG</span>";
$MATURE = "<span class='label label-default'>M</span>";
$ADULT = "<span class='label label-danger'>A</span>";

echo "<div class='panel-group' id='accordion' style='width:auto; height: auto;'>";
if ($type == "classifieds" || !$type) {
$cq = $zw->SQL->query("SELECT * FROM `{$zw->config['profile_db']}`.classifieds WHERE name LIKE '%$search%' OR description LIKE '%$search%' AND creationdate < '$now' AND expirationdate > '$now' AND classifiedflags < '$m' ORDER BY 'creationdate' DESC LIMIT $offset, $totalrecords");
while ($cn = $zw->SQL->fetch_array($cq)) {
$creatoruuid = $cn['creatoruuid'];
$creationdate = $cn['creationdate'];
$expirationdate = $cn['expirationdate'];
$category = $cn['category'];
$name = $cn['name'];
$parceluuid = $cn['parceluuid'];
$description = $cn['description'];
$snapshotuuid = $cn['snapshotuuid'];
$simname = $cn['simname'];
$classifiedflags = $cn['classifiedflags'];
$sim = $cn['simname'];
$parcelname = $cn['parcelname'];

$creationdate = $zw->site->time2date($creationdate);
$expirationdate = $zw->site->time2date($expirationdate);

$parq = $zw->SQL->query("SELECT * FROM `{$zw->config['search_db']}`.allparcels WHERE parcelUUID = '$parceluuid'");
$parrow = $zw->SQL->fetch_array($parq);
$regionid = $parrow['regionUUID'];
$loc = $parrow['landingpoint'];

if (!$loc) {
  $loc = "128/128/25";
	$locr = "128,128,25";
}else{
	$locr = str_replace("/", ",", $loc);
}

$FL = $zw->grid->getosuser_by_uuid($creatoruuid);
$FName = $FL['FirstName'];
$LName = $FL['LastName'];

if ($classifiedflags == 0) {
$classifiedflags = $PG;
}else if ($classifiedflags == 1) {
$classifiedflags = $MATURE;
}else if ($classifiedflags == 2) {
$classifiedflags = $ADULT;
}

if (!$snapshotuuid) {
$pic = "<img src='".$ip2webassets."/asset.php?id=00000000-0000-0000-0000-000000000000' class='pull-right' width='75' height='75'>";
}else{
$pic = "<img src='".$ip2webassets."/asset.php?id=".$snapshotuuid."' class='pull-right' width='75' height='75'>";
}

echo "  <div class='panel panel-default'>
    <div class='panel-heading'>
      <h4 class='panel-title'>
      <a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion' href='#".$parceluuid."'>
        <B>".$name."</B>
      </a>
      </h4>
    </div>
    <div id='".$parceluuid."' class='panel-collapse collapse'>
      <div class='panel-body'>
".$pic."
<p>
<a href='secondlife://".$sim."/".$loc."/'>".$parcelname.", ".$sim." (".$locr.")</a><br>
".$description."
<br>
<small>Created: ".$creationdate." by ".$FName." ".$LName."</small>
</p>
      </div>
    </div>
  </div>
";
}

}
if ($type == "destinations" || !$type) {
$popq = $zw->SQL->query("SELECT * FROM `{$zw->config['search_db']}`.popularplaces WHERE name LIKE '%$search%' AND mature = '$wordm' ORDER BY `dwell` DESC LIMIT $offset, $totalrecords");
$popn = $zw->SQL->num_rows($popq);
if ($popn) {
    while ($popn = $zw->SQL->fetch_array($popq)) {
    $parcelUUID = $popn['parcelUUID'];
    $name = $popn['name'];
    $mature = $popn['mature'];

    if ($mature == 0) {
    $mature = $PG;
    }else if ($mature == 1) {
    $mature = $MATURE;
    }else if ($mature == 2) {
    $mature = $ADULT;
    }

    $parq = $zw->SQL->query("SELECT * FROM `{$zw->config['search_db']}`.parcels WHERE parcelUUID = '$parcelUUID'");
    $parrow = $zw->SQL->fetch_array($parq);
    $reguuid = $parrow['regionUUID'];
    $landing = $parrow['landingpoint'];
    $desc = $parrow['description'];

    $simname = $zw->grid->regionname($reguuid);
    $usersonregion = $zw->grid->userspersim($reguuid);

    echo "  <div class='panel panel-default'>
        <div class='panel-heading'>
          <h4 class='panel-title'>
          <a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion' href='#Dest".$parcelUUID."'>
    	<B>".$name."</B>
          </a>
          </h4>
        </div>
        <div id='Dest".$parcelUUID."' class='panel-collapse collapse'>
          <div class='panel-body'>
    ".$desc."<br>
    <B>Mature Rating:</B> ".$mature."<br>
    <B>Avatars on region:</B> ".$usersonregion."
    <br><a href='secondlife://".$simname."/".$landing."/'>Teleport</a>
         </div>
        </div>
      </div>";

    }

  }else{
  }
}
if ($type == "events" || !$type) {
$evq = $zw->SQL->query("SELECT * FROM `{$zw->config['search_db']}`.events WHERE name LIKE '%$search%' OR description LIKE '%$search%' AND eventflags < '$m' ORDER BY `dateUTC` LIMIT $offset, $totalrecords");
$evn = $zw->SQL->num_rows($evq);
if ($evn) {
    while ($evnum = $zw->SQL->fetch_array($evq)) {
    $creator = $evnum['creatoruuid'];
    $time = $evnum['dateUTC'];
    $eventid = $evnum['eventid'];
    $eventname = $evnum['name'];
    $eventsim = $evnum['simname'];
    $eventlocation = $evnum['globalPos'];
    $eventinfo = $evnum['description'];
    $event_type = $evnum['eventflags'];

    $event_time = date("M d Y h:i a T",$time);

    $getname = $zw->grid->getosuser_by_uuid($creator);
    $first = $getname['FirstName'];
    $last = $getname['LastName'];
    $event_host = "$first $last";

    if ($event_type == 0) {
    $event_type = $PG;
    }else if ($event_type == 1) {
    $event_type = $MATURE;
    }else if ($event_type == 2) {
    $event_type = $ADULT;
    }

    $loc = str_replace("<", "", $eventlocation);
    $loc = str_replace(">", "", $loc);
    $loclink = str_replace(",", "/", $loc);

      if ($time >= $now) {
      echo "  <div class='panel panel-default'>
          <div class='panel-heading'>
            <h4 class='panel-title'>
            <a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion' href='#".$eventid."'>
      <B>".$eventname." - ".$event_time."</B> ".$event_type."
            </a>
      	</h4>
          </div>
          <div id='".$eventid."' class='panel-collapse collapse'>
            <div class='panel-body'>
      ".$eventinfo."<br>
      <a href='secondlife://".$eventsim."/".$loclink."/'>".$eventsim."(".$loc.")</a>
            </div>
          </div>
        </div>
      ";
      }else if ($time <= $now) {
      // dont display anything
      }

    }
 }else{
 }
}
if ($type == "groups" || !$type) {
  // Edit below. os_groups_groups for V1 osgroup for V2
$grpq = $zw->SQL->query("SELECT * FROM `{$zw->config['group_db']}`.osgroup WHERE Name LIKE '%$search%' AND ShowInList = '1' ORDER BY `Name` ASC LIMIT $offset, $totalrecords");
while ($grpn = $zw->SQL->fetch_array($grpq)) {
$GroupID = $grpn['GroupID'];
$Name = $grpn['Name'];
$Charter = $grpn['Charter'];
$InsigniaID = $grpn['InsigniaID'];
$FounderID = $grpn['FounderID'];
$OpenEnrollment = $grpn['OpenEnrollment'];
$MembershipFee = $grpn['MembershipFee'];
$MaturePublish = $grpn['MaturePublish'];

$FL = $zw->grid->getosuser_by_uuid($FounderID);
$FName = $FL['FirstName'];
$LName = $FL['LastName'];

// V1 is os_groups_membership while V2 is osgroupmembership
$gmq = $zw->SQL->query("SELECT * FROM `{$zw->config['group_db']}`.osgroupmembership WHERE GroupID = '$GroupID'");
$gmcount = $zw->SQL->num_rows($gmq);

if (!$InsigniaID) {
$pic = "<img src='".$ip2webassets."/asset.php?id=00000000-0000-0000-0000-000000000000' class='pull-right' width='75' height='75'>";
}else{
$pic = "<img src='".$ip2webassets."/asset.php?id=".$InsigniaID."' class='pull-right' width='75' height='75'>";
}

if ($OpenEnrollment == "1") {
$join = "<a href='secondlife:///app/group/".$GroupID."/about' class='btn btn-success btn-xs'>JOIN</a>";
}else if ($OpenEnrollment == "0") {
$join = "Closed to invites only.";
}

if ($MaturePublish == 0) {
$MaturePublish = $PG;
}else if ($MaturePublish == 1) {
$MaturePublish = $MATURE;
}else if ($MaturePublish == 2) {
$MaturePublish = $ADULT;
}

echo "  <div class='panel panel-default'>
    <div class='panel-heading'>
      <h4 class='panel-title'>
      <a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion' href='#".$GroupID."'>
<B>".$Name."</B> ".$MaturePublish."
      </a>
	</h4>
    </div>
    <div id='$GroupID' class='panel-collapse collapse'>
      <div class='panel-body'>
      <p>
        ".$pic."
        ".$Charter."
      </p>
      <p>
        Total Members: ".$gmcount."
      </p>
      <p>
        Enrollment: ".$join."
      </p>
      <p>
        <small>Created by ".$FName." ".$LName."</small>
      </p>
      </div>
    </div>
  </div>
";
}

}
if ($type == "4sale" || !$type) {
  $forsaleq = $zw->SQL->query("SELECT * FROM `{$zw->config['search_db']}`.parcelsales WHERE parcelname LIKE '%$search%' ORDER BY `saleprice` ASC LIMIT $offset, $totalrecords");
  while ($forsaler = $zw->SQL->fetch_array($forsaleq)) {
    $regionUUID = $forsaler['regionUUID'];
    $parcelname = $forsaler['parcelname'];
    $parcelUUID = $forsaler['parcelUUID'];
    $area = $forsaler['area'];
    $saleprice = $forsaler['saleprice'];
    $landingpoint = $forsaler['landingpoint'];

    $regionname = $zw->grid->regionname($regionUUID);

    echo "  <div class='panel panel-default'>
        <div class='panel-heading'>
          <h4 class='panel-title'>
          <a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion' href='#forsale".$parcelUUID."'>
    <B>".$parcelname."</B>
          </a>
    	</h4>
        </div>
        <div id='forsale".$parcelUUID."' class='accordion-body collapse'>
          <div class='panel-body'>
    <p>
    <B>Area:</B> ".$area."<br>
    <B>Price:</B> Z$ ".$saleprice."<br>
    <B>Region:</B> ".$regionname."<br>
    <a href='secondlife://".$regionname."/".$landingpoint."/'>Teleport</a>
    </p>
          </div>
        </div>
      </div>
    ";
  }
}
if ($type == "people" || !$type) {
  if ($search) {
    $usersearch = explode(" ", $search);
    $searchfirst = $usersearch[0];
    $searchlast = $usersearch[1];
    if (!$searchlast) {
      $searchlast = "Resident";
    }
    $pplq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE FirstName LIKE '%$searchfirst%' OR LastName LIKE '%$searchlast%' AND UserLevel > '-1' ORDER BY `FirstName` ASC LIMIT $offset, $totalrecords");
  }else if (!$search) {
    $pplq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE UserLevel > '-1' ORDER BY `FirstName` ASC LIMIT $offset, $totalrecords");
  }
while ($pplnum = $zw->SQL->fetch_array($pplq)) {
$uuid = $pplnum['PrincipalID'];
$sFirst = $pplnum['FirstName'];
$sLast = $pplnum['LastName'];

if ($sLast == "Resident") {
$profname = $sFirst;
$sprofname = $sFirst;
}else{
$profname = $sFirst.".".$sLast;
$sprofname = $sFirst." ".$sLast;
}

if ($zw->grid->online($uuid)) {
$onoff = "<span class='label label-danger'>Online</span>";
}else{
$onoff = "<span class='label label-success'>Offline</span>";
}

$profq = $zw->SQL->query("SELECT * FROM `{$zw->config['profile_db']}`.userprofile WHERE useruuid = '$uuid' AND profileMaturePublish < '$m'");
$prow = $zw->SQL->fetch_array($profq);
$show = $prow['profileAllowPublish'];
$MaturePublish = $prow['profileMaturePublish'];
$abouttext = $prow['profileAboutText'];
$fakepic = $prow['profileImage'];

if ($show == "0") {

  if ($abouttext) {
  $abouttext = htmlspecialchars_decode($abouttext, ENT_QUOTES);
  $abouttext = html_entity_decode($abouttext);
  $abouttext = substr($abouttext, 0, 125);
  $fakelife = "<p>
  $abouttext
  </p>";
  }else if (!$abouttext) {
  $fakelife = "";
  }

  if ($MaturePublish == 0) {
  $MaturePublish = $PG;
  }else if ($MaturePublish == 1) {
  $MaturePublish = $MATURE;
  }else if ($MaturePublish == 2) {
  $MaturePublish = $ADULT;
  }

  if (!$fakepic) {
  $pic = "<img src='".$ip2webassets."/asset.php?id=00000000-0000-0000-0000-000000000000' class='pull-right' width='75' height='75'>";
  }else{
  $pic = "<img src='".$ip2webassets."/asset.php?id=".$fakepic."' class='pull-right' width='75' height='75'>";
  }

  echo "  <div class='panel panel-default'>
      <div class='panel-heading'>
        <h4 class='panel-title'>
        <a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion' href='#people".$uuid."'><B>".$sprofname."</B></a> ".$onoff."
  	</h4>
      </div>
      <div id='people".$uuid."' class='panel-collapse collapse'>
        <div class='panel-body'>
  ".$pic."
  ".$fakelife."
        </div>
        <div class='panel-footer'><small><a href='".$site_address."/profile.php?u=".$profname."&issearch=true' class='btn btn-primary btn-sm'>View profile</a></small> <a href='secondlife:///app/agent/".$uuid."/about' class='btn btn-success btn-sm'>Add as Friend</a></div>
      </div>
    </div>
  ";

}else if ($show == "1" || !$show){
}

}

}
if ($type == "places" || !$type) {
$placeq = $zw->SQL->query("SELECT * FROM `{$zw->config['search_db']}`.parcels WHERE parcelname LIKE '%$search%' OR description LIKE '%$search%' AND public = 'true' ORDER BY `parcelname` ASC LIMIT $offset, $totalrecords");
while ($placer = $zw->SQL->fetch_array($placeq)) {
$regionUUID = $placer['regionUUID'];
$parcelname = $placer['parcelname'];
$parcelUUID = $placer['parcelUUID'];
$landingpoint = $placer['landingpoint'];
$description = $placer['description'];
$searchcategory = $placer['searchcategory'];
$mature = $placer['mature'];

$regionname = $zw->grid->regionname($regionUUID);
$usersonregion = $zw->grid->userspersim($regionUUID);

if ($mature == "PG") {
$mr = "1";
$mat = $PG;
}
if ($mature == "Mature") {
$mr = "2";
$mat = $MATURE;
}
if ($mature == "Adult") {
$mr = "3";
$mat = $ADULT;
}

echo "  <div class='panel panel-default'>
    <div class='panel-heading'>
      <h4 class='panel-title'>
      <a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion' href='#places".$parcelUUID."'>
<B>".$parcelname."</B>
      </a>
	</h4>
    </div>
    <div id='places".$parcelUUID."' class='panel-collapse collapse'>
      <div class='panel-body'>
<p>
$pic
".$description."<br>
<B>Mature Rating:</B> ".$mat."<br>
<B>Avatars on region:</B> ".$usersonregion."
<br>
<a href='secondlife://".$regionname."/".$landingpoint."/'>Teleport</a>
</p>
      </div>
    </div>
  </div>
";

}

}
echo "</div><br>
<div id='result".$button_offset."'></div>";
?>