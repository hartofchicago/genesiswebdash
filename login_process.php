<?php
define('ZW_IN_SYSTEM', true);
require_once('inc/headerless.php');

if ($zw->Security->make_safe($_POST['process'])) {
	$FirstName = $zw->Security->make_safe($_POST['FirstName']);
	$LastName = $zw->Security->make_safe($_POST['LastName']);
	$pass = $zw->Security->make_safe($_POST['password']);
	$remember = $zw->Security->make_safe($_POST['remember']);
	$lastpage = $zw->Security->make_safe($_POST['lastpage']);
	if ($zw->Users->login($FirstName, $LastName, $pass, $remember)) {
		if (!$lastpage) {
			$lastpage = "index.php";
		}
	    $zw->redirect($lastpage);
	}else{
	    $zw->redirect('login.php?err=invalidcreds');
	}
}else{
    $zw->redirect('login.php?err=unable2process');
}
?>