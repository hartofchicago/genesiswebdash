<?php
$page_title = "Confirm Email";
$hide_sidebars = true;
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');

$email = $zw->Security->make_safe($_GET['email']);
$code = $zw->Security->make_safe($_GET['code']);

if (!$user_uuid) {
	if ($email && $code) {
		$confirm = $zw->Users->emailconfirmed($email, $code);
		if ($confirm) {
			echo $zw->site->displayalert("Email address confirmed. Welcome to ".$zw->config['GridName'], "success");
		}else{
			echo $zw->site->displayalert("Invalid response.", "danger");
		}
	}else{ // else if ($email && $code)
		echo $zw->site->displayalert("Invalid response.", "danger");
	} // end if ($email && $code)
}else if ($user_uuid) {
	echo $zw->site->displayalert("You are already logged in. Please logout confirm your email address.", "danger");
}
include ('inc/footer.php');
?>