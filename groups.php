<?php
$page_title = "Groups";
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');
$gid = $zw->Security->make_safe($_GET['gid']);
if ($gid) {
$groupq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.os_groups_groups WHERE GroupID = '$gid' $where");
$groupr = $zw->SQL->fetch_array($groupq);

$Name = $groupr['Name'];
$Charter = $groupr['Charter'];
$InsigniaID = $groupr['InsigniaID'];
$FounderID = $groupr['FounderID'];
$OpenEnrollment = $groupr['OpenEnrollment'];
$MembershipFee = $groupr['MembershipFee'];
$MaturePublish = $groupr['MaturePublish'];
$FounderName = $zw->grid->uuid2name($FounderID);

$gmtbl = "<div class='table-responsive'>
<table class='table table-condensed table-hover table-striped'>
<thead>
<tr>
<th>Name</th>
<th>Title</th>
</tr>
</thead>
<tbody>
";
$groupmq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.os_groups_membership WHERE GroupID = '$gid'");
while ($groupmr = $zw->SQL->fetch_array($groupmq)) {
	$Princ = $zw->grid->uuid2name($groupmr['PrincipalID']);
	$selroleid = $groupmr['SelectedRoleID'];
	$selroleq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.os_groups_roles WHERE GroupID = '$gid' AND RoleID = '$selroleid'");
	$selroler = $zw->SQL->fetch_array($selroleq);
	$selrolename = $selroler['Title'];
	$gmtbl .= "
	<tr>
	<td><a href='".$site_address."/profile.php?u=".$Princ."'>".$Princ."</a></td>
	<td>".$selrolename."</td>
	</tr>";
}
$gmtbl .= "</tbody>
</table>
</div>";

$gmmtbl = "<div class='table-responsive'>
<table class='table table-condensed table-hover table-striped'>
<thead>
<tr>
<th>Subject</th>
<th>From</th>
</tr>
</thead>
<tbody>
";
$groupmmq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.os_groups_notices WHERE GroupID = '$gid' ORDER BY `TMStamp` DESC LIMIT 0,25");
while ($groupmmr = $zw->SQL->fetch_array($groupmmq)) {
	$NoticeID = $groupmmr['NoticeID'];
	$FromName = $groupmmr['FromName'];
	$Subject = $groupmmr['Subject'];
	$Message = $groupmmr['Message'];
	$gmmtbl .= "
	<tr>
	<td><a href='#' data-toggle='modal' data-target='#GNotice".$NoticeID."'>".$Subject."</a></td>
	<td><a href='".$site_address."/profile.php?u=".$FromName."'>".$FromName."</a></td>
	</tr>
<div class='modal fade' id='GNotice".$NoticeID."' tabindex='-1' role='dialog' aria-labelledby='GNoticeModalLabel' aria-hidden='true'>
  <div class='modal-dialog'>
    <div class='modal-content'>
      <div class='modal-header'>
      	".$Subject."
      </div>
      <div class='modal-body'>
      	".$Message."
      </div>
      <div class='modal-footer'>
      	<button type='button' class='btn btn-warning btn-sm' data-dismiss='modal'>Close</button>
      </div>
    </div>
  </div>
</div>";
}
$gmmtbl .= "</tbody>
</table>
</div>";
if ($OpenEnrollment == "1") {
$join = "<a href='secondlife:///app/group/".$gid."/join' class='btn btn-success btn-xs'>JOIN</a>";
}else if ($OpenEnrollment == "0") {
$join = "Closed to invites only.";
}
echo "<h1>".$Name."</h1>
<ul class='nav nav-tabs' role='tablist'>
	<li class='active'><a href='#info' role='tab' data-toggle='tab'>Info</a></li>
	<li><a href='#members' role='tab' data-toggle='tab'>Members</a></li>
	<li><a href='#notices' role='tab' data-toggle='tab'>Notices</a></li>
</ul>

<div class='tab-content'>
	<div class='tab-pane active' id='info'>
		<img src='".$ip2webassets."/asset.php?id=".$InsigniaID."&format=JPG' class='img-responsive pull-left' width='75' height='75'>
		<h3>Founder: <a href='".$site_address."/profile.php?u=".$FounderName."'>".$FounderName."</a></h3>
		".$Charter."<br>
		<B>Enrollment:</B> ".$join."
	</div>
	<div class='tab-pane' id='members'>
		".$gmtbl."
	</div>
	<div class='tab-pane' id='notices'>
		".$gmmtbl."
	</div>
</div>
";
}else if (!$gid) {
	if ($user_uuid) {
		echo "<div class='table-responsive'>
		<table class='table table-striped table-hover'>
		<thead>
		<tr>
		<th><B>NAME</B></th>
		<th><B>MEMBERS</B></th>
		</tr>
		</thead>
		<tbody>
		";
		$q = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.os_groups_membership WHERE PrincipalID = '$user_uuid'");
		while ($r = $zw->SQL->fetch_array($q)) {
			$GroupID = $r['GroupID'];
			$q2 = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.os_groups_groups WHERE GroupID = '$GroupID'");
			$q3 = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.os_groups_membership WHERE GroupID = '$GroupID'");
			$r2 = $zw->SQL->fetch_array($q2);
			$r3 = $zw->SQL->num_rows($q3);
			$Name = $r2['Name'];
			echo "<tr>
			<td><a href='".$zw->config['SiteAddress']."/groups.php?gid=".$GroupID."'>".$Name."</a></td>
			<td>".number_format($r3)."</td>
			</tr>
			";
		}
		echo "</tbody>
		</table>
		</div>";
	}
} // ends if ($gid)
include('inc/footer.php');
?>