<?php
$page_title = "Home";
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');

$i = 0;
$li = "";
$div = "";
$caroq = $zw->SQL->query("SELECT * FROM `{$zw->config['db_prefix']}carousel` ORDER BY `id` DESC LIMIT 0,3");
$caron = $zw->SQL->num_rows($caroq);
if ($caron) {
	while ($caror = $zw->SQL->fetch_array($caroq)) {
		$name = $caror['name'];
		$desc = $caror['description'];
		$image = $caror['image'];
		if ($i == 0) {
			$liclass = "class='active'";
			$divclass = "active";
		}else{
			$liclass = "";
			$divclass = "";
		}
		$li .= "<li data-target='#carousel' data-slide-to='".$i."' ".$liclass."></li>";
		$div .= "<div class='item ".$divclass."'>
	      <img src='".$image."' alt='".$name."'>
	      <div class='carousel-caption'>
	      	<h3>".$name."</h3>
	        <p>".$desc."</p>
	      </div>
	    </div>";
		$i++;
	}
}else{
	$li = "<li data-target='#carousel' data-slide-to='0' class='active'></li>";
	$div = "<div class='item active'>
      <img src='' alt='Nothing here yet'>
      <div class='carousel-caption'>
      	<h3>Nothing here yet</h3>
        <p>Move along. Nothing to see here.</p>
      </div>
    </div>";
}
?>
<div class="row">
	<div class="col-md-10">
		<div id="carousel" class="carousel slide" data-ride="carousel">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
		  	<?php echo $li; ?>
		  </ol>
		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
		    <?php echo $div; ?>
		  </div>
		  <!-- Controls -->
		  <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
		    <span class="fa fa-chevron-left"></span>
		  </a>
		  <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
		    <span class="fa fa-chevron-right"></span>
		  </a>
		</div>
  		<h3><?php echo $zw->config['GridName']; ?> News</h3>
  		<?php echo $zw->site->getNews('0', ''); ?>
	</div>
</div>
<?php
include ('inc/footer.php');
?>