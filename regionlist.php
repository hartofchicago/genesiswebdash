<?php
$page_title = "Region List";
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');

$landingpoint = "128/128/25";
$tplp = "128,128,25";

$tp = $zw->Security->make_safe($_POST['tp']);
if ($tp == "Instant TP" && $user_uuid != "") {
  $tpregion = $zw->Security->make_safe($_POST['tpregion']);
  $tploc = $zw->Security->make_safe($_POST['tploc']);
  $data = "TP=".$user_uuid."=".$tpregion."=<".$tploc.">";
  $zw->lsl->send2server($data);
}

$c = $zw->Security->make_safe($_GET['c']);
$page = $zw->Security->make_safe($_GET['page']);

$totaloffset = "50";
if (!$page || $page == "0" || $page == "1") {
$page = "1";
$offset = "0";
}else if ($page == "2") {
$offset = $totaloffset;
}else if ($page >= "2") {
$offset = $totaloffset / 2 * $page;
}

$regionq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.regions WHERE access != '21' ORDER BY `regionName` ASC LIMIT $offset, $totaloffset");
while ($regionr = $zw->SQL->fetch_array($regionq)) {
$regionUUID = $regionr['uuid'];
$ownerUUID = $regionr['owner_uuid'];
$regionname = $regionr['regionName'];
$regionMapTexture = $regionr['regionMapTexture'];
$sizex = $regionr['sizeX'];
$sizey = $regionr['sizeY'];
$locx = $regionr['locX'] / 256;
$locy = $regionr['locY'] / 256;
$loc = "<B>X:</B>".$locx." <B>Y:</B>".$locy;

$usersonregion = $zw->grid->userspersim($regionUUID);
$ownername = $zw->grid->uuid2name($ownerUUID);

if ($ownerUUID == $user_uuid) {
  $ismine = "<small>You own this region</small>";
}else{
  $ismine = "";
}

$pic = "<a href='' data-toggle='modal' data-target='#picmodal".$regionUUID."'><img src='".$ip2webassets."/asset.php?id=".$regionMapTexture."&format=JPG' class='img-responsive' width='100' height='100'></a>";
$mpic = "<img src='".$ip2webassets."/asset.php?id=".$regionMapTexture."&format=JPG' class='img-responsive' width='512' height='512'>";

echo "<div class='panel panel-default'>
    <div class='panel-heading'>
      <h4 class='panel-title'>
      <a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion' href='#regions".$regionUUID."'>
<B>".$regionname."</B> ".$ismine."
      </a>
	</h4>
    </div>
    <div id='regions".$regionUUID."' class='panel-collapse collapse'>
      <div class='panel-body'>
".$pic."
<B>Size:</B> ".$sizex." by ".$sizey."<br>
<B>Avatars on region:</B> ".$usersonregion."<br>
<B>Owner:</B> ".$ownername."<br>
<B>World Location:</B> ".$loc."
<br>
".$zw->config['HGAddress'].":".$regionname."<br>
";
if ($user_uuid != "" && $zw->config['InWorldServer'] != "") {
echo "<form method='post' action='".$site_address."/regionlist.php?c=".$c."' class='form' role='form'>
<input type='hidden' name='tpregion' value='".$regionname."'>
<input type='hidden' name='tploc' value='".$tplp."'>
<input type='submit' name='tp' value='Instant TP' class='btn btn-sm btn-info'>
</form>
      </div>
    </div>
</div>
";
}else if ($user_uuid != "" && $zw->config['InWorldServer'] == "") {
echo "<a href='secondlife://".$regionname."/".$landingpoint."/'>Teleport</a>";
}
?>
<div class="modal fade" id="picmodal<?php echo $regionUUID; ?>" tabindex="-1" role="dialog" aria-labelledby="PicModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Region Picture</h4>
      </div>
      <div class="modal-body">
        <p>
        <a data-dismiss="modal"><?php echo $mpic; ?></a>
      </p>
        <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php
}
$pager = $site_address."/regionlist.php?c=".$c;
$tbl_name = $zw->config['robust_db'].".regions WHERE access != '21'";
echo $zw->pagination->paging($tbl_name, $pager, $totaloffset);

include ('inc/footer.php');
?>