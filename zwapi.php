<?php
define('ZW_IN_SYSTEM', true);
require_once('inc/headerless.php');

$t = $zw->Security->make_safe($_GET['t']);
$u = $zw->Security->make_safe($_GET['u']);
$uuid = $zw->Security->make_safe($_GET['uuid']);

if ($t == "getpic") {
	echo $site_address."/webassets/asset.php?id=".$uuid;
	// to use this just add your zw site domain followed by /zwapi.php?t=getpic&uuid=UUID to a <img html tag
	// Then replace UUID with the UUID of the texture you like to show up as a website image.
}

if ($t == "usercounts") {
$onlineq = $zw->SQLgrid->query("SELECT * FROM GridUser WHERE Online = 'TRUE'");
$online = $zw->SQLgrid->num_rows($onlineq);

$totalq = $zw->SQLgrid->query("SELECT * FROM UserAccounts");
$totalc = $zw->SQLgrid->num_rows($totalq);

$monthago = $now - 2592000;
$latestq = $zw->SQLgrid->query("SELECT * FROM GridUser WHERE Login > '$monthago'");
$latestc = $zw->SQLgrid->num_rows($latestq);

$regionq = $zw->SQLgrid->query("SELECT * FROM regions");
$regionc = $zw->SQLgrid->num_rows($regionq);
$array =  array('ONLINE' => $online, 'TOTALUSERS' => $totalc, 'RECENTJOINS' => $latestc, 'REGIONS' => $regionc);
echo json_encode($array);
}

if ($t == "destinations") {
	$destecho = array("");
	$destq = $zw->SQLsearch->query("SELECT * FROM popularplaces ORDER BY `name` ASC LIMIT 0,10");
	while ($destr = $zw->SQLsearch->fetch_array($destq)) {
		$destname = $destr['name'];
		$destecho .= array($destname);
	}
	echo json_encode($destecho);
}

if ($t == "profile") {
	$userexplode = explode(".", $u);
	$first = $userexplode[0];
	$last = $userexplode[1];
	if (!$last) {
		$last = "Resident";
	}
	$uq = $zw->SQLgrid->query("SELECT * FROM UserAccounts WHERE FirstName = '$first' AND LastName = '$last'");
	$ur = $zw->SQLgrid->fetch_array($uq);
	$uuid = $ur['PrincipalID'];
	$profq = $zw->SQLprofile->query("SELECT * FROM userprofile WHERE useruuid = '$uuid'");
	$r = $zw->SQLprofile->fetch_array($profq);

	$partner = $r['profilePartner'];
	$url = $r['profileURL'];
	$want2mask = $r['profileWantToMask'];
	$want2text = $r['profileWantToText'];
	$skillsmask = $r['profileSkillsMask'];
	$skillstext = $r['profileSkillsText'];
	$lang = $r['profileLanguages'];
	$flpic = $r['profileImage'];
	$fakeaboutme = $r['profileAboutText'];
	$rlpic = $r['profileFirstImage'];
	$realaboutme = $r['profileFirstText'];
	$array = array("UUID"=>$uuid,"PIC"=>$flpic,"ABOUT"=>$fakeaboutme,"RLPIC"=>$rlpic,"RLABOUT"=>$realaboutme,"PARTNER"=>$partner,"URL"=>$url);
	echo json_encode($array);
}

if ($t == "examples") {
	// These are examples of how to get the info above using REST API.

	// Lets first get the usual user and region counts.
	// Remember to change localhost to your domain where your copy of zw is at.
	$body = file_get_contents("http://localhost/zwapi.php?t=usercounts");
	$contents = utf8_encode($body); // i figure it be best just in case to encode into something we humans can understand.
	$json = json_decode($contents); // this decodes all that json into readable strings.
	// now lets break that json down into strings for better use.
	$online = $json['ONLINE'];
	$totaluser = $json['TOTALUSERS'];
	$recentjoins = $json['RECENTJOINS'];
	$regions = $json['REGIONS'];
	// Then just echo or print those strings.

	// That was easy, lets do some looping with destinations guide.
	$destbody = file_get_contents("http://localhost/zwapi.php?t=destinations");
	$destcontents = utf8_encode($destbody);
	$destjson = json_decode($destcontents);
	foreach ($destjson as $key) {
		// This still needs some work but this is how you loop multiple input of the same source / string
	}

	// Ok that was abit confusing. MOVING ON!
	// profile API works simular to usercounts but requires 1 extra feild in the address.
	// The address for profiles is...
	// http://localhost/zwapi.php?t=profile&u=FirstName.LastName
	// See the FirstName.LastName ? Replace that with a existing user.
	// Example: http://localhost/zwapi.php?t=profile&u=Christina.Vortex
	// This is a great way to convert someones name to their uuid key.
	// I will write some RESTful php api for LSL soon.
}
?>