<?php
$page_title = "How to connect";
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');
?>
<h2>How to connect to the grid</h2>
<div class="row">
	<div class="col-md-4">
		<p>
			You will need a opensim compatible viewer. <a href="http://opensimulator.org/wiki/Connecting">http://opensimulator.org/wiki/Connecting</a> has a great list of viewers that work with opensim and a simple tutorial.
		</p>

		<p>
			The loginURI for this grid is <a href="<?php echo $zw->config['loginURI']; ?>"><?php echo $zw->config['loginURI']; ?></a>
		</p>

	</div>
</div>
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Viewer Downloads</h3>
  </div>
  <div class="panel-body">
    <a href="http://www.firestormviewer.org/downloads/" class="btn btn-primary"><i class="fa fa-windows"></i> <i class="fa fa-apple"></i> <i class="fa fa-linux"></i> Firestorm</a> <a href="http://www.singularityviewer.org/" class="btn btn-primary"><i class="fa fa-windows"></i> <i class="fa fa-apple"></i> <i class="fa fa-linux"></i> Singularity</a>
  </div>
</div>
<br />
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Firestorm Instructions</h3>
  </div>
  <div class="panel-body">
    <strong>Step 1</strong> <br />
    <img src="<?php echo $site_address; ?>/img/how2/fs1.png" />
    <p>Click Me > Preference</p> <br />
    <strong>Step 2</strong> <br />
    <img src="<?php echo $site_address; ?>/img/how2/fs2.png" />
    <p>Click Opensim, then in the address field put <strong><?php echo $zw->config['loginURI']; ?></strong> and hit apply</p>
    <strong>Step 3</strong> <br />
    <img src="<?php echo $site_address; ?>/img/how2/fs3.png" />
    <p>Enter your login information you registered with, and choose <strong><?php echo $zw->config['GridName']; ?></strong> from the grid selector, and hit login</p>
  </div>
</div>
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Singularity Instructions</h3>
  </div>
  <div class="panel-body">
    <strong>Step 1</strong> <br />
    <img src="<?php echo $site_address; ?>/img/how2/sing1.png" />
    <p>Click Grid Manager</p> <br />
    <strong>Step 2</strong> <br />
    <img src="<?php echo $site_address; ?>/img/how2/sing2.png" />
    <p>Click Create, then in the address field put <strong><?php echo $zw->config['loginURI']; ?></strong> and hit apply</p>
    <strong>Step 3</strong> <br />
    <img src="<?php echo $site_address; ?>/img/how2/sing1.png" />
    <p>Enter your login information you registered with, and choose <strong><?php echo $zw->config['GridName']; ?></strong> from the grid selector, and hit login</p>
  </div>
</div>
<?php
include ('inc/footer.php');
?>