/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : zmw

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2014-10-08 11:26:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for invoice_payments
-- ----------------------------
DROP TABLE IF EXISTS `invoice_payments`;
CREATE TABLE `invoice_payments` (
  `invoice_id` varchar(255) NOT NULL,
  `transaction_hash` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `user_uuid` varchar(255) DEFAULT NULL,
  `secret` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`invoice_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of invoice_payments
-- ----------------------------

-- ----------------------------
-- Table structure for zw_apps
-- ----------------------------
DROP TABLE IF EXISTS `zw_apps`;
CREATE TABLE `zw_apps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appname` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `port` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `permissions` varchar(255) DEFAULT NULL,
  `oauthId` varchar(255) DEFAULT NULL,
  `oauthSecret` varchar(255) DEFAULT NULL,
  `capabilitiesUrl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_apps
-- ----------------------------

-- ----------------------------
-- Table structure for zw_auth_apps
-- ----------------------------
DROP TABLE IF EXISTS `zw_auth_apps`;
CREATE TABLE `zw_auth_apps` (
  `uuid` varchar(255) NOT NULL,
  `user` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_auth_apps
-- ----------------------------

-- ----------------------------
-- Table structure for zw_carousel
-- ----------------------------
DROP TABLE IF EXISTS `zw_carousel`;
CREATE TABLE `zw_carousel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` longtext,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_carousel
-- ----------------------------
INSERT INTO `zw_carousel` VALUES ('1', 'localhost', 'Welcome to localhost grid. Where communities come together.', './bgimg/welcomesimfullres.jpg');

-- ----------------------------
-- Table structure for zw_emailconfirm
-- ----------------------------
DROP TABLE IF EXISTS `zw_emailconfirm`;
CREATE TABLE `zw_emailconfirm` (
  `uuid` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `isnewuser` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`uuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_emailconfirm
-- ----------------------------

-- ----------------------------
-- Table structure for zw_forum_board
-- ----------------------------
DROP TABLE IF EXISTS `zw_forum_board`;
CREATE TABLE `zw_forum_board` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catid` varchar(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `sort` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_forum_board
-- ----------------------------
INSERT INTO `zw_forum_board` VALUES ('1', '1', 'Test', 'test board', '0');

-- ----------------------------
-- Table structure for zw_forum_cat
-- ----------------------------
DROP TABLE IF EXISTS `zw_forum_cat`;
CREATE TABLE `zw_forum_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `sort` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_forum_cat
-- ----------------------------
INSERT INTO `zw_forum_cat` VALUES ('1', 'Test cat', '0');

-- ----------------------------
-- Table structure for zw_forum_replies
-- ----------------------------
DROP TABLE IF EXISTS `zw_forum_replies`;
CREATE TABLE `zw_forum_replies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `board_id` varchar(11) NOT NULL,
  `topic_id` varchar(11) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `message` longtext,
  `time` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_forum_replies
-- ----------------------------

-- ----------------------------
-- Table structure for zw_forum_topic
-- ----------------------------
DROP TABLE IF EXISTS `zw_forum_topic`;
CREATE TABLE `zw_forum_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `board_id` varchar(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `message` longtext,
  `time` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_forum_topic
-- ----------------------------
INSERT INTO `zw_forum_topic` VALUES ('1', '1', 'Test topic', 'Test message', '1392913592', null);

-- ----------------------------
-- Table structure for zw_mainmenu
-- ----------------------------
DROP TABLE IF EXISTS `zw_mainmenu`;
CREATE TABLE `zw_mainmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `childof` varchar(255) NOT NULL DEFAULT '0',
  `sortby` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_mainmenu
-- ----------------------------
INSERT INTO `zw_mainmenu` VALUES ('1', 'Search', 'search.php', '0', '2');
INSERT INTO `zw_mainmenu` VALUES ('2', 'Land Rentals', '', '0', '3');
INSERT INTO `zw_mainmenu` VALUES ('3', 'Regions', '', '2', '1');
INSERT INTO `zw_mainmenu` VALUES ('4', 'Simulators', '', '2', '2');
INSERT INTO `zw_mainmenu` VALUES ('13', 'Grid Status', 'api/gridstatus.php', '5', '0');
INSERT INTO `zw_mainmenu` VALUES ('5', 'Grid', '', '0', '4');
INSERT INTO `zw_mainmenu` VALUES ('6', 'Community', '', '0', '1');
INSERT INTO `zw_mainmenu` VALUES ('7', 'Map', 'map.php', '5', '2');
INSERT INTO `zw_mainmenu` VALUES ('8', 'How to Connect', 'how2connect.php', '6', '3');
INSERT INTO `zw_mainmenu` VALUES ('9', 'News', 'news.php', '6', '5');
INSERT INTO `zw_mainmenu` VALUES ('10', 'Forum', 'forum', '6', '1');
INSERT INTO `zw_mainmenu` VALUES ('11', 'Land Mass Count', 'landmass.php', '5', '1');
INSERT INTO `zw_mainmenu` VALUES ('12', 'Region List', 'regionlist.php', '5', '2');
INSERT INTO `zw_mainmenu` VALUES ('14', 'Live Chat Support', 'hipchatsupport.php', '6', '0');
INSERT INTO `zw_mainmenu` VALUES ('16', 'eLetters', 'eletter.php', '6', '6');

-- ----------------------------
-- Table structure for zw_mostusers
-- ----------------------------
DROP TABLE IF EXISTS `zw_mostusers`;
CREATE TABLE `zw_mostusers` (
  `count` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_mostusers
-- ----------------------------

-- ----------------------------
-- Table structure for zw_news
-- ----------------------------
DROP TABLE IF EXISTS `zw_news`;
CREATE TABLE `zw_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `msg` longtext,
  `time` varchar(255) DEFAULT NULL,
  `edit_time` varchar(255) DEFAULT NULL,
  `poster` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_news
-- ----------------------------

-- ----------------------------
-- Table structure for zw_newsletter
-- ----------------------------
DROP TABLE IF EXISTS `zw_newsletter`;
CREATE TABLE `zw_newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `useruuid` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` longtext CHARACTER SET latin1,
  `time` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_newsletter
-- ----------------------------

-- ----------------------------
-- Table structure for zw_resetcode
-- ----------------------------
DROP TABLE IF EXISTS `zw_resetcode`;
CREATE TABLE `zw_resetcode` (
  `uuid` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `expiry` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_resetcode
-- ----------------------------

-- ----------------------------
-- Table structure for zw_sessions
-- ----------------------------
DROP TABLE IF EXISTS `zw_sessions`;
CREATE TABLE `zw_sessions` (
  `id` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_sessions
-- ----------------------------

-- ----------------------------
-- Table structure for zw_settings
-- ----------------------------
DROP TABLE IF EXISTS `zw_settings`;
CREATE TABLE `zw_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `info` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_settings
-- ----------------------------
INSERT INTO `zw_settings` VALUES ('1', 'robust_db', 'grid', 'db name for grid logins', 'grid');
INSERT INTO `zw_settings` VALUES ('2', 'group_db', 'grid', 'db name for groups', 'grid');
INSERT INTO `zw_settings` VALUES ('3', 'profile_db', 'grid', 'db name for profiles', 'grid');
INSERT INTO `zw_settings` VALUES ('4', 'search_db', 'search', 'db name for search related data', 'grid');
INSERT INTO `zw_settings` VALUES ('5', 'money_db', 'money', 'db name for money', 'grid');
INSERT INTO `zw_settings` VALUES ('6', 'SiteAddress', 'http://localhost', 'Site address of this site', 'zmw');
INSERT INTO `zw_settings` VALUES ('7', 'loginURI', 'localhost:8002', 'Login URL address for users to use to log into your grid', 'grid');
INSERT INTO `zw_settings` VALUES ('8', 'GridNick', 'localhost', 'Grid\'s nickname', 'grid');
INSERT INTO `zw_settings` VALUES ('9', 'GridName', 'localhost', 'Name of your grid', 'grid');
INSERT INTO `zw_settings` VALUES ('10', 'PrimEmail', '@localhost', 'This is used to email in world prims with data', 'grid');
INSERT INTO `zw_settings` VALUES ('11', 'GridMoney', 'OS$', 'Currency icon seen at the top right of most viewers', 'money');
INSERT INTO `zw_settings` VALUES ('12', 'AllowRegistration', 'y', 'Allow people to join through the ZMW registration page?', 'zmw');
INSERT INTO `zw_settings` VALUES ('13', 'SiteEmail', 'noreply@localhost', 'Email address to use to send email to users such as when registering if activation_type is set to User', 'zmw');
INSERT INTO `zw_settings` VALUES ('14', 'TimeZone', 'America/Los_Angeles', null, 'zmw');
INSERT INTO `zw_settings` VALUES ('15', 'cookie_prefix', 'zw_', null, 'zmw');
INSERT INTO `zw_settings` VALUES ('16', 'cookie_length', '1209600', null, 'zmw');
INSERT INTO `zw_settings` VALUES ('17', 'cookie_path', '/', null, 'zmw');
INSERT INTO `zw_settings` VALUES ('18', 'cookie_domain', 'localhost', null, 'zmw');
INSERT INTO `zw_settings` VALUES ('19', 'logout_redirect', 'index.php', null, 'zmw');
INSERT INTO `zw_settings` VALUES ('20', 'activation_type', '0', null, 'zmw');
INSERT INTO `zw_settings` VALUES ('21', 'security_image', 'yes', 'This will make people pass the recaptcha test', 'zmw');
INSERT INTO `zw_settings` VALUES ('22', 'redirect_type', '1', null, 'zmw');
INSERT INTO `zw_settings` VALUES ('23', 'max_password', '15', null, 'zmw');
INSERT INTO `zw_settings` VALUES ('24', 'min_password', '6', null, 'zmw');
INSERT INTO `zw_settings` VALUES ('25', 'Style', '1', 'Bootstrap style for this site. zw uses the default bootstrap API system. No crappy WP system.', 'zmw');
INSERT INTO `zw_settings` VALUES ('26', 'Banner', '', null, 'zmw');
INSERT INTO `zw_settings` VALUES ('27', 'Logo', '', null, 'zmw');
INSERT INTO `zw_settings` VALUES ('28', 'Twitter', '', 'The twitter account for this grid.', 'api');
INSERT INTO `zw_settings` VALUES ('29', 'TwitterAPIKey', '', 'API Key for Twitter', 'api');
INSERT INTO `zw_settings` VALUES ('30', 'Facebook', '', 'The facebook account for this grid.', 'api');
INSERT INTO `zw_settings` VALUES ('31', 'FacebookAPIKey', '', 'API Key for Facebook', 'api');
INSERT INTO `zw_settings` VALUES ('32', 'site_admin_level', '200', 'Min. OpenSim UserLevel to access admin stuff on this site.', 'zmw');
INSERT INTO `zw_settings` VALUES ('33', 'DisqusShortName', 'localhost', 'For Disqus chat. Best to get a account at disqus.com', 'zmw');
INSERT INTO `zw_settings` VALUES ('34', 'ReCaptcha_Public_Key', '', 'You need your own recaptcha keys here.', 'zmw');
INSERT INTO `zw_settings` VALUES ('35', 'ReCaptcha_Private_Key', '', 'Please visit http://www.google.com/recaptcha to get your own.', 'zmw');
INSERT INTO `zw_settings` VALUES ('36', 'Default_Female', 'Female Avi', 'Default Female avatar name to copy from when people log in for the first time.', 'grid');
INSERT INTO `zw_settings` VALUES ('37', 'Default_Male', 'Male Avi', 'Default Male and Female feilds MUST be their name, not UUID key or it wont work.', 'grid');
INSERT INTO `zw_settings` VALUES ('38', 'Default_Sim', 'Welcome', 'Default sim name that all new users will have their home set to and login at.', 'grid');
INSERT INTO `zw_settings` VALUES ('39', 'Default_Pos', '135,128,23', 'Default position in world. Example: <128,128,30>', 'grid');
INSERT INTO `zw_settings` VALUES ('40', 'ShowStats', 'true', 'Shows the grid count and status on the splash login page.', 'zmw');
INSERT INTO `zw_settings` VALUES ('41', 'AssetsAddress', 'http://localhost:8003', 'Address to the grid\'s assets folder', 'grid');
INSERT INTO `zw_settings` VALUES ('42', 'WebAssets', 'http://localhost/img', 'Address to your Web Assets', 'grid');
INSERT INTO `zw_settings` VALUES ('43', 'NoPicUUID', '75e02516-8277-41f7-9f90-591f2f1dccfc', 'UUID Key of the in world texture to use', 'zmw');
INSERT INTO `zw_settings` VALUES ('44', 'Money', 'true', 'Do you have a money module installed? ZMW only supports the latest DTL/NSL', 'money');
INSERT INTO `zw_settings` VALUES ('45', 'APITokenLength', '25', 'Number of characters for each generated api token', 'api');
INSERT INTO `zw_settings` VALUES ('46', 'HipChatToken', '', 'Your HipChat API Token.', 'api');
INSERT INTO `zw_settings` VALUES ('47', 'HipChatRoomID', '', 'ID number for your hipchat room that is public', 'api');
INSERT INTO `zw_settings` VALUES ('48', 'HipChatSupportNames', '', 'Names of those who are in the room to give support seperated by a comma (,)', 'api');
INSERT INTO `zw_settings` VALUES ('49', 'HipChatPrivateRoomID', '', 'ID number for your hipchat room that is private to admins only', 'api');
INSERT INTO `zw_settings` VALUES ('50', 'RemoteAdminHost', 'localhost', 'Host Address for Remote Admin, this is usually your loginuri', 'grid');
INSERT INTO `zw_settings` VALUES ('51', 'RemoteAdminPort', '8002', 'Host Port for Remote Admin, this is set in the opensim configs under [RemoteAdmin]', 'grid');
INSERT INTO `zw_settings` VALUES ('52', 'RemoteAdminSecret', 'secret', 'Secret password for Remote Admin, this is set in the opensim configs under [RemoteAdmin]', 'grid');
INSERT INTO `zw_settings` VALUES ('53', 'InWorldServer', '', 'Web address to the in world server prim. This is set automatically when you rez the script.', 'grid');
INSERT INTO `zw_settings` VALUES ('54', 'BitCoinAddress', '', 'Address for received payments', 'money');
INSERT INTO `zw_settings` VALUES ('55', 'BTC2Currency', '100', 'How much in world money per 0.001 BTC', 'money');
INSERT INTO `zw_settings` VALUES ('56', 'PaymentType', 'BitCoin, ', 'Payment type to use for users to buy in world currency seperated by a comma (, )', 'money');
INSERT INTO `zw_settings` VALUES ('57', 'HGAddress', 'hg.localhost:8002', 'Address for the Hypergrid', 'grid');
INSERT INTO `zw_settings` VALUES ('58', 'WebMap', '', 'Address to your webmap', 'zmw');

-- ----------------------------
-- Table structure for zw_settings_menu
-- ----------------------------
DROP TABLE IF EXISTS `zw_settings_menu`;
CREATE TABLE `zw_settings_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_settings_menu
-- ----------------------------
INSERT INTO `zw_settings_menu` VALUES ('1', 'zmw');
INSERT INTO `zw_settings_menu` VALUES ('2', 'grid');
INSERT INTO `zw_settings_menu` VALUES ('3', 'money');
INSERT INTO `zw_settings_menu` VALUES ('4', 'api');

-- ----------------------------
-- Table structure for zw_style
-- ----------------------------
DROP TABLE IF EXISTS `zw_style`;
CREATE TABLE `zw_style` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `folder` varchar(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_style
-- ----------------------------
INSERT INTO `zw_style` VALUES ('1', 'Default', 'Default', '3.1.1 (http://getbootstrap.com)\n ');
INSERT INTO `zw_style` VALUES ('2', 'United', 'United', '3.1.1+1\n ');

-- ----------------------------
-- Table structure for zw_ticket_reply
-- ----------------------------
DROP TABLE IF EXISTS `zw_ticket_reply`;
CREATE TABLE `zw_ticket_reply` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `tid` varchar(255) DEFAULT NULL,
  `user_uuid` varchar(255) DEFAULT NULL,
  `body` longtext,
  `time` varchar(255) DEFAULT NULL,
  `userviewstatus` char(1) DEFAULT '0',
  `adminviewstatus` char(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_ticket_reply
-- ----------------------------

-- ----------------------------
-- Table structure for zw_tickets
-- ----------------------------
DROP TABLE IF EXISTS `zw_tickets`;
CREATE TABLE `zw_tickets` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_uuid` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` longtext,
  `status` varchar(255) DEFAULT '0',
  `time_created` varchar(255) DEFAULT NULL,
  `time_closed` varchar(255) DEFAULT NULL,
  `adminviewstatus` char(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_tickets
-- ----------------------------

-- ----------------------------
-- Table structure for zw_users
-- ----------------------------
DROP TABLE IF EXISTS `zw_users`;
CREATE TABLE `zw_users` (
  `uuid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `style` varchar(255) DEFAULT '0',
  `activationstatus` varchar(255) NOT NULL DEFAULT 'approved',
  `referredby` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `last_action` varchar(255) DEFAULT NULL,
  `last_page` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_users
-- ----------------------------

-- ----------------------------
-- Table structure for zw_weddings
-- ----------------------------
DROP TABLE IF EXISTS `zw_weddings`;
CREATE TABLE `zw_weddings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(255) DEFAULT NULL,
  `ProposeTo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zw_weddings
-- ----------------------------
