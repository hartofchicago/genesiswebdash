<?php
$page_title = "Offline Messages";
$hide_sidebars = true;
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');

$totaloffset = "500"; // number of results to display

$search = $zw->Security->make_safe($_GET['search']);

if ($user_uuid) {

$money = $zw->config['Money'];
$uuid = $user_uuid;

$now = time();

if ($search) {
	$wsearch = strtoupper($search);
	$wsearch = strip_tags($wsearch);
	$wsearch = trim($wsearch);
	$where = "AND UUID LIKE '%$wsearch%'";
}else if (!$search) {
	$where = "";
}

if ($money == "true") {
	$balq = $zw->SQL->query("SELECT * FROM `{$zw->config['money_db']}`.balances WHERE user = '$user_uuid'");
	$balr = $zw->SQL->fetch_array($balq);
	$viewerbal = $balr['balance'];
	$viewername = $zw->grid->uuid2name($getuuid);
?>
	<h3>Transaction Log for <?php echo $viewername; ?></h3>
	<B>Your balance is <?php echo $zw->config['GridMoney']." ".$viewerbal; ?></B> <a href='givemoney.php'>Send Money to someone</a><br>
	<small>Showing your last <?php echo $totaloffset; ?> transactions starting with the most recent.</small><br>
	<form method='get' action='' class="form-horizontal" role="form">
		<div class="form-group">
			<div class="col-sm-10">
    			<input type="text" name="search" value="<?php echo $search; ?>" class="form-control" placeholder="Search for Transaction ID">
    		</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10">
				<button type="submit" class="btn btn-success">Search</button>
				<a href='transactionstest.php' class="btn btn-warning">Reset</a>
			</div>
		</div>
	</form>
	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist" id="tabs">
	  <li class="active"><a href="#received" role="tab" data-toggle="tab">Received</a></li>
	  <li><a href="#sent" role="tab" data-toggle="tab">Sent</a></li>
	</ul>
	<!-- Tab panes -->
	<div class="tab-content">
	  <div class="tab-pane active" id="received">
			<!-- Money Received -->
			<div class='table-responsive'>
				<table class='table table-condensed table-hover table-striped'>
					<thead>
						<tr>
							<th>Transaction ID</th>
							<th>Sender</th>
							<th>Object</th>
							<th>Amount</th>
							<th>Date</th>
							<th>Type of Transaction</th>
						</tr>
					</thead>
					<tbody>
	<?php
	if ($uuid) {
		$gettingq = $zw->SQL->query("SELECT * FROM `{$zw->config['money_db']}`.transactions WHERE receiver = '$user_uuid' $where ORDER BY `time` DESC LIMIT 0, $totaloffset");
		while ($gettingr = $zw->SQL->fetch_array($gettingq)) {
			$rtransid = $gettingr['UUID'];
			$rsenderid = $gettingr['sender'];
			$rreceiveruuid = $gettingr['receiver'];
			$ramountreceived = $gettingr['amount'];
			$robjectUUIDr = $gettingr['objectUUID'];
			$rdesc = $gettingr['description'];
			$rtranstime = $gettingr['time'];

			$rsender = $zw->money->aviuuid2name($rsenderid);
			$objectr = $zw->money->primuuid2name($robjectUUIDr);
			$dater = $zw->site->time2date($rtranstime);
				echo "<tr>
				<td>".$rtransid."</td>
				<td>".$rsender."</td>
				<td>".$objectr."</td>
				<td>".$ramountreceived."</td>
				<td>".$dater."</td>
				<td>".$rdesc."</td>
				</tr>";
		}
	}
	?>
					</tbody>
				</table>
			</div> <!-- class='table-responsive' Money Received -->
		</div>
		<div class="tab-pane" id="sent">
			<!-- Money Sent -->
			<div class='table-responsive'>
				<table class='table table-condensed table-hover table-striped'>
					<thead>
						<tr>
							<th>Transaction ID</th>
							<th>Receiver</th>
							<th>Object</th>
							<th>Amount</th>
							<th>Date</th>
							<th>Type of Transaction</th>
						</tr>
					</thead>
					<tbody>
	<?php
	if ($uuid) {
		$sendingq = $zw->SQL->query("SELECT * FROM `{$zw->config['money_db']}`.transactions WHERE sender = '$user_uuid' $where ORDER BY `time` DESC LIMIT 0, $totaloffset");
		while ($sendingr = $zw->SQL->fetch_array($sendingq)) {
			$stransid = $sendingr['UUID'];
			$ssenderuuid = $sendingr['sender'];
			$sreceiverid = $sendingr['receiver'];
			$samountsent = $sendingr['amount'];
			$sobjectUUIDs = $sendingr['objectUUID'];
			$sdesc = $sendingr['description'];
			$stranstime = $sendingr['time'];

			$sreceiver = $zw->money->aviuuid2name($sreceiverid);
			$sobjects = $zw->money->primuuid2name($sobjectUUIDs);
			$sdate = $zw->site->time2date($stranstime);
				echo "<tr>
				<td>".$stransid."</td>
				<td>".$sreceiver."</td>
				<td>".$sobjects."</td>
				<td>".$samountsent."</td>
				<td>".$sdate."</td>
				<td>".$sdesc."</td>
				</tr>";
		}
	}
	?>
					</tbody>
				</table>
			</div> <!-- class='table-responsive' Money Sent -->

		</div>
	</div> <!-- ends the tabbing -->
<?php
}else{
	echo $zw->site->displayalert("This server currently does not have a money module installed.", "warning");
}
}else if(!$user_uuid) {
echo $zw->site->displayalert("You need to be logged into the ".$zw->config['GridName']." website to see your transactions.", "danger");
} // ends if ($user_uuid)
include ('inc/footer.php');
?>