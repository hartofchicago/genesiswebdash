<?php
$page_title = "My Sims";
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');

$uuid = $zw->Security->make_safe($_POST['uuid']);
$restart = $zw->Security->make_safe($_POST['restart']);

if ($user_uuid) {
	if ($restart == "Restart") {
		$getinfoq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.regions WHERE uuid = '$uuid'");
		$getinfor = $zw->SQL->fetch_array($getinfoq);
		$url = $getinfor['serverIP'];
		$port = $getinfor['serverPort'];
		$command = "admin_restart";
		$paramaters = array('regionID' => $uuid);
		echo $zw->console->remoteadmin($url, $port, $command, $paramaters);
	}
	$q = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.regions WHERE owner_uuid = '$user_uuid'");
	while ($r = $zw->SQL->fetch_array($q)) {
		$uuid = $r['uuid'];
		$name = $r['regionName'];
		echo "<form method='post' action='' class='form' role='form'>
		<input type='hidden' name='uuid' value='".$uuid."'>
		".$name." - <input type='submit' name='restart' value='Restart' class='btn btn-primary'>
		</form>";
	}
}else{
	echo $zw->site->displayalert("You need to be logged in to manage your regions.", "danger");
}

include ('inc/footer.php');
?>