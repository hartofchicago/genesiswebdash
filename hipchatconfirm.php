<?php
define('ZW_IN_SYSTEM', true);
require_once('inc/headerless.php');

$appname = "HipChat";
$port = "80";
$type = "chat";
$token = $zw->config['HipChatToken'];
$permissions = "";

$jsonpost = file_get_contents('php://input');
//if ($jsonpost) {
	$json = json_decode($jsonpost, TRUE);
	$capabilitiesUrl = $json['capabilitiesUrl'];
	$oauthId = $json['oauthId'];
	$oauthSecret = $json['oauthSecret'];
	$checkappq = $zw->SQL->query("SELECT * FROM `{$zw->config['db_prefix']}apps` WHERE appname = '$appname'");
	$checkappn = $zw->SQL->num_rows($checkappq);
	if ($checkappn) {
		$updaterecords = $zw->SQL->query("UPDATE `{$zw->config['db_prefix']}apps` SET capabilitiesUrl = '$capabilitiesUrl', oauthId = '$oauthId', oauthSecret = '$oauthSecret' WHERE appname = '$appname'");
	}else{
		$updaterecords = $zw->SQL->query("INSERT INTO `{$zw->config['db_prefix']}apps` (appname, name, address, port, token, type, permissions, oauthId, oauthSecret, capabilitiesUrl) VALUES ('$appname', '$jsonpost', '$ip', '$port', '$token', '$type', '$permissions', '$oauthId', '$oauthSecret', '$capabilitiesUrl')");
	}
	if ($updaterecords) {
		$gridname = $zw->config['GridName'];
		$description = "ZMW HipChat integration for ".$gridname." opensim grid";
		$cookieexplode = explode(".", $zw->config['cookie_domain']);
  		$domkey = $cookieexplode[1].".".$cookieexplode[0].".zmw";
  		$confirmaddy = $site_address."/hipchatconfirm.php";
  		$linkarray = array("homepage" => $site_address, "self" => $confirmaddy);
  		$capsarray = array("scopes" => "send_notification");
  		$capabilitiesarray = array("hipchatApiConsumer" => $capsarray);
		$jsonarray = array("name" => $gridname." HipChat", "description" => $description, "key" => $domkey, "links" => $linkarray, "capabilities" => $capabilitiesarray);
		//$zw->hipchat->sendinstallback($capabilitiesUrl, $jsonarray);
		header('Content-Type: application/json; charset=utf-8');
		$echojsonarray = array("grant_type" => $token, "scope" => "send_notification");
		echo json_encode($echojsonarray);
	}else{
		$jsonarray = array("name" => "JSONRequestError", "message" => "ERROR: Unable to save data");
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($jsonarray);
	}
//}
?>