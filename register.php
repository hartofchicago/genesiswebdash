<?php
$page_title = "Register";
$hide_sidebars = true;
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');

if (!$user_uuid && $zw->config['AllowRegistration'] == "y") {
	if (isset($_POST['process'])) {
		echo $zw->Users->register_user();
	}else{
        require_once('register_form.php');
	}
}else if (!$user_uuid && $zw->config['AllowRegistration'] == "n") {
	echo "Registrations for this grid are currently closed.";
}else if ($user_uuid) {
    echo "You are already logged in.";
}

require_once('inc/footer.php');
?>