<?php
$page_title = "Search";
$hide_sidebars = true;
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');
/*** *** *** *** *** ***
* OSSearch is also required for this page to get search info from your grid.
* For this to work on your grid add the following line into your Robust.ini or Robust.HG.ini in the [LoginService] section.
* SearchURL = "http://yourwebsitesite_addresshere/search.php?search=[QUERY]"
*** *** *** *** *** ***/
$gsearch = $zw->Security->make_safe($_GET['search']);
$search = strtoupper($gsearch);
$search = strip_tags($search);
$search = trim($search);
$searchlink = $gsearch;

$type = $zw->Security->make_safe($_GET['type']);
$m = $zw->Security->make_safe($_GET['m']);

if ($search) {
$placeholder = "Searching for $search";
}else if (!$search) {
$placeholder = "Search";
}

$select = "selected";

if (!$m) {
$m = "1";
}

// no idea here yet
$eventcat = "<select name='eventcat'>
<option value=''></option>
<option value=''></option>
<option value=''></option>
<option value=''></option>
<option value=''></option>
<option value=''></option>
<option value=''></option>
<option value=''></option>
</select>";

$PG = "<span class='label label-success'>PG</span>";
$MATURE = "<span class='label label-default'>M</span>";
$ADULT = "<span class='label label-danger'>A</span>";
?>
<form class="form-inline" method="get" action="" role="form">
<div class="form-group">
    <input type="text" name="search" class="form-control" id="appendedInputButtons" placeholder="<?php echo $placeholder; ?>">
</div>
<div class="form-group">
    <select name="type" class="form-control">
	   <option value="" <?php if (!$type) { echo ""; } ?>>Everything</option>
	   <option value="classifieds" <?php if ($type == "classifieds") { echo "$select"; } ?>>Classifieds</option>
	   <option value="destinations" <?php if ($type == "destinations") { echo "$select"; } ?>>Destinations</option>
	   <option value="events" <?php if ($type == "events") { echo "$select"; } ?>>Events</option>
	   <option value="groups" <?php if ($type == "groups") { echo "$select"; } ?>>Groups</option>
	   <option value="4sale" <?php if ($type == "4sale") { echo "$select"; } ?>>Land & Rentals</option>
	   <option value="people" <?php if ($type == "people") { echo "$select"; } ?>>People</option>
	   <option value="places" <?php if ($type == "places") { echo "$select"; } ?>>Places</option>
    </select>
</div>

<div class="form-group">
  <input type="radio" id="inlineCheckbox1" name="m" value="1" <?php if ($m == "1") { echo "CHECKED"; } ?>><?php echo $PG; ?>
  <input type="radio" id="inlineCheckbox2" name="m" value="2" <?php if ($m == "2") { echo "CHECKED"; } ?>><?php echo $MATURE; ?>
  <input type="radio" id="inlineCheckbox3" name="m" value="3" <?php if ($m == "3") { echo "CHECKED"; } ?>><?php echo $ADULT; ?>
</div>

<div class="form-group">
<button type="submit" class="btn btn-success">Search</button>
</div>

</form>

<TABLE>
<TR VALIGN="top" style="valign: top;">
<TD ALIGN="left" STYLE="width:150px;">
  <ul class="nav nav-pills nav-stacked">
<li <?php if (!$type) { echo "class='active'"; } ?>><a href="<?php echo $thispage."?search=$searchlink&type=&m=$m"; ?>">Everything</a></li>


<li <?php if ($type == "classifieds") { echo "class='active'"; } ?>><a href="<?php echo $thispage."?search=$searchlink&type=classifieds&m=$m"; ?>">Classifieds</a></li>


<li <?php if ($type == "destinations") { echo "class='active'"; } ?>><a href="<?php echo $thispage."?search=$searchlink&type=destinations&m=$m"; ?>">Destinations</a></li>


<li <?php if ($type == "events") { echo "class='active'"; } ?>><a href="<?php echo $thispage."?search=$searchlink&type=events&m=$m"; ?>">Events</a></li>
<?php if ($type == "events") { echo $eventcat; } ?>

<li <?php if ($type == "groups") { echo "class='active'"; } ?>><a href="<?php echo $thispage."?search=$searchlink&type=groups&m=$m"; ?>">Groups</a></li>
<?php
if ($type == "groups") {
echo "Group Test";
}
?>

<li <?php if ($type == "4sale") { echo "class='active'"; } ?>><a href="<?php echo $thispage."?search=$searchlink&type=4sale&m=$m"; ?>">Land & Rentals</a></li>


<li <?php if ($type == "people") { echo "class='active'"; } ?>><a href="<?php echo $thispage."?search=$searchlink&type=people&m=$m"; ?>">People</a></li>


<li <?php if ($type == "places") { echo "class='active'"; } ?>><a href="<?php echo $thispage."?search=$searchlink&type=places&m=$m"; ?>">Places</a></li>
<?php
if ($type == "places") {
echo "Test";
}
?>
  </ul>
</TD>
<TD ALIGN="left" STYLE='max-width:600px'>
<div class="panel-group" id="accordion" style="width:600px; height: auto;">
<?php
if ($type == "classifieds" || !$type) {
	echo "<h3>Classifieds</h3>";
$cq = $zw->SQL->query("SELECT * FROM `{$zw->config['profile_db']}`.classifieds WHERE name LIKE '%$search%' OR description LIKE '%$search%' AND creationdate < '$now' AND expirationdate > '$now' AND classifiedflags < '$m' ORDER BY 'creationdate' DESC LIMIT 0,100");
while ($cn = $zw->SQL->fetch_array($cq)) {
$classifieduuid = $cn['classifieduuid'];
$creatoruuid = $cn['creatoruuid'];
$creationdate = $cn['creationdate'];
$expirationdate = $cn['expirationdate'];
$category = $cn['category'];
$name = $cn['name'];
$parceluuid = $cn['parceluuid'];
$description = $cn['description'];
$snapshotuuid = $cn['snapshotuuid'];
$simname = $cn['simname'];
$classifiedflags = $cn['classifiedflags'];
$parceluuid = $cn['parceluuid'];
$parcelname = $cn['parcelname'];

$creationdate = $zw->site->time2date($creationdate);
$expirationdate = $zw->site->time2date($expirationdate);

$parq = $zw->SQL->query("SELECT * FROM `{$zw->config['search_db']}`.allparcels WHERE parcelUUID = '$parceluuid'");
$parrow = $zw->SQL->fetch_array($parq);
$loc = $parrow['landingpoint'];

if (!$loc) {
	$locr = "128,128,25";
}else{
	$locr = str_replace("/", ",", $loc);
}

$FL = $zw->grid->uuid2name($creatoruuid);

if ($classifiedflags == "0") {
$classifiedflags = $PG;
}else if ($classifiedflags == "8") {
$classifiedflags = $MATURE;
}else if ($classifiedflags == "40") {
$classifiedflags = $ADULT;
}

if (!$snapshotuuid) {
$pic = "<img src='".$ip2webassets."/asset.php?id=00000000-0000-0000-0000-000000000000' class='pull-right' width='75' height='75'>";
}else{
$pic = "<img src='".$ip2webassets."/asset.php?id=".$snapshotuuid."' class='pull-right' width='75' height='75'>";
}

echo "  <div class='panel panel-default'>
    <div class='panel-heading'>
      <h4 class='panel-title'>
      <a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion' href='#class".$classifieduuid."'>
        <B>".$name."</B> ".$classifiedflags."
      </a>
      </h4>
    </div>
    <div id='class".$classifieduuid."' class='panel-collapse collapse'>
      <div class='panel-body'>
".$pic."
<p>
<a href='secondlife://".$simname."/".$loc."/'>".$parcelname.", ".$simname." (".$locr.")</a><br>
".$description."
<br>
<small>Created: ".$creationdate." by ".$FL."</small>
</p>
      </div>
    </div>
  </div>
";
}

}
if ($type == "destinations" || !$type) {
	echo "<h3>Destinations</h3>";
$popq = $zw->SQL->query("SELECT * FROM `{$zw->config['search_db']}`.popularplaces WHERE name LIKE '%$search%' AND mature < '$m' ORDER BY `dwell` DESC LIMIT 0,100");
while ($popn = $zw->SQL->fetch_array($popq)) {
$parcelUUID = $popn['parcelUUID'];
$name = $popn['name'];
$mature = $popn['mature'];

if ($mature == 0) {
$mature = "$PG";
}else if ($mature == 1) {
$mature = "$MATURE";
}else if ($mature == 2) {
$mature = "$ADULT";
}

$parq = $zw->SQL->query("SELECT * FROM `{$zw->config['search_db']}`.parcels WHERE parcelUUID = '$parcelUUID'");
$parrow = $zw->SQL->fetch_array($parq);
$reguuid = $parrow['regionUUID'];
$landing = $parrow['landingpoint'];
$desc = $parrow['description'];

$simname = $zw->grid->regionname($reguuid);
$usersonregion = $zw->grid->userspersim($reguuid);

echo "  <div class='panel panel-default'>
    <div class='panel-heading'>
      <h4 class='panel-title'>
      <a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion' href='#Dest$parcelUUID'>
	<B>$name</B>
      </a>
      </h4>
    </div>
    <div id='Dest$parcelUUID' class='panel-collapse collapse'>
      <div class='panel-body'>
$desc<br>
<B>Mature Rating:</B> $mature<br>
<B>Avatars on region:</B> $usersonregion
<br><a href='secondlife://$simname/$landing/'>Teleport</a>
     </div>
    </div>
  </div>";

}

}
if ($type == "events" || !$type) {
	echo "<h3>Events</h3>";
$evq = $zw->SQL->query("SELECT * FROM `{$zw->config['search_db']}`.events WHERE name LIKE '%$search%' OR description LIKE '%$search%' AND dateUTC > $now AND eventflags < '$m' ORDER BY `dateUTC` LIMIT 0,100");
while ($evnum = $zw->SQL->fetch_array($evq)) {

$creator = $evnum['creatoruuid'];
$eventtime = $evnum['dateUTC'];
$eventid = $evnum['eventid'];
$eventname = $evnum['name'];
$eventinfo = $evnum['description'];
$event_type = $evnum['eventflags'];
$covercharge = $evnum['covercharge'];
$coveramount = $evnum['coveramount'];
$eventduration = $evnum['duration'];
$simname = $evnum['simname'];
$globalPos = $evnum['globalPos'];

$event_time = $zw->site->time2date($eventtime);

$event_host = $zw->grid->uuid2name($creator);

if ($covercharge == "0") {
$price = "FREE";
}else if ($covercharge == "1") {
$price = $zw->config['GridMoney'].$coveramount;
}

if ($eventduration == "10") $dura = "10 Minutes";
if ($eventduration == "20") $dura = "20 Minutes";
if ($eventduration == "25") $dura = "25 Minutes";
if ($eventduration == "30") $dura = "30 Minutes";
if ($eventduration == "45") $dura = "45 Minutes";
if ($eventduration == "60") $dura = "1 Hour";
if ($eventduration == "90") $dura = "1.5 Hours";
if ($eventduration == "120") $dura = "2 Hours";
if ($eventduration == "150") $dura = "2.5 Hours";
if ($eventduration == "180") $dura = "3 Hours";
if ($eventduration == "1440") $dura = "All Day";

if ($event_type == "0") {
$eventtype = $PG;
}else if ($event_type == "1") {
$eventtype = $MATURE;
}else if ($event_type == "2") {
$eventtype = $ADULT;
}
if ($globalPos == "< , , >" || $globalPos == "<,,>") {
$locr = "128,128,25";
}else{
$locr1 = str_replace("<", "", $globalPos);
$locr = str_replace(">", "", $locr1);
}
$location = $simname." @ ".$locr;

  if ($eventtime >= $now) {
echo "
  <div class='panel panel-default'>
    <div class='panel-heading'>
      <h4 class='panel-title'>
      <a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion' href='#".$eventid."'>
<B>".$eventname." - ".$event_time." ".$eventtype."</B>
      </a>
	</h4>
    </div>
    <div id='".$eventid."' class='panel-collapse collapse'>
      <div class='panel-body'>
".$eventinfo."<br>
<B>Host:</B> ".$event_host."<br>
<B>Duration:</B> ".$dura."<br>
<B>Cover Charge:</B> ".$price."<br>
<B>Location: </B> ".$location."
      </div>
    </div>
  </div>
";
  }else if ($eventtime <= $now) {
  // dont display anything
  }

}

}
if ($type == "groups" || !$type) {
	echo "<h3>Groups</h3>";
$grpq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.os_groups_groups WHERE Name LIKE '%$search%' AND ShowInList = '1' ORDER BY `Name` ASC LIMIT 0,100");
while ($grpn = $zw->SQL->fetch_array($grpq)) {

$GroupID = $grpn['GroupID'];
$Name = $grpn['Name'];
$Charter = $grpn['Charter'];
$InsigniaID = $grpn['InsigniaID'];
$FounderID = $grpn['FounderID'];
$OpenEnrollment = $grpn['OpenEnrollment'];
$MembershipFee = $grpn['MembershipFee'];
$MaturePublish = $grpn['MaturePublish'];

$FL = $zw->grid->uuid2name($FounderID);

$gmq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.os_groups_membership WHERE GroupID = '$GroupID'");
$gmcount = $zw->SQL->num_rows($gmq);

if (!$InsigniaID) {
$pic = "<img src='".$ip2webassets."/asset.php?id=00000000-0000-0000-0000-000000000000' class='pull-right' width='75' height='75'>";
}else{
$pic = "<img src='".$ip2webassets."/asset.php?id=$InsigniaID' class='pull-right' width='75' height='75'>";
}

if ($OpenEnrollment == "1") {
$join = "<a href='secondlife:///app/group/".$GroupID."/join' class='btn btn-success btn-xs'>JOIN</a>";
}else if ($OpenEnrollment == "0") {
$join = "Closed to invites only.";
}

if ($MaturePublish == 0) {
$MaturePublish = "$PG";
}else if ($MaturePublish == 1) {
$MaturePublish = "$MATURE";
}else if ($MaturePublish == 2) {
$MaturePublish = "$ADULT";
}

echo "  <div class='panel panel-default'>
    <div class='panel-heading'>
      <h4 class='panel-title'>
      <a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion' href='#".$GroupID."'>
<B>".$Name."</B> ".$MaturePublish."
      </a>
	</h4>
    </div>
    <div id='".$GroupID."' class='panel-collapse collapse'>
      <div class='panel-body'>
      <p>
        ".$pic."
        ".$Charter."
      </p>
      <p>
        Total Members: ".$gmcount."
      </p>
      <p>
        Enrollment: ".$join."
      </p>
      <p>
        <small>Created by ".$FL."</small>
      </p>
      <p>
        <a href='".$site_address."/groups.php?gid=".$GroupID."'><small>View Group Page</small></a>
      </p>
      </div>
    </div>
  </div>
";
}

}
if ($type == "4sale" || !$type) {
	echo "<h3>For Sale</h3>";
$forsaleq = $zw->SQL->query("SELECT * FROM `{$zw->config['search_db']}`.parcelsales WHERE parcelname LIKE '%$search%' AND mature < '$m' ORDER BY `saleprice` ASC LIMIT 0,100");
while ($forsnum = $zw->SQL->fetch_array($forsaleq)) {

$regionUUID = $forsnum['regionUUID'];
$parcelname = $forsnum['parcelname'];
$parcelUUID = $forsnum['parcelUUID'];
$area = $forsnum['area'];
$saleprice = $forsnum['saleprice'];
$landingpoint = $forsnum['landingpoint'];

$regionname = $zw->grid->regionname($regionUUID);

echo "  <div class='panel panel-default'>
    <div class='panel-heading'>
      <h4 class='panel-title'>
      <a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion' href='#forsale$parcelUUID'>
<B>$parcelname</B>
      </a>
	</h4>
    </div>
    <div id='forsale$parcelUUID' class='accordion-body collapse'>
      <div class='panel-body'>
<p>
<B>Area:</B> $area<br>
<B>Price:</B> V$ $saleprice<br>
<B>Region:</B> $regionname<br>
<a href=''>Teleport</a>
</p>
      </div>
    </div>
  </div>
";
}

}
if ($type == "people" || !$type) {
	echo "<h3>People</h3>";
$pplq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE FirstName LIKE '%$search%' OR LastName LIKE '%$search%' AND UserLevel > '-1' ORDER BY `FirstName` ASC, `LastName` ASC LIMIT 0,100");
while ($pplnum = $zw->SQL->fetch_array($pplq)) {

$uuid = $pplnum['PrincipalID'];
$sFirst = $pplnum['FirstName'];
$sLast = $pplnum['LastName'];

if ($sLast == "Resident") {
$profname = $sFirst;
}else{
$profname = $sFirst.".".$sLast;
}

$online = $zw->grid->online($uuid);
if ($online) {
$onoff = "onlinedot.png";
}else{
$onoff = "offlinedot.png";
}

$profq = $zw->SQL->query("SELECT * FROM `{$zw->config['profile_db']}`.userprofile WHERE useruuid = '$uuid' AND profileMaturePublish < '$m'");
$prow = $zw->SQL->fetch_array($profq);

$show = $prow['profileAllowPublish'];
if ($show == "0") {

$MaturePublish = $prow['profileMaturePublish'];
$abouttext = $prow['profileAboutText'];
$fakepic = $prow['profileImage'];

if ($abouttext) {
$abouttext = htmlspecialchars_decode($abouttext, ENT_QUOTES);
$abouttext = html_entity_decode($abouttext);
$abouttext = substr($abouttext, 0, 125);
$fakelife = "<p>
$abouttext
</p>";
}else if (!$abouttext) {
$fakelife = "";
}

if ($MaturePublish == 0) {
$MaturePublish = "$PG";
}else if ($MaturePublish == 1) {
$MaturePublish = "$MATURE";
}else if ($MaturePublish == 2) {
$MaturePublish = "$ADULT";
}

if (!$fakepic) {
$pic = "<img src='".$ip2webassets."/asset.php?id=00000000-0000-0000-0000-000000000000' class='pull-right' width='75' height='75'>";
}else{
$pic = "<img src='".$ip2webassets."/asset.php?id=".$fakepic."' class='pull-right' width='75' height='75'>";
}

echo "  <div class='panel panel-default'>
    <div class='panel-heading'>
      <h4 class='panel-title'>
      <a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion' href='#people".$uuid."'>
<B>".$sFirst." ".$sLast."</B> <img src='".$site_address."/img/".$onoff."' border='0'>
      </a>
	</h4>
    </div>
    <div id='people".$uuid."' class='panel-collapse collapse'>
      <div class='panel-body'>
".$pic."
".$fakelife."
      </div>
      <div class='panel-footer'><small><a href='".$site_address."/profile.php?u=".$profname."'>View profile</a></small></div>
    </div>
  </div>
";

}else if ($show == "1" || !$show){
}

}

}
if ($type == "places" || !$type) {
	echo "<h3>Places</h3>";
$placeq = $zw->SQL->query("SELECT * FROM `{$zw->config['search_db']}`.parcels WHERE parcelname LIKE '%$search%' OR description LIKE '%$search%' AND public = 'true' ORDER BY `parcelUUID` ASC LIMIT 0,100");
while ($placenum = $zw->SQL->fetch_array($placeq)) {

$regionUUID = $placenum['regionUUID'];
$parcelname = $placenum['parcelname'];
$parcelUUID = $placenum['parcelUUID'];
$landingpoint = $placenum['landingpoint'];
$description = $placenum['description'];
$searchcategory = $placenum['searchcategory'];
$mature = $placenum['mature'];

$regionname = $zw->grid->regionname($regionUUID);
$usersonregion = $zw->grid->userspersim($regionUUID);

if ($mature == "PG") {
$mr = "1";
$mat = "$PG";
}
if ($mature == "Mature") {
$mr = "2";
$mat = "$MATURE";
}
if ($mature == "Adult") {
$mr = "3";
$mat = "$ADULT";
}

if ($mr <= $m) {
echo "  <div class='panel panel-default'>
    <div class='panel-heading'>
      <h4 class='panel-title'>
      <a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion' href='#places".$parcelUUID."'>
<B>".$parcelname."</B>
      </a>
	</h4>
    </div>
    <div id='places".$parcelUUID."' class='panel-collapse collapse'>
      <div class='panel-body'>
<p>
".$pic."
".$description."<br>
<B>Mature Rating:</B> ".$mat."<br>
<B>Avatars on region:</B> ".$usersonregion."
<br>
<a href='secondlife://".$regionname."/".$landingpoint."/'>Teleport</a>
</p>
      </div>
    </div>
  </div>
";
}else{
}

}

}
?>
</div>
</TD>
</TR>
</TABLE>
<?php
include ('inc/footer.php');
?>