<?php
$page_title = "Send Money";
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');

if ($user_uuid) {
$first = $zw->Security->make_safe($_GET['first']);
$last = $zw->Security->make_safe($_GET['last']);

$money = $zw->config['Money'];
$uuid = $user_uuid;
$nullkey = "00000000-0000-0000-0000-000000000000";
$now = time();

$submit = $zw->Security->make_safe($_POST['submit']);
$giving2first = $zw->Security->make_safe($_POST['firstname']);
$giving2last = $zw->Security->make_safe($_POST['lastname']);
$giving2amount = $zw->Security->make_safe($_POST['amount']);

if ($giving2first && $giving2last && is_numeric($giving2amount) && $submit) {
	$checkbalq = $zw->SQL->query("SELECT * FROM `{$zw->config['money_db']}`.balances WHERE user = '$uuid'");
	$checkbalr = $checkbalq->fetch_array(MYSQLI_BOTH);
	$checkviewerbal = $checkbalr['balance'];
	$checkbalq->close();
	if ($checkviewerbal >= $giving2amount) {
		$avicheckq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE FirstName = '$giving2first' AND LastName = '$giving2last'");
		$avicheckn = $zw->SQL->num_rows($avicheckq);
		if ($avicheckn) {
			$avicheckr = $zw->SQL->fetch_array($avicheckq);
			$giving2uuid = $avicheckr['PrincipalID'];
			$give2uuid = $giving2uuid;
			$give2balq = $zw->SQL->query("SELECT * FROM `{$zw->config['money_db']}`.balances WHERE user = '$give2uuid'");
			$give2balr = $zw->SQL->fetch_array($give2balq);
			$give2bal = $give2balr['balance'];
			$givingbal = $give2bal + $giving2amount;
			$takingbal = $checkviewerbal - $giving2amount;
			$ugiveq = $zw->SQL->query("UPDATE `{$zw->config['money_db']}`.balances SET balance = '$givingbal' WHERE user = '$give2uuid'");
			$utakeq = $zw->SQL->query("UPDATE `{$zw->config['money_db']}`.balances SET balance = '$takingbal' WHERE user = '$uuid'");
			if ($ugiveq && $utakeq) {
				echo $zw->config['GridMoney']." ".$giving2amount." has been taken from your account and given to ".$giving2first." ".$giving2last."<br>
				If this is incorrect please send a support ticket for this to be corrected by a grid admin.";
				$newuuid = $zw->getNewUUID();
				$newsecureuuid = $zw->getNewUUID();
				$zw->SQL->query("INSERT INTO `{$zw->config['money_db']}`.transactions (UUID, sender, receiver, amount, objectUUID, regionHandle, type, time, secure, status, commonName, description) VALUES ('$newuuid', '$uuid', '$give2uuid', '$giving2amount', '$nullkey', '1', '5009', '$now', '$newsecureuuid', '0', '', 'Avatar to avatar transaction via website')");
			}else{
				echo "Error with sending money to ".$giving2first." ".$giving2last.". Please make sure you have the correct name.";
			}
		}else{
			echo "Unable to find that user.";
		}
	}else{
		echo "Your balance is less then the amount you want to give.";
	}
}

if ($money == "true") {
$balq = $zw->SQL->query("SELECT * FROM `{$zw->config['money_db']}`.balances WHERE user = '$uuid'");
$balr = $zw->SQL->fetch_array($balq);
$viewerbal = $balr['balance'];
$viewername = $zw->grid->uuid2name($user_uuid);
?>
<h3>
Logged in as <?php echo $viewername; ?><br>
Your balance is <?php echo $zw->config['GridMoney']." ".$viewerbal; ?>
</h3>
Please enter the avatar's first and last name in the fields below as well as the amount you like to give them.<br>
<form method='post' action='' class="form-horizontal" role="form">
<div class="form-group">
	<div class="col-sm-10">
		<input type="text" name="firstname" value="<?php echo $first; ?>" class="form-control" placeholder="First Name">
	</div>
</div>
<div class="form-group">
	<div class="col-sm-10">
		<input type="text" name="lastname" value="<?php echo $last; ?>" class="form-control" placeholder="Last Name">
	</div>
</div>
<div class="form-group">
	<div class="col-sm-10">
		<div class="input-group">
			<span class="input-group-addon"><?php echo $zw->config['GridMoney']; ?></span>
			<input type="number" name="amount" value="" class="form-control" placeholder="<?php echo $zw->config['GridMoney']; ?> amount to give">
		</div>
	</div>
</div>
<div class="form-group">
	<div class="col-sm-10">
		<input type="submit" name="submit" value="Submit" class="btn btn-success">
	</div>
</div>
</form>
<?php
}else{
	echo $zw->site->displayalert("This server currently does not have a money module installed.", "warning");
}
}else if(!$user_uuid) {
echo $zw->site->displayalert("You need to be logged into the ".$zw->config['GridName']." website to send money.", "danger");
} // ends if ($user_uuid)
include ('inc/footer.php');
?>