<?php
$page_title = "HipChat Support";
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');

if ($zw->config['HipChatToken'] != "") {
	$url = $zw->hipchat->fetchroomstatus("html");
	if ($url != "") {
		if (!$user) {
			$username = "";
		}else{
			$username = $user;
		}
		echo "<iframe src='".$url."?name=".$username."' frameborder='0' width='100%' height='600' scrolling='no' seamless=''></iframe>";
	}else{
		echo $zw->site->displayalert("Support staff is currently not online. Please check back later.", "danger");
	}
}else{
	echo $zw->site->displayalert("This grid is not setup a HipChat Live Chat Support room. Please check back later.", "danger");
}

include ('inc/footer.php');
?>