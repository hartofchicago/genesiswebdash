<?php
$page_title = "My Regions";
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');
if ($user_uuid) {

$r = $zw->Security->make_safe($_GET['r']);
$page = $zw->Security->make_safe($_GET['page']);

$landingpoint = "128/128/25";
$tplp = "128,128,25";

$submit = $zw->Security->make_safe($_POST['restartregion']);
$regionID = $zw->Security->make_safe($_POST['regionid']);
$tp = $zw->Security->make_safe($_POST['tp']);
$postregionname = $zw->Security->make_safe($_POST['postregionname']);

echo $zw->site->displayalert("Region restarts do not currently work. Sorry for any inconvenience.", "danger");
if ($submit == "Restart Region") {
	//echo $zw->site->displayalert("Sim is restarting. Please allow up to 2 minutes before logging in.", "warning");
	//echo $zw->remote->restartsim($regionID);
}
if ($tp == "Instant TP" && $user_uuid != "") {
  $data = "TP=".$user_uuid."=".$postregionname."=<".$tplp.">";
  $zw->lsl->send2server($data);
  echo $zw->site->displayalert("Your avatar has been teleported to ".$postregionname, "success");
}

$totaloffset = "50";
if (!$page || $page == "0" || $page == "1") {
$page = "1";
$offset = "0";
}else if ($page == "2") {
$offset = $totaloffset;
}else if ($page >= "2") {
$offset = $totaloffset * $page;
}

$regionq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.regions WHERE owner_uuid = '$user_uuid'");
$regions = $zw->SQL->num_rows($regionq);

//echo $zw->remote->admin_broadcast("Testing admin broadcast");
?>
<small>Some of your regions may not be in this list for 2 reasons.<br>
A) The grid admin(s) may not of put your region online yet or<br>
B) Your region is currently being restarted or is offline for maintenance.</small><br>
<p><B>Total Regions:</B> <?php echo $regions; ?></p>
<div class='table-responsive'>
<table class='table table-hover table-bordered table-striped'>
<thead>
<tr>
<th>Region Name</th>
<th></th>
</tr>
</thead>
<tbody>
<?php
$regionlistq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.regions WHERE owner_uuid = '$user_uuid' ORDER BY `regionName` ASC LIMIT $offset, $totaloffset");
while ($regionlistr = $zw->SQL->fetch_array($regionlistq)) {
$regionuuid = $regionlistr['uuid'];
$regionname = $regionlistr['regionName'];
$owner = $regionlistr['owner_uuid'];
$sizeX = $regionlistr['sizeX'];
$sizeY = $regionlistr['sizeY'];
$serverURI = $regionlistr['serverURI'];
$last_seen = $zw->site->time2date($regionlistr['last_seen']);
$ownername = $zw->grid->uuid2name($owner);
$usersonsim = $zw->grid->userspersim($regionuuid);
$ownerlinked = str_replace(" ", ".", $ownername);

if ($zw->config['InWorldServer'] != "" && $zw->grid->online($user_uuid)) {
	$tplink = "<input type='submit' name='tp' value='Instant TP' class='btn btn-sm btn-info'>";
}else if ($zw->config['InWorldServer'] == "" || $zw->grid->online($user_uuid) == false) {
	$tplink = "<a href='secondlife://".$regionname."/".$landingpoint."/'>Teleport</a>";
}
echo "<tr>
<td>
".$regionname."
 <small>(".$sizeX." x ".$sizeY.") - (<B>Users on Sim:</B> ".$usersonsim.")</small>
</td>
<td>
<form method='post' action='".$site_address."/myregions.php?r=".$r."' class='form' role='form'>
<input type='hidden' name='regionid' value='".$regionuuid."'>
<input type='hidden' name='postregionname' value='".$regionname."'>
<input type='submit' name='submit' value='Restart Region' class='btn btn-sm btn-danger'>
".$tplink."
</form>
</td>
</tr>";
}
?>
</tbody>
</table>
</div>
<?php
$pager = $site_address."/myregions.php?r=".$r;
$tbl_name = "`{$zw->config['robust_db']}`.regions WHERE owner_uuid = '$user_uuid'";
echo $zw->pagination->paging($tbl_name, $pager, $totaloffset);
}else{
	echo $zw->site->displayalert("You must be logged in to see your regions.", "danger");
}
include ('inc/footer.php');
?>