<?php
define('ZW_IN_SYSTEM', true);
require_once('inc/headerless.php');

$ShowStats = $zw->config['ShowStats'];

$channel = $zw->Security->make_safe($_GET['channel']);
$grid = $zw->Security->make_safe($_GET['grid']);
$lang = $zw->Security->make_safe($_GET['lang']);
$os = $zw->Security->make_safe($_GET['os']);
$sourceid = $zw->Security->make_safe($_GET['sourceid']);
$version = $zw->Security->make_safe($_GET['version']);

if ($zw->grid->gridonline()) {
	$onoff = "Online";
	$onoffcolour = "#00EE00";
}else{
	$onoff = "Offline";
	$onoffcolour = "#FA1D2F";
}

$defsize = "256";
$defsqm = "65536";

$fiveago = $now - 300;
$onlineq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.Presence WHERE RegionID != '{$zw->nullkey}'");
$online = $zw->SQL->num_rows($onlineq);

$totalq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts");
$totalc = $zw->SQL->num_rows($totalq);

$monthago = $now - 2592000;
$latestq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.GridUser WHERE Login > '$monthago' AND Login != '0'");
$latestc = 0;
while ($latestr = $zw->SQL->fetch_array($latestq)) {
	$checkmuuid = $latestr['UserID'];
	$checkusermq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$checkmuuid'");
	$checkusermn = $zw->SQL->num_rows($checkusermq);
	if ($checkusermn) {
		$latestc++;
	}
}
$hoursago = $now - 86400;
$latest24q = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.GridUser WHERE Login > '$hoursago' AND Login != '0'");
$latest24c = 0;
while ($latest24r = $zw->SQL->fetch_array($latest24q)) {
	$check24uuid = $latest24r['UserID'];
	$checkuser24q = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$check24uuid'");
	$checkuser24n = $zw->SQL->num_rows($checkuser24q);
	if ($checkuser24n) {
		$latest24c++;
	}
}

$sizextotal = "";
$sizeytotal = "";
$sizetotal = "";
$totalsingleregions = "";
$regionq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.regions");
$regionc = $zw->SQL->num_rows($regionq);
while($regionr = $zw->SQL->fetch_array($regionq)) {
	$x = $regionr['sizeX'];
	$y = $regionr['sizeY'];
	$sizextotal += $x;
	$sizeytotal += $y;
	$xytotal = $x * $y;
	$sizetotal += $xytotal;
	if ($xytotal >= $defsqm) {
		$totalsingleregions += $xytotal / $defsqm;
	}else if($xytotal == $defsqm) {
		++$totalsingleregions;
	}
}
/*
$destecho = "";
$destq = $zw->SQL->query("SELECT * FROM `{$zw->config['search_db']}`.popularplaces ORDER BY `name` ASC LIMIT 0,10");
while ($destr = $zw->SQL->fetch_array($destq)) {
	$destname = $destr['name'];
	$dname = rawurlencode($destname);
	$destecho .= "<tr><td align='center'><a href='secondlife://$dname' target='_self' style='text-decoration: none;'><h4>$destname</h4></a></td></tr>";
}
*/
if ($logoimg) {
	$logo = "<img src='" . $zw->config['Logo'] . "' border='0'>";
}else{
	$logo = "<h1>" . $zw->config['GridName'] . "</h1>";
}

$dir = "bgimg"; // directory aka folder where your background images aka screenshots will go
if (is_dir($dir))
{
	if ($dh = opendir($dir))
	{
		while (false !== ($file = readdir($dh)))
		{
			if ($file == '.' || $file == '..') { 
			}else{
			$jbgimg .= "'" . $file . "', ";
			$cbgimg = $file;
			}
		}
	closedir($dh);
	}
	$jquerypics .= $jbgimg."~~";
	$jquerypics = str_replace(", ~~", "", $jquerypics);
}

$classifies = file_get_contents($site_address."/classifiedapi.php?type=website&size=225x225");

if ($_GET['stat']) {
	$apiarray = array('Status' => $onoff, "UsersOnline" => $online, 'TotalUsers' => $totalc, 'TotalRegions' => $regionc, 'TotalSingleRegions' => $totalsingleregions, 'Active30Days' => $latestc, 'Active24Hours' => $latest24c);
	$stat = $_GET['stat'];
	if ($stat == "online") {
		echo $onoff;
	}
	if ($stat == "usersonline" && $ShowStats == "true") {
		echo $online;
	}
	if ($stat == "totalusers" && $ShowStats == "true") {
		echo $totalc;
	}
	if ($stat == "totalregions" && $ShowStats == "true") {
		echo $regionc;
	}
	if ($stat == "active30days" && $ShowStats == "true") {
		echo $latestc;
	}
	if ($stat == "totalsingleregions" && $ShowStats == "true") {
		echo $totalsingleregions;
	}
	if ($stat == "json") {
		echo json_encode($apiarray);
	}
	if ($stat == "xmlrpc") {
		echo xmlrpc_encode($apiarray);
	}
	if ($stat == "lsl") {
		echo $onoff."=".$online."=".$totalc."=".$regionc."=".$latest24c."=".$latestc."=".$totalsingleregions;
	}
}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Splash Screen - <?php echo $zw->config['GridName']; ?></title>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<script>
var newBg = [<?php echo $jquerypics; ?>];
var path="<?php echo $dir; ?>/";
var i = 0;
var rotateBg = setInterval(function(){
    $('body').css('background-image' ,  "url('" +path+newBg[i]+ "')");
    i++;
}, 20000);
</script>
<script>
var newBg = [<?php echo $jquerypics; ?>];
var path="<?php echo $dir; ?>/";
var i = 0;
var rotateBg = setInterval(function(){
    $('body').css('backgroundImage' ,  "url('" +path+newBg[i]+ "')");
    i++;
}, 5000);
</script>
<style>
body
{
background-image: url('<?php echo $dir; ?>/5276e598-7b7f-45b2-adf0-434348ab88e5.jpg');
background-repeat: no-repeat;
padding-top: 25px;
padding-bottom: 0px;
background-size:100% 110%;
}
.ceil {
background-color: #1c1c1c;
color: #ffffff;
-webkit-border-radius: 8px;
-moz-border-radius: 8px;
overflow:hidden;
border-radius: 10px; 
overflow: hidden;
}
</style>
</head>
<body>
<div class="container-fluid">
	<div class='row'>
		<div class='col-md-2 ceil'>
			<center>
				<?php echo $logo; ?>
			</center>
			<br><?php echo $classifies; ?>
		</div>
		<div class='col-md-8'>
			<!-- Blank middle spot -->
		</div>
		<div class='col-md-2 ceil'>
			<B><?php echo "<font color='".$onoffcolour."'>Grid is ".$onoff."</font>"; ?></B><br>
			<?php
			if ($ShowStats == "true") {
			?>
			Users in world: <?php echo $online; ?><br>
			Online Last 30 days: <?php echo $latestc; ?><br>
			Online Last 24 hours: <?php echo $latest24c; ?><br>
			Total Users: <?php echo $totalc; ?><br>
			Total Regions: <?php echo $regionc; ?> <small>(<?php echo $totalsingleregions; ?>*)</small><br>
			<small>* Total count for all sims as if they were <?php echo $defsize." by ".$defsize; ?> meters</small>
			<?php
			}
			echo $zw->site->twitter();
			?>
		</div>
	</div>
</div>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</body>
</html>
<?php
} // ends if ($_GET['stat'])
?>
