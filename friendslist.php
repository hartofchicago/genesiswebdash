<?php
$page_title = "Friends List";
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');

$c = $zw->Security->make_safe($_GET['c']);
$page = $zw->Security->make_safe($_GET['page']);

$friend = $zw->Security->make_safe($_POST['friend']);
$delfriend = $zw->Security->make_safe($_POST['delfriend']);
$addfriend = $zw->Security->make_safe($_POST['addfriend']);
$declinefriend = $zw->Security->make_safe($_POST['declinefriend']);
$acceptproposal = $zw->Security->make_safe($_POST['acceptproposal']);
$declineproposal = $zw->Security->make_safe($_POST['declineproposal']);
$cancelproposal = $zw->Security->make_safe($_POST['cancelproposal']);
$divorce = $zw->Security->make_safe($_POST['divorce']);
$propose = $zw->Security->make_safe($_POST['propose']);
$submit = $zw->Security->make_safe($_POST['submit']);
$addrequest = $zw->Security->make_safe($_POST['addrequest']);

if ($user_uuid) {
	if ($friend) {
		if ($delfriend == "Delete") {
			$delmsgq = $zw->SQL->query("DELETE FROM `{$zw->config['robust_db']}`.Friends WHERE PrincipalID = '$user_uuid' AND Friend = '$friend'");
			$delmsgq .= $zw->SQL->query("DELETE FROM `{$zw->config['robust_db']}`.Friends WHERE PrincipalID = '$friend' AND Friend = '$user_uuid'");
			if ($delmsgq) {
				echo $zw->site->displayalert("Friend deleted!", "success");
			}else{
				echo $zw->site->displayalert("Friend was not deleted!", "danger");
			}
		}
		if ($addfriend == "Accept") {
			$addfqu = $zw->SQL->query("UPDATE `{$zw->config['robust_db']}`.Friends SET Flags = '1' WHERE Friend = '$friend' AND PrincipalID = '$user_uuid'");
			if ($addfqu) {
				$addfqi = $zw->SQL->query("INSERT INTO `{$zw->config['robust_db']}`.Friends (PrincipalID, Friend, Flags, Offered) VALUES ('$friend', '$user_uuid', '1', '0')");
				if ($addfqi) {
					echo $zw->site->displayalert("Friend Added!", "success");
				}else{
					echo $zw->site->displayalert("Friend was not added!", "danger");
				}
			}else{
				echo $zw->site->displayalert("Friend was not added!", "danger");
			}
		}
		if ($declinefriend == "Decline") {
			$decq = $zw->SQL->query("DELETE FROM `{$zw->config['robust_db']}`.Friends WHERE Friend = '$friend' AND PrincipalID = '$user_uuid'");
			if ($decq) {
				echo $zw->site->displayalert("Friend requested has been declined!", "success");
			}else{
				echo $zw->site->displayalert("Friend request was not declined!", "danger");
			}
		}
		if ($addrequest == "Add Friend") {
			$addrqi = $zw->SQL->query("INSERT INTO `{$zw->config['robust_db']}`.Friends (PrincipalID, Friend, Flags, Offered) VALUES ('$friend', '$user_uuid', '0', '0')");
			if ($addrqi) {
				echo $zw->site->displayalert("Friend request sent!", "success");
			}else{
				echo $zw->site->displayalert("Friend request was not sent!", "danger");
			}
		}
		if ($acceptproposal == "Accept Proposal") {
			$properq1 = $zw->SQL->query("UPDATE `{$zw->config['robust_db']}`.userprofile SET profilePartner = '$friend' WHERE useruuid = '$user_uuid'");
			$properq2 = $zw->SQL->query("UPDATE `{$zw->config['robust_db']}`.userprofile SET profilePartner = '$user_uuid' WHERE useruuid = '$friend'");
			if ($properq1 && $properq2) {
				$zw->SQL->query("DELETE FROM `{$zw->config['db_prefix']}weddings` WHERE UserID = '$friend' AND ProposeTo = '$user_uuid'");
				echo $zw->site->displayalert("Proposal accepted! I now pronounce you (husband and wife)/(husband and husband)/(wife and wife). You may kiss the bride.", "success");
			}else{
				echo $zw->site->displayalert("Umm i seem to not be able to get you twos hitched. Try again later.", "danger");
			}
		}
		if ($declineproposal == "Decline Proposal") {
			$decpq = $zw->SQL->query("DELETE FROM `{$zw->config['db_prefix']}weddings` WHERE UserID = '$friend' AND ProposeTo = '$user_uuid'");
			if ($decpq) {
				echo $zw->site->displayalert("Aaww ok declined the wedding proposal.", "success");
			}else{
				echo $zw->site->displayalert("Wow unable to decline proposal.", "danger");
			}
		}
		if ($cancelproposal == "Cancel Proposal") {
			$canceledq = $zw->SQL->query("DELETE FROM `{$zw->config['db_prefix']}weddings` WHERE UserID = '$user_uuid' AND ProposeTo = '$friend'");
			if ($canceledq) {
				echo $zw->site->displayalert("Aaww ok canceled the wedding proposal.", "success");
			}else{
				echo $zw->site->displayalert("Wow unable to canceled proposal.", "danger");
			}
		}
		if ($divorce == "Divorce") {
			$divorceq1 = $zw->SQL->query("UPDATE `{$zw->config['robust_db']}`.userprofile SET profilePartner = '00000000-0000-0000-0000-000000000000' WHERE useruuid = '$user_uuid'");
			$divorceq2 = $zw->SQL->query("UPDATE `{$zw->config['robust_db']}`.userprofile SET profilePartner = '00000000-0000-0000-0000-000000000000' WHERE useruuid = '$friend'");
			if ($divorceq1 && $divorceq2) {
				echo $zw->site->displayalert("You twos are now divorced", "success");
			}else{
				echo $zw->site->displayalert("Unable to file divorce papers. Try again later.", "danger");
			}
		}
		if ($propose == "Propose") {
			$propiq = $zw->SQL->query("INSERT INTO `{$zw->config['db_prefix']}weddings` (UserID, ProposeTo) VALUES ('$user_uuid', '$friend')");
			if ($propiq) {
				echo $zw->site->displayalert("Proposal sent. Please wait for your partner to accept.", "success");
				$postingmsg = $user." has proposed to you.\nPlease visit ".$site_address."/friendslist.php to accept or decline the proposal";
				$data = "MSG=".$postingmsg."=".$friend;
				$zw->lsl->send2server($data);
			}else{
				echo $zw->site->displayalert("Unable to send proposal. Try again later.", "danger");
			}
		}
	}
	if ($submit == "Send Message") {
		$aviuuid = $zw->Security->make_safe($_POST['aviuuid']);
		$postmsg = $zw->Security->make_safe($_POST['postmsg']);
		if ($postmsg != "") {
			$postingmsg = "ZMW Message from ".$user.":\n".$postmsg;
			$data = "MSG=".$postingmsg."=".$aviuuid;
			$zw->lsl->send2server($data);
			echo $zw->site->displayalert("Message sent.", "success");
		}else{
			echo $zw->site->displayalert("Message field was empty. Message not sent.", "warning");
		}
	}
$totaloffset = "10"; // number of results to display
if (!$page || $page == "0" || $page == "1") {
$page = "1";
$offset = "0";
}else if ($page == "2") {
$offset = $totaloffset;
}else if ($page >= "2") {
$offset = $totaloffset / 2 * $page;
}
?>
<h2>Friends List Manager</h2>
<small>
**Your friend will need to relog for them to see you on their list IF they are in world when you accept their request.**<br>
**Deleting friends here will delete them in world and deletes you from their list.**<br>
**If deleting your partner please click the Divorce button first**<br>
**Blocking someone wont work due to a bug with this in opensim**<br>
</small>
<div class="table-responsive">
<table class='table table-striped table-hover table-condensed'>
<thead>
<tr>
<th><small><B>NAME</B></small></th>
<th><small><B>ACTION</B></small></th>
</tr>
</thead>
<tbody>
<?php
$approvalq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.Friends WHERE PrincipalID = '$user_uuid' AND Flags = '0' LIMIT 0, $totaloffset");
$approvaln = $zw->SQL->num_rows($approvalq);
if ($approvaln) {
	while($approvalr = $zw->SQL->fetch_array($approvalq)) {
		$PrincipalID = $approvalr['Friend'];
		$Requestername = $zw->grid->uuid2name($PrincipalID);
		$Online = $zw->grid->online($PrincipalID);
		if ($Online) {
			$isOnline = "Online";
			$isOnlineStyle = "success";
		}else{
			$isOnline = "Offline";
			$isOnlineStyle = "muted";
		}
echo "
<tr>
<td><span class='text-".$isOnlineStyle."'><B>".$Requestername."</B> <small>".$isOnline."</small></span></td>
<td>
<form method='post' action='friendslist.php?c=".$c."&page=".$page."' class='form-horizontal' role='form'>
<input type='hidden' name='friend' value='".$PrincipalID."'>
<input type='submit' name='addfriend' value='Accept' class='btn btn-success btn-sm'>
<input type='submit' name='declinefriend' value='Decline' class='btn btn-warning btn-sm'>
</form>
</td></tr>
";
	} // end while loop
}
$friendlistq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.Friends WHERE PrincipalID = '$user_uuid' AND Flags > '0' LIMIT $offset, $totaloffset");
$friendlistn = $zw->SQL->num_rows($friendlistq);
if ($friendlistn) {
	while($friendlistr = $zw->SQL->fetch_array($friendlistq)) {
		$Frienduuid = $friendlistr['Friend'];
		$Friendname = $zw->grid->uuid2name($Frienduuid);
		$Online = $zw->grid->online($Frienduuid);
		if ($Online) {
			$isOnline = "Online";
			$isOnlineStyle = "success";
		}else{
			$isOnline = "Offline";
			$isOnlineStyle = "muted";
		}
		if ($zw->site->checkproposal($Frienduuid, $user_uuid) && $Friendname != "HG") {
			$propbutton = "<input type='submit' name='acceptproposal' value='Accept Proposal' class='btn btn-success btn-sm'>
			<input type='submit' name='declineproposal' value='Decline Proposal' class='btn btn-danger btn-sm'>";
		}else if ($zw->site->checkproposal($user_uuid, $Frienduuid) && $Friendname != "HG") {
			$propbutton = "<input type='submit' name='cancelproposal' value='Cancel Proposal' class='btn btn-danger btn-sm'>";
		}else if ($zw->grid->checkpartner($user_uuid, $Frienduuid) && $Friendname != "HG") {
			$propbutton = "<input type='submit' name='divorce' value='Divorce' class='btn btn-warning btn-sm'>";
		}else{
			if ($zw->grid->ismarried($Frienduuid) || $zw->grid->ismarried($user_uuid) || $Friendname == "HG") {
				$propbutton = "";
			}else{
				$propbutton = "<input type='submit' name='propose' value='Propose' class='btn btn-success btn-sm'>";
			}
		}
		if ($Friendname == "HG") {
			$Friendname = "HG Friend";
			$delbutton = "";
			$moneylink = "";
			$sendmsgbutton = "";
			$sendmsgmodel = "";
		}else{
			$sFriendnameexplode = explode(" ", $Friendname);
			$sFriendnamefirst = $sFriendnameexplode[0];
			$sFriendnamelast = $sFriendnameexplode[1];
			if ($zw->config['Money'] == "true") {
				$monelinker = $zw->config['SiteAddress']."/givemoney.php?first=".$sFriendnamefirst."&last=".$sFriendnamelast;
				$moneylink = "<a href='".$monelinker."' class='btn btn-info btn-sm'>Send Money</a>";
			}else{
				$moneylink = "";
			}
			$delbutton = "<input type='submit' name='delfriend' value='Delete' class='btn btn-danger btn-sm'>";
			$sendmsgbutton = "<button class='btn btn-link btn-sm' data-toggle='modal' data-target='#msgModal'>Send Message</button>";
			$sendmsgmodel = "
			<div class='modal fade' id='msgModal' tabindex='-1' role='dialog' aria-labelledby='msgModalLabel' aria-hidden='true'>
			  <div class='modal-dialog'>
			    <div class='modal-content'>
			      <div class='modal-header'>
			        <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
			        <h4 class='modal-title' id='msgModalLabel'>Send ".$FromName." a message</h4>
			      </div>
			      <div class='modal-body'>
			      	<form method='post' action='friendslist.php?c=".$c."&page=".$page."' class='form' role='form'>
					<input type='hidden' name='aviuuid' value='".$Frienduuid."'>
			        <input type='text' name='postmsg' value='' class='form-control' placeholder='Send a message'>
					<input type='submit' name='submit' value='Send Message' class='btn btn-info btn-sm'>
					</form>
			      </div>
			      <div class='modal-footer'>
			        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
			      </div>
			    </div>
			  </div>
			</div>";
		}

		if ($zw->config['InWorldServer'] != "") {
			$msgbuttons = $sendmsgbutton.$sendmsgmodel;
		}else{
			$msgbuttons = "";
		}
echo "
<tr>
<td>
<span class='text-".$isOnlineStyle."'>
<B>".$Friendname."</B> <small>".$isOnline."</small> ".$msgbuttons."
</span>
</td>
<td>
<form method='post' action='friendslist.php?c=".$c."&page=".$page."' class='form-horizontal' role='form'>
<input type='hidden' name='friend' value='".$Frienduuid."'>
".$delbutton." ".$propbutton." ".$moneylink."
</form>
</td></tr>
";
	} // end while loop
}

echo "</tbody>
</table>
</div>
";
$pager = $site_address."/friendslist.php?c=".$c;
$tbl_name = $zw->config['robust_db'].".Friends WHERE PrincipalID = '$user_uuid' AND Flags > '0'";
echo $zw->pagination->paging($tbl_name, $pager, $totaloffset);

}else if (!$user_uuid) {
} // ends if ($user_uuid)

include('inc/footer.php');
?>