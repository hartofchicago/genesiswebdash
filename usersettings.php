<?php
$page_title = "User Settings";
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');

if ($user_uuid) {

$type = $zw->Security->make_safe($_GET['type']);

$uq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$user_uuid'");
$ur = $zw->SQL->fetch_array($uq);
$firstname = $ur['FirstName'];
$lastname = $ur['LastName'];
$email = $ur['Email'];
$reztime = $ur['Created'];
$usertitle = $ur['UserTitle'];
$rezday = $zw->site->time2date($reztime);

  $checkdbq = $zw->SQL->query("SELECT * FROM `{$zw->config['db_prefix']}users` WHERE uuid = '$user_uuid'");
  $checkdbn = $zw->SQL->num_rows($checkdbq);
  if ($checkdbn) {
    $zmwuser = $zw->SQL->fetch_array($checkdbq);
  }else{
    if (!$usertitle || $usertitle == "Local User" || $usertitle == "Administrator" || $usertitle == "Admin User") {
      $syncstatus = "approved";
    }else if ($usertitle == "Pending") {
      $syncstatus = "waiting";
    }
    $zw->SQL->query("INSERT INTO `{$zw->config['db_prefix']}users` (uuid, name, activationstatus) VALUES ('$user_uuid','$user','$syncstatus')");
  }

$sitestyle = $zmwuser['style'];
$token = $zmwuser['token'];

  $submit = $zw->Security->make_safe($_POST['submit']);
  if ($submit == "Save Settings") {
  $saveemail = $zw->Security->make_safe($_POST['saveemail']);
  $savesitestyle = $zw->Security->make_safe($_POST['savesitestyle']);
    if ($saveemail != $email) {
      $findme = '@';
      $echeck = strpos($saveemail, $findme);
      if ($echeck !== false) {
          $changeemail = $zw->SQL->query("UPDATE `{$zw->config['robust_db']}`.UserAccounts SET Email = '$saveemail' WHERE PrincipalID = '$user_uuid'");
          if ($changeemail) {
            echo $zw->site->displayalert("Email address has been changed.", "success");
          }else{
            echo $zw->site->displayalert("Unable to change email address.", "danger");
          }
      }else if ($echeck === false) {
        echo $zw->site->displayalert('Incorrect email address', "danger");
      }
    }
    if ($savesitestyle != $sitestyle) {
      $updatestyle = $zw->SQL->query("UPDATE `{$zw->config['db_prefix']}users` SET style = '$savesitestyle' WHERE uuid = '$user_uuid'");
      if ($updatestyle) {
        echo $zw->site->displayalert("Site Style has been updated.", "success");
      }else{
        echo $zw->site->displayalert("Unable to update your site's style.", "danger");
      }
    }
  }
  if ($submit == "Update Profile") {
    $aboutfl = $zw->Security->make_safe($_POST['aboutfl']);
    $aboutrl = $zw->Security->make_safe($_POST['aboutrl']);
    $skills = $zw->Security->make_safe($_POST['skills']);
    $languages = $zw->Security->make_safe($_POST['languages']);
    $profileurl = $zw->Security->make_safe($_POST['profileurl']);
    $updateprofq = $zw->SQL->query("UPDATE `{$zw->config['robust_db']}`.userprofile SET profileURL = '$profileurl', profileLanguages = '$languages', profileSkillsText = '$skills', profileAboutText = '$aboutfl', profileFirstText = '$aboutrl' WHERE useruuid = '$user_uuid'");
    if ($updateprofq) {
      echo $zw->site->displayalert("Profile updated.", "success");
    }else{
      echo $zw->site->displayalert("Unable to update your profile.", "danger");
    }
  }
  $generatetoken = $zw->Security->make_safe($_POST['generatetoken']);
  if ($generatetoken) {
    $istoken = $zw->api->generatetoken();
    if ($istoken != "0") {
      $token = $istoken;
      echo $zw->site->displayalert("API Token generated.<br>New api token is: ".$token, "success");
    }else{
      $token = $token;
      echo $zw->site->displayalert("Unable to generate a new API token for you at this time.", "danger");
    }
  }

if (!$type || $type == "settings") {
  $sactive = "class='active'";
  $pactive = "";
  $aactive = "";
  $iactive = "";
}else if ($type == "profile") {
  $sactive = "";
  $pactive = "class='active'";
  $iactive = "";
  $aactive = "";
}else if ($type == "inv") {
  $sactive = "";
  $pactive = "";
  $iactive = "class='active'";
  $aactive = "";
}else if ($type == "api") {
  $sactive = "";
  $pactive = "";
  $iactive = "";
  $aactive = "class='active'";
}else{
  $sactive = "";
  $pactive = "";
  $iactive = "";
  $aactive = "";
}
?>
<h3>User Control Panel</h3>
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li <?php echo $sactive; ?>><a href="<?php echo $site_address; ?>/usersettings.php?type=settings">Settings</a></li>
  <li <?php echo $pactive; ?>><a href="<?php echo $site_address; ?>/usersettings.php?type=profile">Profile</a></li>
  <li <?php echo $iactive; ?>><a href="<?php echo $site_address; ?>/usersettings.php?type=inv">Inventory</a></li>
  <li <?php echo $aactive; ?>><a href="<?php echo $site_address; ?>/usersettings.php?type=api">API</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
<?php if ($type == "settings" || !$type) { ?>
    <form class="form-horizontal" method="post" action="<?php echo $site_address; ?>/usersettings.php?type=<?php echo $type; ?>" role="form">
      <div class="form-group">
          <label for="inputEmail" class="col-sm-2 control-label">Change Email Address</label>
          <div class="col-sm-10">
            <input type="text" name="saveemail" value="<?php echo $email; ?>" id="inputEmail" class="form-control" placeholder="Current Email Address">
          </div>
      </div>
      <div class="form-group">
          <label for="inputSiteStyle" class="col-sm-2 control-label">Change Site Style</label>
          <div class="col-sm-10">
            <select name="savesitestyle" id="inputSiteStyle" class="form-control">
              <option value='0' <?php if ($sitestyle == "0") { echo "SELECTED"; } ?>>Site's Default Style</option>
              <?php
                $styleq = $zw->SQL->query("SELECT * FROM `{$zw->config['db_prefix']}style` ORDER BY `id` ASC");
                while ($styler = $zw->SQL->fetch_array($styleq)) {
                  $styleid = $styler['id'];
                  $stylename = $styler['name'];
                  $styleversion = $styler['version'];
                  if ($styleid == $sitestyle) {
                    $styleselected = "SELECTED";
                  }else{
                    $styleselected = "";
                  }
                  echo "
                  <option value='".$styleid."' ".$styleselected.">".$stylename." - ".$styleversion."</option>
                  ";
                }
              ?>
            </select>
          </div>
      </div>
      <div class="form-group">
        <div class="col-sm-10">
          <input type="submit" name="submit" value="Save Settings" class="btn btn-success">
        </div>
      </div>
    </form><br>
    <a href="changepassword.php" class='btn btn-sm btn-danger'>Change Password</a>

<?php
}else if ($type == "profile") {
$profq = $zw->SQL->query("SELECT * FROM `{$zw->config['profile_db']}`.userprofile WHERE useruuid = '$user_uuid'");
$profr = $zw->SQL->fetch_array($profq);
$url = $profr['profileURL'];
$want2mask = $profr['profileWantToMask'];
$want2text = $profr['profileWantToText'];
$skillsmask = $profr['profileSkillsMask'];
$skillstext = $profr['profileSkillsText'];
$lang = $profr['profileLanguages'];
$fakeaboutme = $profr['profileAboutText'];
$realaboutme = $profr['profileFirstText'];
$profileallowpublish = $profr['profileAllowPublish'];
$profilePartner = $profr['profilePartner'];

if ($profilePartner == "00000000-0000-0000-0000-000000000000") {
  $partnername = "";
}else{
  $partnerq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$profilePartner'");
    $partnerr = $zw->SQL->fetch_array($partnerq);
    $partnerFirstName = $partnerr['FirstName'];
    $partnerLastName = $partnerr['LastName'];
    if ($partnerLastName == "Resident") {
      $pname = $partnerFirstName;
      $pdname = $partnerFirstName;
    }else{
      $pname = $partnerFirstName.".".$partnerLastName;
      $pdname = $partnerFirstName." ".$partnerLastName;
    }
  $partnername = "<B>Partner: </B><a href='".$site_address."/profile.php?u=".$pname."'>".$pdname."</a>";
}
?>
    <form class="form-horizontal" method="post" action="<?php echo $site_address; ?>/usersettings.php?type=<?php echo $type; ?>" role="form">
      <div class="form-group">
          <label for="inputUUID" class="col-sm-2 control-label"></label>
          <div class="col-sm-10">
            <B>UUID: </B><?php echo $user_uuid; ?>
          </div>
      </div>
      <div class="form-group">
          <label for="inputTitle" class="col-sm-2 control-label"></label>
          <div class="col-sm-10">
            <B>Avatar Name: </B><?php echo $firstname." ".$lastname; ?>
          </div>
      </div>
      <?php if ($profilePartner != "00000000-0000-0000-0000-000000000000") { ?>
      <div class="form-group">
          <label for="inputRezDay" class="col-sm-2 control-label"></label>
          <div class="col-sm-10">
            <?php echo $partnername; ?>
          </div>
      </div>
      <?php } ?>
      <div class="form-group">
          <label for="inputTitle" class="col-sm-2 control-label"></label>
          <div class="col-sm-10">
            <B>Title: </B><?php echo $usertitle; ?>
          </div>
      </div>
      <div class="form-group">
          <label for="inputRezDay" class="col-sm-2 control-label"></label>
          <div class="col-sm-10">
            <B>Rez Day: </B><?php echo $rezday; ?>
          </div>
      </div>
      <div class="form-group">
          <label for="inputAboutFL" class="col-sm-2 control-label">About In World Me</label>
          <div class="col-sm-10">
            <textarea name="aboutfl" rows="3" id="inputAboutFL" class="form-control"><?php echo $fakeaboutme; ?></textarea>
          </div>
      </div>
      <div class="form-group">
          <label for="inputAboutRL" class="col-sm-2 control-label">About Real World Me</label>
          <div class="col-sm-10">
            <textarea name="aboutrl" rows="3" id="inputAboutRL" class="form-control"><?php echo $realaboutme; ?></textarea>
          </div>
      </div>
      <div class="form-group">
          <label for="inputSkills" class="col-sm-2 control-label">Skills</label>
          <div class="col-sm-10">
            <input type="text" name="skills" value="<?php echo $skillstext; ?>" id="inputSkills" class="form-control" placeholder="Skills">
          </div>
      </div>
      <div class="form-group">
          <label for="inputLanguages" class="col-sm-2 control-label">Languages</label>
          <div class="col-sm-10">
            <input type="text" name="languages" value="<?php echo $lang; ?>" id="inputLanguages" class="form-control" placeholder="Languages">
          </div>
      </div>
      <div class="form-group">
          <label for="inputWebsite" class="col-sm-2 control-label">Website</label>
          <div class="col-sm-10">
            <input type="text" name="profileurl" value="<?php echo $url; ?>" id="inputWebsite" class="form-control" placeholder="Website URL">
          </div>
      </div>
      <div class="form-group">
        <div class="col-sm-10">
          <input type="submit" name="submit" value="Update Profile" class="btn btn-success">
        </div>
      </div>
    </form>
<?php }else if ($type == "inv") { ?>
    <h4>**WIP (Work In Progress)**</h4>
    <small>Managing your inventory here is not recommended at this time.</small>
  	<table class='table'>
  		<thead>
  			<tr>
  				<th>Folder/Item name</th>
  				<th>Type</th>
  				<th>Action</th>
  			</tr>
  		</thead>
  		<tbody>
  		<?php
 		$ifq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.inventoryfolders WHERE agentID = '$user_uuid' ORDER BY `folderName` ASC LIMIT 0,100");
 		while ($ifr = $zw->SQL->fetch_array($ifq)) {
 			$folderName = $ifr['folderName'];
 			$folderID = $ifr['folderID'];
      if ($folderName == "Trash") {
        $fbuttons = "<input type='submit' name='emptytrash' value='Empty Trash' class='btn btn-primary'>";
      }else{
        $fbuttons = "<input type='submit' name='movefolder' value='Move Folder' class='btn btn-primary'>";
      }
echo "
    <tr>
      <td>".$folderName."</td>
      <td></td>
      <td>".$fbuttons."</td>
    </tr>
    ";
 			$iiq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.inventoryitems WHERE parentFolderID = '$folderID' ORDER BY `inventoryName` ASC LIMIT 0,100");
      $iin = $zw->SQL->num_rows($iiq);
      if ($iin) {
        while ($iir = $zw->SQL->fetch_array($iiq)) {
          $invName = $iir['inventoryName'];
          $invDesc = $iir['inventoryDescription'];
          $invType = $iir['assetType'];
          if ($invName == "") {
            $ibuttons = "<input type='submit' name='emptytrash' value='Empty Trash' class='btn btn-primary'>";
          }else{
            $ibuttons = "<input type='submit' name='moveitem' value='Move Item' class='btn btn-primary'>";
          }
          echo "
           <tr>
             <td> - ".$invName."</td>
             <td></td>
             <td>".$ibuttons."</td>
          </tr>
          ";
        }
      }else{

      }

 		}
  		?>
  		</tbody>
  	</table>
<?php }else if ($type == "api") { ?>
    <?php
    if ($token) {
    ?>
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Your <?php echo $zw->config['GridName']; ?> API Token</h3>
      </div>
      <div class="panel-body">
        <?php echo $token; ?>
      </div>
    </div>
    <?php } ?>
    <form method='post' action='<?php echo $site_address; ?>/usersettings.php?type=<?php echo $type; ?>' class='form' role='form'>
      <input type='submit' name='generatetoken' value='Generate New API Token' class='btn btn-primary'>
    </form>
<?php
  }else{
  } // end if ($type)
} // end if ($user_uuid)
include ('inc/footer.php');
?>