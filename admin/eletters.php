<?php
$page_title = "ELetters";
define('ZW_IN_SYSTEM', true);
require_once('../inc/header.php');
if ($zw->grid->isAdmin($user_uuid)) {
	$now = time();
	$submit = $zw->Security->make_safe($_POST['submit']);
	if ($submit != "") {
		$subject = $zw->Security->make_safe($_POST['subject']);
		$message = "<a href='".$zw->config['SiteAddress']."/eletter.php?subject=".$subject."'><small>Click here to read on our website.</small></a>";
		$message .= $zw->Security->make_safe($_POST['message']);
		$q = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts ORDER BY `Email` ASC");
		while ($r = $zw->SQL->fetch_array($q)) {
			$uem = $r['Email'];
			$zw->site->sendemail($uem, $subject, $message);
		}
		$zw->SQL->query("INSERT INTO `{$zw->config['db_prefix']}newsletter` (useruuid, subject, message, time) VALUES ('$user_uuid', '$subject', '$message', '$now')");
		echo $zw->site->displayalert("Emails are being delivered.", "success");
	}
?>
<form class="form-horizontal" method="post" action="" role="form">
  <div class="form-group">
    <label for="inputSubject" class="col-sm-2 control-label">Subject</label>
    <div class="col-sm-10">
      <input type="text" name="subject" class="form-control" id="inputSubject" placeholder="Subject">
    </div>
  </div>
  <div class="form-group">
    <label for="inputMessage" class="col-sm-2 control-label">Message</label>
    <div class="col-sm-10">
      <textarea name="message" class="form-control" rows="10" id="inputMessage" placeholder="Message"></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <input type="submit" name="submit" value="Send Email" class="btn btn-primary">
    </div>
  </div>
</form>
<?php
}else{
	echo $zw->site->displayalert("You are not the captian.", "danger");
}
include ('../inc/footer.php');
?>