<?php
$page_title = "My Tickets";
define('ZW_IN_SYSTEM', true);
require_once('../inc/header.php');
$id = $zw->Security->make_safe($_GET['id']);
if ($zw->grid->isAdmin($user_uuid) && $id != "") {

$send = $zw->Security->make_safe($_POST['send']);
if ($send == "Send Reply") {
	$replymsg = $zw->Security->make_safe($_POST['replymsg']);
	if ($replymsg != "") {
		$now = time();
		$i = $zw->SQL->query("INSERT INTO `{$zw->config['db_prefix']}ticket_reply` (tid, user_uuid, body, userviewstatus, adminviewstatus, time) VALUES ('$id', '$user_uuid', '$replymsg', '0', '1', '$now')");
		if ($i) {
			echo $zw->site->displayalert("Reply saved.", "success");
			$zw->api->sendmsg("New reply to ticket #".$id." from ".$zw->grid->uuid2name($user_uuid), $colour = "yellow");
		}else{
			echo $zw->site->displayalert("Unable to save reply. Please try later.", "danger");
		}
	}
}
$submit = $zw->Security->make_safe($_POST['submit']);
if ($submit == "Close Ticket") {
	$now = time();
	$u = $zw->SQL->query("UPDATE `{$zw->config['db_prefix']}tickets` SET status = '1', time_closed = '$now' WHERE id = '$id'");
	if ($u) {
		echo $zw->site->displayalert("Ticket closed.", "success");
	}else{
		echo $zw->site->displayalert("Unable to close ticket. Please try later.", "danger");
	}
}
if ($submit == "Reopen Ticket") {
	$u = $zw->SQL->query("UPDATE `{$zw->config['db_prefix']}tickets` SET status = '0', time_closed = '' WHERE id = '$id'");
	if ($u) {
		echo $zw->site->displayalert("Ticket reopened.", "success");
	}else{
		echo $zw->site->displayalert("Unable to reopened ticket. Please try later.", "danger");
	}
}
echo "<a href='tickets.php' class='btn btn-info'>Back to your ticket list</a><p>";

$tq = $zw->SQL->query("SELECT * FROM `{$zw->config['db_prefix']}tickets` WHERE id = '$id' AND user_uuid != '$user_uuid'");
$tn = $zw->SQL->num_rows($tq);
if ($tn) {
	$tr = $zw->SQL->fetch_array($tq);
	$userid = $tr['user_uuid'];
	$subject = $tr['subject'];
	$body = $tr['body'];
	$body = htmlspecialchars_decode($body, ENT_NOQUOTES);
	$body = html_entity_decode($body);
	$dateopen = $zw->site->time2date($tr['time_created']);
	$tusername = $zw->grid->uuid2name($userid);
	$arelastreplies = $zw->tickets->replies($id, "admin");
	if ($tr['adminviewstatus'] == "0") {
		$zw->tickets->setviewed("ticket", $id, "admin");
	}
	if ($arelastreplies != "0") {
		$lastreplies = $arelastreplies;
	}else{
		$lastreplies = "";
	}
	if ($tr['status'] == "0") {
		$ticketbutton = "<input type='submit' name='submit' value='Close Ticket' class='btn btn-danger'>";
	}else{
		$ticketbutton = "<input type='submit' name='submit' value='Reopen Ticket' class='btn btn-warning'>";
	}

	$online = $zw->grid->online($userid);
	if ($online) {
	$onoff = "onlinedot.png";
	}else{
	$onoff = "offlinedot.png";
	}
	$isOnline = "<img src='".$site_address."/img/".$onoff."' border='0'>";
	echo "
	<form method='post' action='viewticket.php?id=".$id."' class='form' role='form'>
	".$ticketbutton."
	</form>
	<div class='row well'>
		<div class='col-md-4'><B>".$tusername." ".$isOnline." </B><br><small><B>Date:</B> ".$dateopen."</small></div>
		<div class='col-md-8'><B>".$subject."</B><br>".$body."</div>
		<div class='clearfix visible-xs-block'></div>
	</div>
	<h3>Latest Replies<br><small><a href='#reply'>Create a reply</a></small></h3>
	".$lastreplies;
	if ($tr['status'] == "0") {
		$adminname = $zw->grid->uuid2name($user_uuid);
		echo "
		<div class='row well' id='reply'>
			<div class='col-md-4'>Replying as ".$adminname."</div>
			<div class='col-md-8'>
			<form method='post' action='viewticket.php?id=".$id."' class='form' role='form'>
				<textarea class='form-control' rows='8' name='replymsg'></textarea><br>
				<input type='submit' name='send' value='Send Reply' class='btn btn-success'>
			</form>
			</div>
		</div>
		";
	}else{
		$dateclose = $zw->site->time2date($tr['time_closed']);
		echo "
		<div class='row well'>
			<div class='col-md-12'><B>Ticket has been closed since </B> ".$dateclose."</div>
		</div>
		";
	}
}else{
	echo $zw->site->displayalert("You selected one of your own tickets.<br>
		Please go back and select a ticket that is not yours.<br>
		If you want to see your own tickets please visit My Tickets in the User menu instead.", "danger");
	echo "<br><a href='tickets.php' class='btn btn-info'>Back to your ticket list</a>";
}
}else if ($zw->grid->isAdmin($user_uuid) && $id == "") {
	echo $zw->site->displayalert("Id number is empty. Please go back and select a ticket to view", "danger");
	echo "<br><a href='tickets.php' class='btn btn-info'>Back to your ticket list</a>";
}else{
	echo $zw->site->displayalert("You are not a admin.", "danger");
}
include ('../inc/footer.php');
?>