<?php
$page_title = "Settings";
define('ZW_IN_SYSTEM', true);
require_once('../inc/header.php');

$hctoken = $zw->config['HipChatToken'];

$cat = $zw->Security->make_safe($_GET['cat']);
$type = $zw->Security->make_safe($_GET['type']);
if (!$type) {
  $type = "zmw";
}

$savesettings = $zw->Security->make_safe($_POST['savesettings']);
$zwsettings = $zw->Security->make_safe($_POST['zwsettings']);

$posx = $zw->Security->make_safe($_POST['posx']);
$posy = $zw->Security->make_safe($_POST['posy']);
$posz = $zw->Security->make_safe($_POST['posz']);

if ($zw->grid->isAdmin($user_uuid)) {
if ($savesettings) {
$pos = $posx.",".$posy.",".$posz;
$zw->SQL->query("UPDATE `{$zw->config['db_prefix']}settings` SET value = '$pos' WHERE name = 'Default_Pos'");
	foreach ($zwsettings as $key => $value) {
	 $zw->SQL->query("UPDATE `{$zw->config['db_prefix']}settings` SET value = '$value' WHERE name = '$key'");
	}
echo $zw->site->displayalert('<strong>SAVED!</strong> Settings saved', "success");
}

if ($hctoken != "") {
  $addy = $site_address."/hipchatconfirm.php";
  $cookieexplode = explode(".", $zw->config['cookie_domain']);
  $domkey = $cookieexplode[1].".".$cookieexplode[0].".zmw";
  echo "<a href='https://www.hipchat.com/addons/install?url=".$addy."&key=".$domkey."' class='btn btn-small btn-primary' target='_blank'>Install HipChat</a>";
}

echo "
<ul class='nav nav-tabs' role='tablist'>
";
$menuq = $zw->SQL->query("SELECT * FROM `{$zw->config['db_prefix']}settings_menu` ORDER BY `id` ASC LIMIT 0,100");
while ($menur = $zw->SQL->fetch_array($menuq)) {
  $menuname = $menur['name'];
  if ($type == $menuname) {
    $typeselected = "class='active'";
  }else{
    $typeselected = "";
  }
  echo "<li ".$typeselected."><a href='settings.php?type=".$menuname."'>".$menuname."</a></li>";
}
echo "
</ul>
<form class='form-horizontal' role='form' method='post' action='settings.php?type=".$type."'>
   <div class='form-group'>
    <div class='col-sm-2'>
      <B>Setting Name</B>
    </div>
    <div class='col-sm-4'>
      <B>Setting Value</B>
    </div>
    <div class='col-sm-4'>
      <B>Setting Info</B>
    </div>
  </div>";

$sq = $zw->SQL->query("SELECT * FROM `{$zw->config['db_prefix']}settings` WHERE type = '$type' ORDER BY `name` ASC LIMIT 0,100");
while ($sr = $zw->SQL->fetch_array($sq)) {
  $sid = $sr['id'];
  $sname = $sr['name'];
  $svalue = $sr['value'];
  $info = $sr['info'];

  if ($info) {
    $sinfo = "<small>".$info."</small>";
  }else{
    $sinfo = "";
  }

  if ($sname == "Default_Pos") {
    $sposexplode = explode(",", $svalue);
    $sposx = $sposexplode[0];
    $sposy = $sposexplode[1];
    $sposz = $sposexplode[2];
    $inputa = "X:<input type='text' id='posx' maxlength='5' size='5' placeholder='Pos X' name='posx' value='".$sposx."'>";
    $inputa .= " Y:<input type='text' id='posy' maxlength='5' size='5' placeholder='Pos Y' name='posy' value='".$sposy."'>";
    $inputa .= " Z:<input type='text' id='posz' maxlength='5' size='5' placeholder='Pos Z' name='posz' value='".$sposz."'>";
    $input = $inputa;
  }else if ($sname == "activation_type") {
    $inputa = "";
    $inputa .= "<select name='zwsettings[activation_type]' class='form-control'>
    <option value='0'";
    if ($svalue == "0" ) {
      $inputa .= "SELECTED";
    }else{
    }
    $inputa .= ">None</option>
    <option value='1'";
    if ($svalue == "1" ) {
      $inputa .= "SELECTED";
    }else{
    }
    $inputa .= ">User</option>
    <option value='2'";
    if ($svalue == "2" ) {
      $inputa .= "SELECTED";
    }else{
    }
    $inputa .= ">Admin</option>
    </select>";
    $input = $inputa;
  }else if ($sname == "AllowRegistration") {
    $inputa = "";
    $inputa .= "<select name='zwsettings[AllowRegistration]' class='form-control'>
    <option value='n'";
    if ($svalue == "n" ) {
      $inputa .= "SELECTED";
    }else{
    }
    $inputa .= ">No</option>
    <option value='y'";
    if ($svalue == "y" ) {
      $inputa .= "SELECTED";
    }else{
    }
    $inputa .= ">Yes</option>
    </select>";
    $input = $inputa;
  }else if ($sname == "redirect_type") {
    $inputa = "";
    $inputa .= "<select name='zwsettings[redirect_type]' class='form-control'>
    <option value='1'";
    if ($svalue == "1" ) {
      $inputa .= "SELECTED";
    }else{
    }
    $inputa .= ">PHP</option>
    <option value='2'";
    if ($svalue == "2" ) {
      $inputa .= "SELECTED";
    }else{
    }
    $inputa .= ">HTML</option>
    <option value='3'";
    if ($svalue == "3" ) {
      $inputa .= "SELECTED";
    }else{
    }
    $inputa .= ">JavaScript</option>
    </select>";
    $input = $inputa;
  }else if ($sname == "site_admin_level") {
    $inputa = "";
    $inputa .= "<select name='zwsettings[site_admin_level]' class='form-control'>
    <option value='100'";
    if ($svalue == "100" ) {
      $inputa .= "SELECTED";
    }else{
    }
    $inputa .= ">100</option>
    <option value='150'";
    if ($svalue == "150" ) {
      $inputa .= "SELECTED";
    }else{
    }
    $inputa .= ">150</option>
    <option value='200'";
    if ($svalue == "200" ) {
      $inputa .= "SELECTED";
    }else{
    }
    $inputa .= ">200</option>
    <option value='250'";
    if ($svalue == "250" ) {
      $inputa .= "SELECTED";
    }else{
    }
    $inputa .= ">250</option>
    </select>";
    $input = $inputa;
  }else if ($sname == "security_image") {
    $inputa = "";
    $inputa .= "<select name='zwsettings[security_image]' class='form-control'>
    <option value='no'";
    if ($svalue == "no" ) {
      $inputa .= "SELECTED";
    }else{
    }
    $inputa .= ">No</option>
    <option value='yes'";
    if ($svalue == "yes" ) {
      $inputa .= "SELECTED";
    }else{
    }
    $inputa .= ">Yes</option>
    </select>";
    $input = $inputa;
  }else if ($sname == "ShowStats") {
    $inputa = "";
    $inputa .= "<select name='zwsettings[ShowStats]' class='form-control'>
    <option value='false'";
    if ($svalue == "false" ) {
      $inputa .= "SELECTED";
    }else{
    }
    $inputa .= ">false</option>
    <option value='true'";
    if ($svalue == "true" ) {
      $inputa .= "SELECTED";
    }else{
    }
    $inputa .= ">true</option>
    </select>";
    $input = $inputa;
  }else if ($sname == "Money") {
    $inputa = "";
    $inputa .= "<select name='zwsettings[Money]' class='form-control'>
    <option value='false'";
    if ($svalue == "false" ) {
      $inputa .= "SELECTED";
    }else{
    }
    $inputa .= ">false</option>
    <option value='true'";
    if ($svalue == "true" ) {
      $inputa .= "SELECTED";
    }else{
    }
    $inputa .= ">true</option>
    </select>";
    $input = $inputa;
  }else if ($sname == "Style") {
    $inputa = "";
    $inputa .= "<select name='zwsettings[Style]' class='form-control'>";
    $styq = $zw->SQL->query("SELECT * FROM {$zw->config['db_prefix']}style");
    while ($styr = $zw->SQL->fetch_array($styq)) {
      $styid = $styr['id'];
      $styname = $styr['name'];
      if ($styid == $svalue) {
        $stysel = "SELECTED";
      }else{
        $stysel = "";
      }
      $inputa .= "<option value='".$styid."' ".$stysel.">".$styname."</option>";
    }
    $inputa .= "</select>";
    $input = $inputa;
  }else{
    $input = "<input type='text' class='form-control' id='".$sname."' placeholder='".$sname."' name='zwsettings[".$sname."]' value='".$svalue."'>";
  }
  echo "
   <div class='form-group'>
    <label for='".$sname."' class='col-sm-2 control-label'>".$sname."</label>
    <div class='col-sm-4'>
      ".$input."
    </div>
    <div class='col-sm-4'>
      ".$sinfo."
    </div>
  </div>
  ";
}
?>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <input type="submit" class="btn btn-primary" name="savesettings" value="Save">
    </div>
  </div>
</form>
<?php
}
include ('../inc/footer.php');
?>