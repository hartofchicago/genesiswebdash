<?php
$page_title = "My Tickets";
define('ZW_IN_SYSTEM', true);
require_once('../inc/header.php');
if ($zw->grid->isAdmin($user_uuid)) {
echo "<small>Tickets that were closed a month ago are automatically deleted</small><br>
<div class='table-responsive'>
<table class='table table-hover table-striped'>
<thead>
<tr>
<th>#</th>
<th>User</th>
<th>Subject</th>
<th>Status</th>
<th>Date Created</th>
<th>Date Closed</th>
<th>Last Reply</th>
<th>Last Reply Date</th>
</tr>
</thead>
<tbody>
";
$tq = $zw->SQL->query("SELECT * FROM `{$zw->config['db_prefix']}tickets` WHERE user_uuid != '$user_uuid' ORDER BY `id` DESC LIMIT 0,100");
$tn = $zw->SQL->num_rows($tq);
while ($tr = $zw->SQL->fetch_array($tq)) {
	$id = $tr['id'];
	$userid = $tr['user_uuid'];
	$subject = $tr['subject'];
	$username = $zw->grid->uuid2name($userid);
	$dateopen = $zw->site->time2date($tr['time_created']);
	if ($tr['status'] == "0") {
		$status = "<p class='text-success'>Open</p>";
		$dateclosed = "";
		$trcolor = "";
	}else if ($tr['status'] == "1") {
		$status = "<p class='text-danger'>Closed</p>";
		$dateclosed = $zw->site->time2date($tr['time_closed']);
		$trcolor = "danger";
	}
	$lastreply = $zw->tickets->lastreply($id);
	if ($lastreply != "0") {
		$replyuuid = $lastreply['user_uuid'];
		if ($zw->grid->isAdmin($replyuuid)) {
			$replystatus = "<p class='text-danger'>Admin Reply</p>";
			$repr = "admin";
		}else{
			$replystatus = "<p class='text-primary'>User Reply</p>";
			$repr = "user";
		}
		$lastreplydate = $zw->site->time2date($lastreply['time']);
	}else{
		$lastreplydate = "";
	}
	if ($trcolor == "") {
		if ($repr == "admin") {
			$trcolor = "warning";
		}else if ($repr == "user") {
			$trcolor = "info";
		}
	}
	echo "
	<tr class='".$trcolor."'>
		<td><B>".$id."</B></td>
		<td>".$username."</td>
		<td><a href='viewticket.php?id=".$id."'>".$subject."</a></td>
		<td>".$status."</td>
		<td>".$dateopen."</td>
		<td>".$dateclosed."</td>
		<td>".$replystatus."</td>
		<td>".$lastreplydate."</td>
	</tr>
	";
}
echo "
</tbody>
</table>
</div>";
}else{
	echo $zw->site->displayalert("You are not the captian.", "danger");
}
include ('../inc/footer.php');
?>