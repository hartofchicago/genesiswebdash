<?php
$page_title = "Edit Users";
define('ZW_IN_SYSTEM', true);
require_once('../inc/header.php');
if ($zw->grid->isAdmin($user_uuid)) {
$aviuuid = $zw->Security->make_safe($_POST['aviuuid']);
$edituser = $zw->Security->make_safe($_POST['edituser']);
$submit = $zw->Security->make_safe($_POST['submit']);

if ($aviuuid != "") {

	if ($submit == "Reset Password") {
		if ($zw->grid->sendresetconfirm($aviuuid)) {
			$resetuserq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$aviuuid'");
			$resetuserr = $zw->SQL->fetch_array($resetuserq);
			$resetuserfirst = $resetuserr['FirstName'];
			$resetuserlast = $resetuserr['LastName'];
			echo $zw->site->displayalert("Confirmation for a password reset has been sent to ".$resetuserfirst." ".$resetuserlast. " via email address ".$resetuseremail, "success");
		}else{
			echo $zw->site->displayalert("Unable to send confirmation for a password reset. Please see inform a ZMW developer.", "danger");
		}
	}
	if ($submit == "Save User") {
		$postemail = $zw->Security->make_safe($_POST['postemail']);
		$postlevel = $zw->Security->make_safe($_POST['postlevel']);
		$posttitle = $zw->Security->make_safe($_POST['posttitle']);
		$u = $zw->SQL->query("UPDATE `{$zw->config['robust_db']}`.UserAccounts SET Email = '$postemail', UserLevel = '$postlevel', UserTitle = '$posttitle' WHERE PrincipalID = '$aviuuid'");
		if ($u) {
			echo $zw->site->displayalert("User Updated", "success");
		}else{
			echo $zw->site->displayalert("User was not updated", "danger");
		}
	}
	if ($submit == "Approve User" || $submit == "Unban User") {
		$u = $zw->SQL->query("UPDATE `{$zw->config['robust_db']}`.UserAccounts SET UserLevel = '0', UserTitle = 'LocalUser' WHERE PrincipalID = '$aviuuid'");
	}
	if ($submit == "Ban User") {
		$u = $zw->SQL->query("UPDATE `{$zw->config['robust_db']}`.UserAccounts SET UserLevel = '-1', UserTitle = 'Banned' WHERE PrincipalID = '$aviuuid'");
	}
	if ($submit == "Send Message") {
		$postmsg = $zw->Security->make_safe($_POST['postmsg']);
		if ($postmsg != "") {
			$data = "MSG=Admin message from ".$zw->grid->uuid2name($user_uuid).":\n".$postmsg."=".$aviuuid;
			$lslreturn = $zw->lsl->send2server($data);
			echo $zw->site->displayalert("Message sent<br>".$lslreturn, "success");
		}else{
			echo $zw->site->displayalert("Message field was empty. Message not sent.", "warning");
		}
	}
	if ($submit == "Kick User") {
		$data = "KICK=".$aviuuid."=You have been kicked by ".$zw->grid->uuid2name($user_uuid);
		$zw->lsl->send2server($data);
		echo $zw->site->displayalert("Message sent", "success");
	}
	$q = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$aviuuid'");
	$r = $zw->SQL->fetch_array($q);
	$aviemail = $r['Email'];
	$avilevel = $r['UserLevel'];
	$avititle = $r['UserTitle'];
	$avicreated = $r['Created'];
	$gq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.GridUser WHERE UserID = '$aviuuid'");
	$gr = $zw->SQL->fetch_array($gq);
	$avihomeregion = $zw->grid->regionname($gr['HomeRegionID']);
	$avilastregion = $zw->grid->regionname($gr['LastRegionID']);
	$avlastlogin = $gr['Login'];
	if ($avlastlogin == "0") {
		$avilastlogin = "Never";
	}else{
		$avilastlogin = $zw->site->time2date($avlastlogin);
	}

	$regionsowned = "<table class='table table-hover table-striped'>
	<thead>
	<tr>
	<th>Region Name</th>
	<th>Server IP</th>
	<th>World Location</th>
	</tr>
	</thead>
	<tbody>";
	$ownedregionsq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.regions WHERE owner_uuid = '$aviuuid'");
	$ownedregionsn = $zw->SQL->num_rows($ownedregionsq);
	if ($ownedregionsn) {
		while ($ownedregionsr = $zw->SQL->fetch_array($ownedregionsq)) {
			$ownedregionname = $ownedregionsr['regionName'];
			$ownedregionserver = $ownedregionsr['serverURI'];
			$ownedregionlocX = $ownedregionsr['locX'] / 256;
			$ownedregionlocY = $ownedregionsr['locY'] / 256;
			$ownedregionloc = " <B>X:</B>".$ownedregionlocX." <B>Y:</B>".$ownedregionlocY;
			$regionsowned .= "<tr><td>".$ownedregionname."</td><td>".$ownedregionserver."</td><td>".$ownedregionloc."</td></tr>";
		}
	}else{
		$regionsowned .= "<tr><td>None</td><td></td><td></td></tr>";
	}
	$regionsowned .= "</tbody></table>";

	if ($zw->grid->online($aviuuid)) {
		$online = "onlinedot.png";
		$onoffserverbuttons = "<input type='submit' name='submit' value='Kick User' class='btn btn-danger btn-sm'>";
	}else{
		$online = "offlinedot.png";
		$onoffserverbuttons = "";
	}
	if ($zw->config['InWorldServer'] != "") {
		$buttons = $onoffserverbuttons;
		$sendmsgtextbox = "<input type='text' name='postmsg' value='' class='form-control' placeholder='Send a message'>";
	}else{
		$buttons = "";
		$sendmsgtextbox = "No in world server found  yet.";
	}
	if ($avilevel == "-2") {
		$userstatusbuttons = "<input type='submit' name='submit' value='Approve User' class='btn btn-success btn-sm'>";
	}else if ($avilevel == "-1") {
		$userstatusbuttons = "<input type='submit' name='submit' value='Unban User' class='btn btn-warning btn-sm'>";
	}else if ($avilevel >= "-1") {
		$userstatusbuttons = "<input type='submit' name='submit' value='Ban User' class='btn btn-danger btn-sm'>";
	}
	echo "
	<div class='table-responsive'>
	<form method='post' action='' class='form' role='form'>
	<input type='hidden' name='aviuuid' value='".$aviuuid."'>
	<table class='table table-hover table-striped'>
	<tbody>
		<tr>
			<td><B>Name</B></td>
			<td>".$zw->grid->uuid2name($aviuuid)." <img src='".$site_address."/img/".$online."'></td>
		</tr>
		<tr>
			<td><B>UUID</B></td>
			<td>".$aviuuid."</td>
		</tr>
		<tr>
			<td><B>Home Region</B></td>
			<td>".$avihomeregion."</td>
		</tr>
		<tr>
			<td><B>Last Region</B></td>
			<td>".$avilastregion."</td>
		</tr>
		<tr>
			<td><B>Created</B></td>
			<td>".$zw->site->time2date($avicreated)."</td>
		</tr>
		<tr>
			<td><B>Last Login</B></td>
			<td>".$avilastlogin."</td>
		</tr>
		";
		if ($zw->config['Money'] == "true") {
			echo "
		<tr>
			<td><B>Money</B></td>
			<td>".$zw->config['GridMoney'].$zw->money->getbalance($aviuuid)."</td>
		</tr>";
		}
		echo "
		<tr>
			<td><B>Email</B></td>
			<td><input type='text' name='postemail' value='".$aviemail."' class='form-control'></td>
		</tr>
		<tr>
			<td><B>User Level</B></td>
			<td>
				<select name='postlevel' class='form-control'>
					<option value='-2' "; if ($avilevel == "-2") { echo "SELECTED"; } echo">Awating Approval = -2</option>
					<option value='-1' "; if ($avilevel == "-1") { echo "SELECTED"; } echo">Banned = -1</option>
					<option value='0' "; if ($avilevel == "0") { echo "SELECTED"; } echo">Resident = 0</option>
					<option value='10' "; if ($avilevel == "10") { echo "SELECTED"; } echo">Premium = 10</option>
					<option value='50' "; if ($avilevel == "50") { echo "SELECTED"; } echo">Moderator = 50</option>
					<option value='100' "; if ($avilevel == "100") { echo "SELECTED"; } echo">CSR = 100</option>
					<option value='150' "; if ($avilevel == "150") { echo "SELECTED"; } echo">Admin = 150</option>
					<option value='200' "; if ($avilevel == "200") { echo "SELECTED"; } echo">Grid God = 200</option>
					<option value='250' "; if ($avilevel == "250") { echo "SELECTED"; } echo">Owner = 250</option>
				</select>
			</td>
		</tr>
		<tr>
			<td><B>User Title</B></td>
			<td><input type='text' name='posttitle' value='".$avititle."' class='form-control'></td>
		</tr>
		<tr>
			<td><B>Send A Message</B></td>
			<td>".$sendmsgtextbox."</td>
		</tr>
	</tbody>
	</table>
		<input type='submit' name='submit' value='Save User' class='btn btn-success btn-sm'>
		<input type='submit' name='submit' value='Reset Password' class='btn btn-danger btn-sm'>
		<input type='submit' name='submit' value='Send Message' class='btn btn-info btn-sm'>
		".$buttons."
		".$userstatusbuttons."
	</form>
	<h3>Regions Owned</h3>
	".$regionsowned."
	</div>";
}

}else{
	echo $zw->site->displayalert("You are not the captian.", "danger");
}
include ('../inc/footer.php');
?>