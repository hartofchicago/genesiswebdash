<?php
$page_title = "Users";
define('ZW_IN_SYSTEM', true);
require_once('../inc/header.php');
if ($zw->grid->isAdmin($user_uuid)) {

$c = $zw->Security->make_safe($_GET['c']);
$page = $zw->Security->make_safe($_GET['page']);

$sort = $zw->Security->make_safe($_GET['sort']);
$order = $zw->Security->make_safe($_GET['order']);

$onlinecount = $zw->grid->onlinecount();

$totaloffset = "100";
if (!$page || $page == "0" || $page == "1") {
$page = "1";
$offset = "0";
}else if ($page == "2") {
$offset = $totaloffset;
}else if ($page >= "2") {
$offset = $totaloffset / 2 * $page;
}

if (!$order) {
	$order = "ASC";
}
if (!$sort) {
	$sortorder = "`FirstName` $order, `LastName` $order";
}else{
	$sortorder = $sort." ".$order;
}
?>
<div class='table-responsive'>
<table class='table table-hover table-bordered table-striped'>
<thead>
<tr>
<th>
Avatar Name <small>Currently online: <?php echo $onlinecount; ?></small>
<span class="pull-right">
<a href='<?php echo $site_address; ?>/admin/users.php?c=<?php echo $c; ?>&page=<?php echo $page; ?>&sort=&order=ASC'><i class="fa fa-caret-up"></i></a><br>
<a href='<?php echo $site_address; ?>/admin/users.php?c=<?php echo $c; ?>&page=<?php echo $page; ?>&sort=&order=DESC'><i class="fa fa-caret-down"></i></a>
</span>
</th>
<th>
User Email
<span class="pull-right">
<a href='<?php echo $site_address; ?>/admin/users.php?c=<?php echo $c; ?>&page=<?php echo $page; ?>&sort=Email&order=ASC'><i class="fa fa-caret-up"></i></a><br>
<a href='<?php echo $site_address; ?>/admin/users.php?c=<?php echo $c; ?>&page=<?php echo $page; ?>&sort=Email&order=DESC'><i class="fa fa-caret-down"></i></a>
</span>
</th>
<th>
Date Created
<span class="pull-right">
<a href='<?php echo $site_address; ?>/admin/users.php?c=<?php echo $c; ?>&page=<?php echo $page; ?>&sort=Created&order=ASC'><i class="fa fa-caret-up"></i></a><br>
<a href='<?php echo $site_address; ?>/admin/users.php?c=<?php echo $c; ?>&page=<?php echo $page; ?>&sort=Created&order=DESC'><i class="fa fa-caret-down"></i></a>
</span>
</th>
<th>Last Login</th>
<th>Actions</th>
</tr>
</thead>
<tbody>
<?php
$presq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.Presence WHERE RegionID != '$zw->nullkey' ORDER BY `LastSeen` DESC LIMIT 0, 200");
while ($presr = $zw->SQL->fetch_array($presq)) {
	$onaviuuid = $presr['UserID'];
	$onsimuuid = $presr['RegionID'];
	if ($onsimuuid != $nullkey) {
		$useronq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$onaviuuid'");
		$useronn = $zw->SQL->num_rows($useronq);
		if ($useronn) {
			$useronr = $zw->SQL->fetch_array($useronq);
			$onavicreate = $zw->site->time2date($useronr['Created']);
			$onaviemail = $useronr['Email'];
			$onavifirst = $useronr['FirstName'];
			$onavilast = $useronr['LastName'];
			if ($onavilast == "Resident") {
				$onaviname = $onavifirst;
				$linkonaviname = $onavifirst;
			}else{
				$onaviname = $onavifirst." ".$onavilast;
				$linkonaviname = str_replace(" ", ".", $onaviname);
			}
			$onuserlevel = $useronr['UserLevel'];
			$onaviname = "<a href='".$site_address."/profile.php?u=".$linkonaviname."'>".$onaviname."</a>";
			$resetbutton = "<input type='submit' name='edituser' value='Edit User' class='btn btn-info btn-sm'>";
			if ($zw->config['Money'] == "true") {
				$onlubal = $zw->config['GridMoney'].$zw->money->getbalance($onaviuuid);
			}else{
				$onlubal = "";
			}
		}else{
			$onavicreate = $zw->site->time2date($now);
			$onaviemail = "N/A";
			$onaviname = "HG Visitor";
			$resetbutton = "";
			$onlubal = "";
		}

		

		if ($onuserlevel == "0" || $onaviname == "HG Visitor") {
			$onuleveltr = "";
		}else if ($onuserlevel == $zw->config['site_admin_level'] || $onuserlevel >= $zw->config['site_admin_level']) {
			$onuleveltr = "success";
		}else if ($onuserlevel == "-1") {
			$onuleveltr = "danger";
		}else if ($onuserlevel == "-2") {
			$onuleveltr = "info";
		}
		$regionname = $zw->grid->regionname($onsimuuid);
		$ononline = $zw->grid->online($onaviuuid);
		$ononoff = "onlinedot.png";
		echo "<tr class='".$onuleveltr."'>
		<td>".$onaviname." <img src='".$site_address."/img/".$ononoff."'> <small>(".$onaviuuid." - <B>Sim:</B> ".$regionname.") ".$onlubal."</small></td>
		<td>".$onaviemail."</td>
		<td>".$onavicreate."</td>
		<td></td>
		<td>
		<form method='post' action='edituser.php' class='form-horizontal' role='form'>
		<input type='hidden' name='aviuuid' value='".$onaviuuid."'>
		".$resetbutton."
		</form>
		</td>
		</tr>";
	}
}
$useroffq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts ORDER BY $sortorder LIMIT $offset, $totaloffset");
while ($useroffr = $zw->SQL->fetch_array($useroffq)) {
$aviuuid = $useroffr['PrincipalID'];
$aviemail = $useroffr['Email'];
$avifirst = $useroffr['FirstName'];
$avilast = $useroffr['LastName'];
if ($avilast == "Resident") {
	$aviname = $avifirst;
	$linkaviname = $aviname;
}else{
	$aviname = $avifirst." ".$avilast;
	$linkaviname = str_replace(" ", ".", $aviname);
}
$aviuserlevel = $useroffr['UserLevel'];
$avicreate = $zw->site->time2date($useroffr['Created']);
$online = $zw->grid->online($aviuuid);
$onoff = "offlinedot.png";
$aviname = "<a href='".$site_address."/profile.php?u=".$linkaviname."'>".$aviname."</a>";
if ($aviuserlevel == "0") {
	$uleveltr = "";
}else if ($aviuserlevel == $zw->config['site_admin_level'] || $aviuserlevel >= $zw->config['site_admin_level']) {
	$uleveltr = "success";
}else if ($aviuserlevel == "-1") {
	$uleveltr = "danger";
}else if ($aviuserlevel == "-2") {
	$uleveltr = "info";
}
if ($zw->config['Money'] == "true") {
	$lubal = $zw->config['GridMoney'].$zw->money->getbalance($aviuuid);
}else{
	$lubal = "";
}

$lastloginq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.GridUser WHERE UserID = '$aviuuid'");
$lastloginr = $zw->SQL->fetch_array($lastloginq);
$latestlogin = $lastloginr['Login'];
if ($latestlogin == "0") {
	$lastlogin = "Never";
}else{
	$lastlogin = $zw->site->time2date($latestlogin);
}
	if (!$online) {
	echo "<tr class='".$uleveltr."'>
	<td>".$aviname." <img src='".$site_address."/img/".$onoff."'> <small>(".$aviuuid.") ".$lubal."</small></td>
	<td>".$aviemail."</td>
	<td>".$avicreate."</td>
	<td>".$lastlogin."</td>
	<td>
	<form method='post' action='edituser.php' class='form-horizontal' role='form'>
	<input type='hidden' name='aviuuid' value='".$aviuuid."'>
	<input type='submit' name='edituser' value='Edit User' class='btn btn-info btn-sm'>
	</form>
	</td>
	</tr>";
	}
}
?>
</tbody>
</table>
</div>
<?php
$pager = $site_address."/admin/users.php?c=".$c;
$tbl_name = "`{$zw->config['robust_db']}`.UserAccounts";
echo $zw->pagination->paging($tbl_name, $pager, $totaloffset);
}
include ('../inc/footer.php');
?>