<?php
$page_title = "Regions";
define('ZW_IN_SYSTEM', true);
require_once('../inc/header.php');
if ($zw->grid->isAdmin($user_uuid)) {

$r = $zw->Security->make_safe($_GET['r']);
$page = $zw->Security->make_safe($_GET['page']);

$submit = $zw->Security->make_safe($_POST['restartregion']);
$regionID = $zw->Security->make_safe($_POST['regionid']);

if ($submit == "Restart Region") {
	echo $zw->site->displayalert("Sim is restarting. Please allow up to 2 minutes before logging in.", "warning");
	echo $zw->remote->restartsim($regionID);
}

$totaloffset = "50";
if (!$page || $page == "0" || $page == "1") {
$page = "1";
$offset = "0";
}else if ($page == "2") {
$offset = $totaloffset;
}else if ($page >= "2") {
$offset = $totaloffset * $page;
}

$regionq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.regions");
$regions = $zw->SQL->num_rows($regionq);

//echo $zw->remote->admin_broadcast("Testing admin broadcast");
?>
<div class='table-responsive'>
<table class='table table-hover table-bordered table-striped'>
<thead>
<tr>
<th>Region Name <small><B>Total Regions:</B> <?php echo $regions; ?></small></th>
<th>Owner Name</th>
<th>Server URI</th>
<th></th>
</tr>
</thead>
<tbody>
<?php
$regionlistq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.regions ORDER BY `regionName` ASC LIMIT $offset, $totaloffset");
while ($regionlistr = $zw->SQL->fetch_array($regionlistq)) {
$regionuuid = $regionlistr['uuid'];
$regionname = $regionlistr['regionName'];
$owner = $regionlistr['owner_uuid'];
$sizeX = $regionlistr['sizeX'];
$sizeY = $regionlistr['sizeY'];
$serverURI = $regionlistr['serverURI'];
$last_seen = $zw->site->time2date($regionlistr['last_seen']);
$ownername = $zw->grid->uuid2name($owner);
$usersonsim = $zw->grid->userspersim($regionuuid);
$ownerlinked = str_replace(" ", ".", $ownername);
echo "<tr>
<td>
".$regionname."
 <small>(".$sizeX." x ".$sizeY.") - (<B>Users on Sim:</B> ".$usersonsim.") (<B>Last Seen:</B> ".$last_seen.")</small>
</td>
<td><a href='".$zw->config['SiteAddress']."/profile.php?u=".$ownerlinked."'>".$ownername."</a></td>
<td>".$serverURI."</td>
<td>
<form method='post' action='' class='form' role='form'>
<input type='hidden' name='regionid' value='".$regionuuid."'>
<input type='submit' name='submit' value='Restart Region' class='btn btn-sm btn-danger'>
</form>
</td>
</tr>";
}
?>
</tbody>
</table>
</div>
<?php
$pager = $site_address."/admin/regions.php?r=".$r;
$tbl_name = "`{$zw->config['robust_db']}`.regions";
echo $zw->pagination->paging($tbl_name, $pager, $totaloffset);
}
include ('../inc/footer.php');
?>