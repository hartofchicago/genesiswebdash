<?php
$page_title = "Styles";
define('ZW_IN_SYSTEM', true);
require_once('../inc/header.php');
if ($zw->grid->isAdmin($user_uuid)) {

$syncstyle = $zw->Security->make_safe($_POST['syncstyle']);
$admstyid = $zw->Security->make_safe($_POST['admstyid']);
$set = $zw->Security->make_safe($_POST['set']);

if ($set == "Use" && $admstyid != "") {
	$setsty = $zw->SQL->query("UPDATE `{$zw->config['db_prefix']}settings` SET value = '$admstyid' WHERE name = 'Style'");
	if ($setsty) {
		echo $zw->site->displayalert("Style now set.", "success");
	}else{
		echo $zw->site->displayalert("Unable to set style.", "danger");
	}
}
if ($syncstyle == "Sync Styles") {
$zw->site->synstyles();
}
?>
<form method='post' action='' class='form-horizontal' role='form'>
<input type='submit' name='syncstyle' value='Sync Styles' class='btn btn-warning'>
</form>
<div class='table-responsive'>
<table class='table table-hover table-bordered table-striped'>
<thead>
<tr>
<th>ID</th>
<th>Style Name</th>
<th>Style Folder</th>
<th>BootStrap Version</th>
<th>Actions</th>
</tr>
</thead>
<tbody>
<?php
$styleq = $zw->SQL->query("SELECT * FROM `{$zw->config['db_prefix']}style` ORDER BY `id` ASC LIMIT 0,250");
while ($styler = $zw->SQL->fetch_array($styleq)) {
$id = $styler['id'];
$name = $styler['name'];
$folder = $styler['folder'];
$version = $styler['version'];

echo "<tr>
<td>".$id."</td>
<td>".$name."</td>
<td>".$folder."</td>
<td>".$version."</td>
<td>
<form method='post' action='' class='form-horizontal' role='form'>
<input type='hidden' name='admstyid' value='".$id."'>
";
if ($folder != $style) {
	echo "<input type='submit' name='set' value='Use' class='btn btn-success'><br>";
}
echo "</form>
</td>
</tr>";
}
?>
</tbody>
</table>
</div>
<?php
}
include ('../inc/footer.php');
?>