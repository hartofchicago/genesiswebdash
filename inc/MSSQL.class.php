<?php
if (!defined('ZW_IN_SYSTEM')) {
exit;	
}

class MSSQL {

var $zw;

	function MSSQL($server_name, $username, $password, $name, &$zw, $port = 3456) {
	$this->zw = &$zw;
    $this->server_name = $server_name;
    $this->username = $username;
    $this->password = $password;
    $this->name = $name;
    $this->port = $port;
    $this->connection = mssql_connect($this->server_name, $this->username, $this->password);
		if (!$this->connection || !mssql_select_db($this->name, $this->connection)) {
		die("Unable to connect to a MSSQL database");
		}
	}

	function result($q,$i,$d) {
	return mssql_result($q,$i,$d);
	}

	function affected_rows() {
	return mssql_rows_affected($this->connection);
	}

	function fetch_row($result) {
	return mssql_fetch_row($result);	
	}

	function fetch_assoc($result) {
	return mssql_fetch_assoc($result);
	}

	function fetch_array($result) {
	return mssql_fetch_array($result, MSSQL_BOTH);
	}

	function fetch_object($result) {
	return mssql_fetch_object($result);
	}

	function free_result($result) {
		if ($this->last_query == $result) {
		$this->last_query = '';
		}

	return mssql_free_result($result);
	}

	function get_client_info() {
	return phpinfo();
	}

	function insert_id() {
	return false;
	}

	function num_fields($result) {
	return mssql_num_fields($result);
	}

	function num_rows($result) {
	return mssql_num_rows($result);
	}

	function transaction($status = 'BEGIN') {
		switch (strtoupper($status)) {
			default:
			return true;
			break;
			case 'START':
			case 'START TRANSACTION':
			case 'BEGIN':
			return mssql_query($this->connection, 'START TRANSACTION');
			break;
			case 'COMMIT':
			return mssql_query($this->connection, 'COMMIT');
			break;
			case 'ROLLBACK':
			return mssql_query($this->connection, 'ROLLBACK');
			break;
		}
	}

	function query($query) {
		if ($query != '') {
			$result = mssql_query($this->connection, $query);
			return $result;
		}else{
			$result = mssql_query($this->connection, '');
			return $result;
		}
	}

	function close() {
	return mssql_close($this->connection);
	}
}
?>
