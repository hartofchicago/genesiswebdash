<?php
if (!defined('ZW_IN_SYSTEM')) {
exit;	
}
include('RemoteAdmin.php');

class console {
var $zw;
	function console(&$zw) {
		$this->zw = &$zw;
	}
	function send($url, $dir, $data) {
		$request = new HTTPRequest($url.$dir, HTTP_METH_POST);
		$request->setRawPostData($data);
		$request->send();
		$response = $request->getResponseBody();
		return $response;
	}
	function createsession($url, $user, $pass) {
		$dir = "StartSession/";
		$vars = "USER=".$user."&PASS=".$pass;
		$sess = $this->send($url, $dir, $vars);
		return $sess;
	}
	function closesession($url) {
		$sess = $_SESSION['ossession'];
		$dir = "CloseSession/";
		$vars = "ID=".$sess;
		$this->send($url, $dir, $vars);
		$_SESSION['ossession'] = "";
	}
	function command($url, $user, $pass, $command) {
		$sess = $this->createsession($url, $user, $pass);
		$_SESSION['ossession'] = $sess;
		$dir = "SessionCommand/";
		$vars = "ID=".$sess."&COMMAND=".$command;
		$reply = $this->send($url, $dir, $vars);
		return $reply;
		$this->closesession($url);
	}

	function remoteadmin($url, $port, $command, $paramaters) {
		$myRA = new RemoteAdmin($url, $port, 'mysecretpassword');
		$ret = $myRA->SendCommand($command, $paramaters);
		return $ret;
	}
}
?>