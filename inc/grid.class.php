<?php
if (!defined('ZW_IN_SYSTEM')) {
exit;	
}

class grid
{

var $zw;
	
	function grid(&$zw)
	{
		$this->zw = &$zw;
	}

	function newuuid() {
	$uuid = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
		mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
		mt_rand( 0, 0x0fff ) | 0x4000,
		mt_rand( 0, 0x3fff ) | 0x8000,
		mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ) );
	return $uuid;
	}

	function check4msgs() {
		$useruuid = $this->zw->user_info['PrincipalID'];
		$offlineimq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.im_offline WHERE PrincipalID = '$useruuid'");
		$offlineimn = $this->zw->SQL->num_rows($offlineimq);
		return $offlineimn;
	}

	function check4friendrequest() {
		$useruuid = $this->zw->user_info['PrincipalID'];
		$approvalq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.Friends WHERE PrincipalID = '$useruuid' AND Flags = '0'");
		$approvaln = $this->zw->SQL->num_rows($approvalq);
		$approvaln += $this->zw->site->hasproposal();
		return $approvaln;
	}

	function checkpartner($user, $partner) {
		$partnercheckq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.userprofile WHERE useruuid = '$user' AND profilePartner = '$partner'");
		$partnercheckn = $this->zw->SQL->num_rows($partnercheckq);
		if ($partnercheckn) {
			return true;
		}else{
			return false;
		}
	}

	function getbalance() {
		$moneyip = $this->zw->config['Money'];
		if ($moneyip == "true") {
			$useruuid = $this->zw->user_info['PrincipalID'];
			$moneyq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['money_db']}`.balances WHERE user = '$useruuid'");
			$moneyn = $this->zw->SQL->num_rows($moneyq);
			if ($moneyn) {
				$moneyr = $this->zw->SQL->fetch_array($moneyq);
				$money = number_format($moneyr['balance']);
			}else{
				$money = "0";
			}
		}else{
			$money = "0";
		}
		return $money;
	}

	function ismarried($user) {
		$partnercheckq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.userprofile WHERE useruuid = '$user'");
		$partnercheckr = $this->zw->SQL->fetch_array($partnercheckq);
		$partner = $partnercheckr['profilePartner'];
		if ($partner == "00000000-0000-0000-0000-000000000000") {
			return false;
		}else{
			return true;
		}
	}

	function getosuser($FirstName, $LastName) {
	$q = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.UserAccounts WHERE FirstName = '$FirstName' AND LastName = '$LastName'");
	$r = $this->zw->SQL->fetch_array($q);
	return $r;
	}

	function getosuser_by_uuid($uuid) {
	$q = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$uuid'");
	$r = $this->zw->SQL->fetch_array($q);
	return $r;
	}

	function uuid2name($UUID) {
	$q = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$UUID'");
	$n = $this->zw->SQL->num_rows($q);
	if ($n) {
		$r = $this->zw->SQL->fetch_array($q);
		$firstname = $r['FirstName'];
		$lastname = $r['LastName'];
		$name = $firstname." ".$lastname;
	}else{
		$name = "HG";
	}
	return $name;
	}

	function regioninfo($UUID) {
	$q = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.regions WHERE uuid = '$UUID'");
	$r = $this->zw->SQL->fetch_array($q);
	return $r;
	}

	function groupid2name($guid) {
	$grpq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['group_db']}`.os_groups_groups WHERE GroupID = '$guid'");
	$grpr = $this->zw->SQL->fetch_array($grpq);
	$grpname = $grpr['Name'];
	return $grpname;
	}

	function regionname($UUID) {
	$r = $this->regioninfo($UUID);
	$r3 = $r['regionName'];
	return $r3;
	}

	function regionip($UUID) {
	$r = $this->regioninfo($UUID);
	$r4 = $r['serverIP'];
	return $r4;
	}

	function online($UUID) {
	$q = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.Presence WHERE UserID = '$UUID' AND RegionID != '{$this->zw->nullkey}'");
	$n = $this->zw->SQL->num_rows($q);
		if ($n) {
			$online = true;
		}else{
			$online = false;
		}
	return $online;
	}

	function userspersim($sim) {
	$q = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.Presence WHERE RegionID = '$sim'");
	$c = $this->zw->SQL->num_rows($q);
	return $c;
	}

	function isAdmin($uuid) {
		if ($uuid) {
			$site_admin_level = $this->zw->config['site_admin_level'];
			$r = $this->getosuser_by_uuid($uuid);
			$level = $r['UserLevel'];
			if ($level == $site_admin_level || $level >= $site_admin_level) {
				return true;
			}else if ($level <= $site_admin_level) {
				return false;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function sendmsg($sender, $receiver, $msg) {
		$now = time();
		$sendername = $this->uuid2name($sender);
		$randuuid = $this->zw->getNewUUID();
		$xmlmsg = "<?xml version='1.0' encoding='utf-8'?><GridInstantMessage xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'><fromAgentID>".$sender."</fromAgentID><fromAgentName>".$sendername."</fromAgentName><toAgentID>".$receiver."</toAgentID><dialog>1</dialog><fromGroup>false</fromGroup><message>".$msg."</message><imSessionID>".$randuuid."</imSessionID><offline>0</offline><Position><X>128</X><Y>128</Y><Z>25</Z></Position><binaryBucket>R0VNSU5JIEkvMjkvMTIxLzEwMDEA</binaryBucket><ParentEstateID>0</ParentEstateID><RegionID>".$this->zw->nullkey."</RegionID><timestamp>".$now."</timestamp></GridInstantMessage>";
		$iq = $this->zw->SQL->query("INSERT INTO `{$this->zw->config['robust_db']}`.im_offline (PrincipalID, FromID, Message, TMStamp) VALUES ('$receiver', '$sender', '$xmlmsg', '$now')");
		if ($iq) {
			return true;
		}else{
			return false;
		}
	}

	function isonwebsite($uuid) {
		$fiveago = time() - 300;
		$q = $this->zw->Users->get_zw_user($uuid);
		$last_action = $q['last_action'];
		if ($last_action >= $fiveago) {
			return true;
		}else{
			return false;
		}
	}

	function getFriends($uuid) {
		$return = "<div class='table-responsive'>
		<table class='table table-striped table-hover table-condensed'>
		<thead>
		<tr>
		<th><small><B>NAME</B></small></th>
		<th><small><B>STATUS</B></small></th>
		</tr>
		</thead>
		<tbody>
		";
		$tbleron = array();
		$tbleroff = array();
		$fi = 0;
		$q = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.Friends WHERE PrincipalID = '$uuid'");
		while ($r = $this->zw->SQL->fetch_array($q)) {
			if ($fi <= 11) {
				$FriendID = $r['Friend'];
				$r2 = $this->getosuser_by_uuid($FriendID);
				if ($r2 == "") {
					$FriendFirst = "HG";
					$FriendLast = "Friend";
					$profaddy1 = "";
					$profaddy2 = "";
				}else{
					$FriendFirst = $r2['FirstName'];
					$FriendLast = $r2['LastName'];
					if ($FriendLast == "Resident") {
						$FriendName = $FriendFirst;
						$FriendNameLink = $FriendFirst;
					}else{
						$FriendName = $FriendFirst." ".$FriendLast;
						$FriendNameLink = $FriendFirst.".".$FriendLast;
					}
					$profaddy1 = "<a href='".$this->zw->config['SiteAddress']."/profile.php?u=".$FriendNameLink."'>";
					$profaddy2 = "</a>";
				}
				$Online = $this->online($FriendID);
				
				if ($Online) {
					$tbleron[$FriendName] .= "
					<tr>
					<td>".$profaddy1."<span class='text-success'>".$FriendName."</span>".$profaddy2."</td>
					<td><span class='text-success'>Online</span></td>
					</tr>
					";
					$fi++;
				}else{
					// do nothing, dont want to show offline friends
				}
			}else{

			}
		}
		if ($tbleron) {
			asort($tbleron);
			foreach ($tbleron as $keyon => $valon) {
			    $return .= $valon;
			}
		}else{
			$return .= "<tr><td><small>No friends online</small></td><td></td></tr>";
		}
		$return .= "</tbody>
		</table>
		</div>";
		return $return;
	}

	function getGroups($uuid) {
		echo "<div class='table-responsive'>
		<table class='table table-striped table-hover table-condensed'>
		<thead>
		<tr>
		<th><small><B>NAME</B></small></th>
		<th><small><B>MEMBERS</B></small></th>
		</tr>
		</thead>
		<tbody>
		";
		$q = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.os_groups_membership WHERE PrincipalID = '$uuid' LIMIT 0,10");
		while ($r = $this->zw->SQL->fetch_array($q)) {
			$GroupID = $r['GroupID'];
			$q2 = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.os_groups_groups WHERE GroupID = '$GroupID'");
			$q3 = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.os_groups_membership WHERE GroupID = '$GroupID'");
			$r2 = $this->zw->SQL->fetch_array($q2);
			$r3 = $this->zw->SQL->num_rows($q3);
			$Name = $r2['Name'];
			echo "<tr>
			<td><small><a href='".$this->zw->config['SiteAddress']."/groups.php?gid=".$GroupID."'>".$Name."</a></small></td>
			<td><small>".$r3."</small></td>
			</tr>
			";
		}
		echo "</tbody>
		</table>
		</div>";
	}

	function oscat($catid) {
		switch ($catid) {
		case "0":
			$r = "Any";
			break;
		case "18":
			$r = "Discussion";
			break;
		case "19":
			$r = "Sports";
			break;
		case "20":
			$r = "Live Music";
			break;
		case "22":
			$r = "Commercial";
			break;
		case "23":
			$r = "Nightlife/Entertainment";
			break;
		case "24":
			$r = "Games/Contests";
			break;
		case "25":
			$r = "Pageants";
			break;
		case "26":
			$r = "Education";
			break;
		case "27":
			$r = "Arts and Culture";
			break;
		case "28":
			$r = "Charity/Support Groups";
			break;
		case "29":
			$r = "Miscellaneous";
			break;
		}
	return $r;
	}

	function gridonline() {
		$loginuri = $this->zw->config['loginURI'];
		$urlrep = str_replace("http://", "", $loginuri);
		$explode = explode(":", $urlrep);
		$ip2robust = $explode[0];
		$port2robust = $explode[1];
		$fp = @fsockopen($ip2robust, $port2robust, $errno, $errstr, 1);
		if ($fp) {
			$return = true;
			fclose($fp);
		}else{
			$return = false;
		}
		return $return;
	}

	function onlinecount() {
		$onlineq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.Presence WHERE RegionID != '{$this->zw->nullkey}'");
		$online = $this->zw->SQL->num_rows($onlineq);
		if (!$online)$online = 0;
		return $online;
	}

	function gridstatus() {
		$now = time();
		$thisgrid = 0;
		$hgvisiters = 0;
		$onlineq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.Presence WHERE RegionID != '{$this->zw->nullkey}'");
		$online = $this->zw->SQL->num_rows($onlineq);
		while ($onliner = $this->zw->SQL->fetch_array($onlineq)) {
			$onlineuser = $onliner['UserID'];
			$usercheckq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$onlineuser'");
			$usercheckn = $this->zw->SQL->num_rows($usercheckq);
			if ($usercheckn) {
				$thisgrid++;
			}else{
				$hgvisiters++;
			}
		}

		$mostcountcheckq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['db_prefix']}mostusers` ORDER BY `count` DESC LIMIT 0,1");
		$mostcountcheckr = $this->zw->SQL->fetch_array($mostcountcheckq);
		$lastcount = $mostcountcheckr['count'];
		if (strlen($online) == 1) {
			$stronline = "0".$online;
		}else{
			$stronline = $online;
		}
		if ($lastcount == $stronline) {
			$this->zw->SQL->query("UPDATE `{$this->zw->config['db_prefix']}mostusers` SET time = '$now' WHERE count = '$stronline'");
		}else if ($lastcount <= $online) {
			$this->zw->SQL->query("INSERT INTO `{$this->zw->config['db_prefix']}mostusers` (count, time) VALUES ('$stronline', '$now')");
		}

		$totalq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.UserAccounts");
		$totaluser = $this->zw->SQL->num_rows($totalq);

		$monthago = $now - 2592000;
		$latestq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.GridUser WHERE Login < '$monthago'");
		$latest = $this->zw->SQL->num_rows($latestq);
		$defsize = "256";
		$defsqm = "65536";
		$sizextotal = "";
		$sizeytotal = "";
		$sizetotal = "";
		$totalsingleregions = "";
		$regionq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.regions");
		$regions = $this->zw->SQL->num_rows($regionq);
		while($regionr = $this->zw->SQL->fetch_array($regionq)) {
			$x = $regionr['sizeX'];
			$y = $regionr['sizeY'];
			$sizextotal += $x;
			$sizeytotal += $y;
			$xytotal = $x * $y;
			$sizetotal += $xytotal;
			if ($xytotal >= $defsqm) {
				$totalsingleregions += $xytotal / $defsqm;
			}else if($xytotal == $defsqm) {
				++$totalsingleregions;
			}
		}

		$mostcountq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['db_prefix']}mostusers` ORDER BY `count` DESC LIMIT 0,1");
		$mostcountr = $this->zw->SQL->fetch_array($mostcountq);
		$mostcountcount = $mostcountr['count'];
		$mostcounttime = $mostcountr['time'];
		$mostcountdate = $this->zw->site->time2date($mostcounttime);

		if ($this->gridonline()) {
			$gonline = "<font color='Green'>Online</font>";
		}else{
			$gonline = "<font color='Red'>Offline</font>";
		}

		$gridstatus = "<span class='nowrap'><strong>Grid is ".$gonline." </strong></span><br>";

		if ($this->zw->config['ShowStats'] == "true") {
			$gridstatus .= "<span class='nowrap'><strong>Users in World:</strong> ".$online."</span><br>
					<span class='nowrap'><strong>HG Visitors:</strong> ".$hgvisiters."</span><br>
					<span class='nowrap'><strong>Regions:</strong> ".$regions." <small>(".$totalsingleregions."*)</small></span><br>
					<span class='nowrap'><strong>Total Users:</strong> ".$totaluser."</span><br>
					<span class='nowrap'><strong>Active Users (Last 30 Days):</strong> ".$latest."</span><br>
					<span class='nowrap'><strong>Most Online:</strong> ".$mostcountcount." on ".$mostcountdate."</span><br>
					<span class='nowrap'><small>* Total count for all sims as if they were ".$defsize." by ".$defsize." meters</small></span>";
		}
	return $gridstatus;
	}

	function clearexpiredresets() {
		$now = time();
		$this->zw->SQL->query("DELETE FROM `{$this->zw->config['db_prefix']}resetcode` WHERE expiry < '$now'");
	}

	function sendresetconfirm($uuid) {
		$checkifexistq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$uuid'");
		$checkifexistn = $this->zw->SQL->num_rows($checkifexistq);
		if ($checkifexistn) {
			$checkifexistr = $this->zw->SQL->fetch_array($checkifexistq);
			$email = $checkifexistr['Email'];
			$checkifresetexistq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['db_prefix']}resetcode` WHERE uuid = '$uuid'");
			$checkifresetexistn = $this->zw->SQL->num_rows($checkifresetexistq);
			$randomuuid = $this->zw->getNewUUID();
			$expiry = time() + 172800;
			if ($checkifresetexistn) {
				$codeq = $this->zw->SQL->query("UPDATE `{$this->zw->config['db_prefix']}resetcode` SET code = '$randomuuid', expiry = '$expiry' WHERE uuid = '$uuid'");
			}else{
				$codeq = $this->zw->SQL->query("INSERT INTO `{$this->zw->config['db_prefix']}resetcode` (uuid, code, expiry) VALUES ('$uuid', '$randomuuid', '$expiry')");
			}
			if ($codeq) {
				$expirydate = $this->zw->site->time2date($expiry);
				$confirmaadress = $this->zw->config['SiteAddress']."/confirmreset.php?reset=".$randomuuid."&uuid=".$uuid;
				$esubject = "Request for a ".$this->zw->config['GridName']." temporary password.";
				$emessage = "Someone has requested a new ".$this->zw->config['GridName']." temporary password for <B>".$FirstName." ".$LastName."</B><br>
				Please visit <a href='".$confirmaadress."'>".$confirmaadress."</a> to confirm it was you.<br>
				The link will become invalid on ".$expirydate."<br>
				If you did not request a password reset please disregard this email.";
				$this->zw->site->sendemail($email, $esubject, $emessage);
				return true;
			}else{
				return false;
			}
		}
	}
}
?>