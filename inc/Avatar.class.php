<?php
if (!defined('ZW_IN_SYSTEM')) {
exit;	
}

class Avatar
{

var $zw;

	function Avatar(&$zw) {
		$this->zw = &$zw;
	}

	// BUGGED DO NOT USE RIGHT NOW
	function createlook($uuid, $aviname) {
		$aviexplode = explode(" ", $aviname);
		$avifirst = $aviexplode[0];
		$avilast = $aviexplode[1];
		$aviget = $this->zw->grid->getosuser($avifirst, $avilast);
		$avi = $aviget['PrincipalID'];
		$aviq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.Avatars WHERE PrincipalID = '$avi'");
		while($avir = $this->zw->SQL->fetch_array($aviq)) {
			$name = $avir['Name'];
			$value = $avir['Value'];
			$this->zw->SQL->query("INSERT INTO `{$this->zw->config['robust_db']}`.Avatars (PrincipalID, Name, Value) VALUES ('$uuid', '$name', '$value')");
		}
	}

  	function createinv($avuuid) {
	  $inv_template = array('Calling Cards' =>  2,
	                        'Objects' =>  6,
	                        'Landmarks' =>  3,
	                        'Clothing' =>  5,
	                        'Gestures' => 21,
	                        'Body Parts' => 13,
	                        'Textures' =>  0,
	                        'Scripts' => 10,
	                        'Photo Album' => 15,
	                        'Lost And Found' => 16,
	                        'Trash' => 14,
	                        'Notecards' =>  7,
	                        'My Inventory' =>  9,
	                        'Sounds' =>  1,
	                        'Animations' => 20
	                       );
	  $inv_masterID = $this->zw->getNewUUID();
	  $version = "1";
	    foreach ($inv_template as $invfldr_name => $inv_type) {
	      $invfldr_uuid = $this->zw->getNewUUID();
	      if($inv_type == 9) {
	          $invfldr_uuid = $inv_masterID;
	          $inv_parent = '00000000-0000-0000-0000-000000000000';
	      }else{
	          $inv_parent = $inv_masterID;
	      }
	      $invsql = "INSERT INTO `{$this->zw->config['robust_db']}`.inventoryfolders (folderName,type,version,folderID,agentID,parentFolderID) VALUES ('$invfldr_name','$inv_type','$version','$invfldr_uuid','$avuuid','$inv_parent')";
	      $sqlop = $this->zw->SQL->query($invsql);
	      $issaved = "";
	      if ($sqlop) {
	        $issaved = true;
	      }else{
	        $issaved = false;
	      }
	    }
	    return $issaved;
	  } // ends function
}
?>