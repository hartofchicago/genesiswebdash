<?php
if (!defined('ZW_IN_SYSTEM')) {
exit;	
}

class money {

	var $zw;

	function money(&$zw) {
		$this->zw = &$zw;
	}

	function aviuuid2name($aviid) {
		$nullkey = "00000000-0000-0000-0000-000000000000";
		if ($aviid == $nullkey) {
			$aviname = "";
		}else{
			$aviname = $this->zw->grid->uuid2name($aviid);
		}
		return $aviname;
	}

	function primuuid2name($primkey = "") {
		$nullkey = "00000000-0000-0000-0000-000000000000";
		if ($primkey == $nullkey || !$primkey) {
			$primname = "<small>Avatar to Avatar Transaction</small>";
		}else if ($primkey != $nullkey) {
			$primq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['search_db']}`.objects WHERE objectuuid = '$primkey'");
			$primr = $this->zw->SQL->fetch_array($primq);
			$primername = $primr['name'];
			if (!$primername) {
				$primname = "Prim not in search<br><small>".$primkey."</small>";
			}else{
				$primname = $primername."<br><small>".$primkey."</small>";
			}
		}
		return $primname;
	}

	function getbalance($uuid) {
		$moneyip = $this->zw->config['Money'];
		if ($moneyip == "true") {
			$moneyq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['money_db']}`.balances WHERE user = '$uuid'");
			$moneyn = $this->zw->SQL->num_rows($moneyq);
			if ($moneyn) {
				$moneyr = $this->zw->SQL->fetch_array($moneyq);
				$money = number_format($moneyr['balance']);
			}else{
				$money = "0";
			}
		}else{
			$money = "0";
		}
		return $money;
	}
}
?>