<?php
if (!defined('ZW_IN_SYSTEM')) {
exit;
}
error_reporting(0);
session_start();
header('Content-Type: text/html; charset=iso-8859-1');

require_once('zw.class.php');
$zw = new zw();

$timezone = $zw->config['TimeZone'];
date_default_timezone_set($timezone);

$ip = $_SERVER['REMOTE_ADDR'];
$thispage = $_SERVER['PHP_SELF'];

$now = time();
$fiveago = $now - 300;
$currenttime = date('M d Y g:ia T', $now);
$u = $zw->Security->make_safe($_GET['u']);
$api = $zw->Security->make_safe($_GET['api']);
$isviewer = $zw->Security->make_safe($_GET['isviewer']);

if ($u) {
	$ustrpos = strpos($u, ".");
	if ($ustrpos === false) {
		$page_title = $page_title." ".$firstname;
	}else{
		$uexplode = explode(".", $u);
		$firstname = $uexplode[0];
		$lastname = $uexplode[1];
		$page_title = $page_title." ".$firstname." ".$lastname;
	}
}else{
}

$gridname = $zw->config['GridName'];
$gridnick = $zw->config['GridNick'];
$site_address = $zw->config['SiteAddress'];
$site_banner = $zw->config['Banner'];
$site_logo = $zw->config['Logo'];
$gridmoney = $zw->config['GridMoney'];
$style = $zw->site->getstyle();
$ip2webassets = $zw->config['WebAssets'];
$notextureuuid = $zw->config['NoPicUUID'];

$twitter = $zw->config['Twitter'];
$facebook = $zw->config['Facebook'];

$user_uuid = $zw->user_info['PrincipalID'];
$user_id = $user_uuid;
$os_user_info = $zw->grid->getosuser_by_uuid($user_uuid);
$user_first = $zw->user_info['FirstName'];
$user_last = $zw->user_info['LastName'];

if (!$user_uuid) {
	$user = "";
	$balance = "";
}else if ($user_uuid) {
	if ($user_last == "Resident") {
		$user = $user_first;
	}else{
		$user = $user_first." ".$user_last;
	}
	$balance = $zw->grid->getbalance();
}

if ($site_logo) {
	$logo = "<img src='".$site_logo."' border='0' width='163' height='20'>";
}else{
	$logo = "<B>".$gridname."</B>";
}

$nullkey = "00000000-0000-0000-0000-000000000000";

if (!$api) {
?>
<!-- AT4cM9iLW376 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>

<meta name="author" content="<?php echo $zw->config['GridName']; ?> web interface.">
<meta name="description" content="Powered by ZetamexWeb">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title id='titlebar'><?php echo $gridname." - ".$page_title; ?></title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<!-- You can change this to your own bootstrap file -->
<link href="<?php echo $site_address; ?>/css/<?php echo $style; ?>/bootstrap.css" rel="stylesheet">
<link href="<?php echo $site_address; ?>/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo $site_address; ?>/css/normalize.css" rel="stylesheet">
<link href="<?php echo $site_address; ?>/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

<script type="text/javascript" src="<?php echo $site_address; ?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $site_address; ?>/js/bootstrap-datetimepicker.min.js"></script>

<!-- For HipChat -->
<script src="https://www.hipchat.com/atlassian-connect/all.js"></script>
<link rel="stylesheet" href="https://www.hipchat.com/atlassian-connect/all.css">

<!-- Fav Icon -->
<link rel="icon" type="image/png" href="<?php echo $address; ?>/favicon.png">
</head>
<body>
<?php
if ($isviewer != "true") {
?>
<style>
body {
	padding-top: 70px;
	padding-left: 25px;
	padding-right: 25px;
	padding-bottom: 70px;
}
</style>
<?php
}else if ($isviewer == "true") {
}
?>
<div class="container-fluid" id="content">
<?php
if ($site_banner) {
	echo "<img src='" . $site_banner . "' border='0'>";
}else if (!$site_banner) {
}

if ($nomenu == "true" || $isviewer == "true") {
}else if (!$nomenu) {
include ('menu.php');
}

if ($hide_sidebars == "true") {
}else{
?>
<div class="row">
<div class="col-md-2">
	<div id="clock">
		<small>
			<?php echo $currenttime; ?>
		</small>
	</div>
		<div class="hidden-sm hidden-xs">
			<div id="fllist">
		<?php
		if ($user_uuid) {
			echo "<div class='panel'>
  			<div class='panel-heading'><B>Friends</B></div>
  			<div class='panel-body'>";
  			echo $zw->grid->getFriends($user_uuid);
  			echo "<small><a href='".$zw->config['SiteAddress']."/friendslist.php'>Manage Friends List</a></small>
  			</div></div>";
  			echo "<div class='panel'>
  			<div class='panel-heading'><B>Groups</B></div>
  			<div class='panel-body'>";
  			echo $zw->grid->getGroups($user_uuid);
  			echo "<small><a href='".$zw->config['SiteAddress']."/groups.php'>Manage Groups</a></small>
  			</div></div>";
		}
		?>
			</div>
		<div class='panel'>
  		 <div class="panel-heading"><B>Social Networks</B></div>
  			<div class="panel-body">
  				<?php
  				if ($twitter) {
	  				echo $zw->site->twitter();
  				}
  				if ($facebook) {
	  				echo "<a href='http://www.facebook.com/".$facebook."'>Like us on Facebook</a>";
  				}
  				?>
	  		</div>
  		 </div>
			<div id="ginfo">
	  		<?php
			echo "<small>";
			echo "<strong>Login</strong> ".$zw->config['loginURI']."<br>";
			if ($zw->config['HGAddress'] != "") {
			echo "<strong>HG</strong> ".$zw->config['HGAddress']."<br>";
			}
			echo $zw->grid->gridstatus();
			echo "<br><a href='".$site_address."/tos.php'>Terms of Service</a><br>
			Built With <a href='http://getbootstrap.com' target='_blank'>Bootstrap</a>.<br>
			Powered by <a href='http://zetaworlds.com' target='_blank'>ZetamexWeb</a><br>";
			echo "</small>";
			?>
			</div>
		</div>
</div>
<div class="col-md-10">
<?php
} // this is to be able to hide the side bars. most useful for full page layout
} // ends if (!$api)
?>