<?php
if (!defined('ZW_IN_SYSTEM')) {
exit;	
}

class forum
{

var $zw;
	
	function forum(&$zw)
	{
		$this->zw = &$zw;
	}

	function countTopics($where)
	{
		$q = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['db_prefix']}forum_topic` $where");
		$c = $this->zw->SQL->num_rows($q);
		return $c;
	}

	function countReplies($where)
	{
		$q = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['db_prefix']}forum_replies` $where");
		$c = $this->zw->SQL->num_rows($q);
		return $c;
	}
}
?>