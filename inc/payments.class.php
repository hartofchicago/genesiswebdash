<?php
if (!defined('ZW_IN_SYSTEM')) {
exit;	
}

class payments {

var $zw;

	function payments(&$zw) {
		$this->zw = &$zw;
	}

	// math problem here trying to calculate the bitcoins required for the requested in world money.
	function bitcoinbuy($amount) {
		$loggedinuuid = $this->zw->user_info['PrincipalID'];
		$BitCoinAddress = $this->zw->config['BitCoinAddress'];
		$money = $this->zw->config['Money'];
		$btcrate = $this->zw->config['BTC2Currency'];
		$converter = $btcrate / 0.001;
		$btcamount = $amount / $converter;
		$invoice_id = $this->zw->site->randcode("20");
		$secret = $this->zw->site->randcode("20");
		$my_callback_url = $this->zw->config['SiteAddress'].'/bt_receive_callback.php?invoice_id='.$invoice_id.'&secret='.$secret;
		$root_url = 'https://blockchain.info/api/receive';
		$parameters = 'method=create&address='.$BitCoinAddress.'&callback='.urlencode($my_callback_url);
		$response = file_get_contents($root_url.'?'.$parameters);
		$decode = json_decode($response);
		$receivedaddress = $decode->input_address;
		$this->zw->SQL->query("INSERT INTO invoice_payments (invoice_id, user_uuid, secret) VALUES ('$invoice_id', '$loggedinuuid', '$secret')");
		$qrbtclabel = "BTC2".$this->zw->config['GridMoney'];
		$btcqr = $this->zw->api->btcqr($BitCoinAddress, $qrbtclabel, $btcamount, "H");
		$return = $this->zw->site->displayalert('Please Send Payment To : '.$BitCoinAddress.' in the BTC amount of '.$btcamount.'<p>'.$btcqr, "success");
		return $return;
	}

}
?>