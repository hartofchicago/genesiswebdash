<?php
if (!defined('ZW_IN_SYSTEM')) {
exit; 
}

class createuser {

var $zw;
  function createuser(&$zw) {
    $this->zw = &$zw;
  }

  function makeuser($avfirst, $avlast, $password, $email, $avtitle, $avlevel) {
  /*
  $avfirst = $_POST['avfirst'];
  $avlast = $_POST['avlast'];
  $password = $_POST['password'];
  $email = $_POST['email'];
  $avtitle = $_POST['avtitle'];
  $avlevel = $_POST['avlevel'];
  */

  $simuuid = $startsim;
  $pos = $startcords;

  // Generate an UUID for the avatar
  $avuuid = $this->zw->getNewUUID();

  // Generate Timestamp
  $avtimestamp = strtotime(date("Y-m-d H:i:s"));

  // Build UserAccounts Row
  $query1 = "INSERT INTO UserAccounts (PrincipalID,ScopeID,FirstName,LastName,Email,ServiceURLs,Created,UserLevel,UserFlags,UserTitle) VALUES ('$avuuid','00000000-0000-0000-0000-000000000000','$avfirst','$avlast','HomeURI= GatekeeperURI= InventoryURI= AssetServerURI= ','$avtimestamp','$avlevel','0','$avtitle')";
  $output1 = $this->zw->SQL->query($query1);
  $salt = $this->zw->Users->generate_password_salt();
  $hashedpass = $this->zw->Users->generate_password_hash($password, $salt);
  $insertauth = $this->zw->SQL->query("INSERT INTO auth (UUID, passwordHash, passwordSalt) VALUES ('$avuuid', '$hashedpass', '$salt')");
  $insertgu = $this->zw->SQL->query("INSERT INTO GridUser (UserID, HomeRegionID, HomePosition, LastRegionID, LastPosition) VALUES ('$avuuid', '$simuuid', '$pos', '$simuuid', '$pos')");

  // Build Inventory
  // write inventory skeleton
  $inv_template = array('Calling Cards' =>  2,
                        'Objects' =>  6,
                        'Landmarks' =>  3,
                        'Clothing' =>  5,
                        'Gestures' => 21,
                        'Body Parts' => 13,
                        'Textures' =>  0,
                        'Scripts' => 10,
                        'Photo Album' => 15,
                        'Lost And Found' => 16,
                        'Trash' => 14,
                        'Notecards' =>  7,
                        'My Inventory' =>  9,
                        'Sounds' =>  1,
                        'Animations' => 20
                       );
  // $inv_masterID is the uuid of the 'My Inventory' folder, the parent folder of all other folders, and which has a
  // folder UUID of UUID.zero
  // all other folders have a unique, random UUID for a folder ID, and the folder ID of the 'My Inventory' folder for their
  // parent folder ID.

  $inv_masterID = $this->zw->getNewUUID();
  $version = "1";

  // echo "Inventory Skeleton Initialized in memory\n";

    foreach ($inv_template as $invfldr_name => $inv_type) {
      $invfldr_uuid = $this->zw->getNewUUID();

      if($inv_type === 9):
          $invfldr_uuid = $inv_masterID;
          $inv_parent = '00000000-0000-0000-0000-000000000000';
      else:
          $inv_parent = $inv_masterID;
      endif;

      // echo "Creating Folder: " . $invfldr_name . "\n";

      $invsql = "INSERT INTO inventoryfolders (folderName,type,version,folderID,agentID,parentFolderID) VALUES ('$invfldr_name','$inv_type','$version','$invfldr_uuid','$avuuid','$inv_parent')";
      $sqlop = $this->zw->SQL->query($invsql);
      $issaved = "";
      if ($sqlop) {
        $issaved = true;
      }else{
        $issaved = false;
      }
    }
    return $issaved;
  } // ends function
}
?>