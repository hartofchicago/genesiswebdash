<?php
if (!defined('ZW_IN_SYSTEM')) {
exit;	
}

class tickets {

var $zw;

	function tickets(&$zw) {
		$this->zw = &$zw;
	}

	function check4tickets() {
		$loggedinuuid = $this->zw->user_info['PrincipalID'];
		$q = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['db_prefix']}tickets` WHERE adminviewstatus = '0' AND user_uuid != '$loggedinuuid'");
		return $this->zw->SQL->num_rows($q);
	}

	function check4replies($type) {
		if ($type == "admin") {
			$q = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['db_prefix']}ticket_reply` WHERE adminviewstatus = '0'");
			$c = $this->zw->SQL->num_rows($q);
		}else if ($type == "user") {
			$loggedinuuid = $this->zw->user_info['PrincipalID'];
			$tq= $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['db_prefix']}tickets` WHERE user_uuid = '$loggedinuuid'");
			$tn = $this->zw->SQL->num_rows($q);
			if ($tn) {
				$c = "";
				while ($tr = $this->zw->SQL->fetch_array($tq)) {
					$tid = $tr['id'];
					$q = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['db_prefix']}ticket_reply` WHERE userviewstatus = '0' AND tid = '$tid'");
					$c .= $this->zw->SQL->num_rows($q);
				}
			}else{
				$c = "0";
			}
			
		}
		return $c;
	}

	function lastreply($id) {
		$q = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['db_prefix']}ticket_reply` WHERE tid = '$id' ORDER BY `id` DESC LIMIT 0,1");
		$n = $this->zw->SQL->num_rows($q);
		if ($n) {
			$r = $this->zw->SQL->fetch_array($q);
		}else{
			$r = "0";
		}
		return $r;
	}

	function setviewed($type, $id, $viewer = "") {
		if ($type == "ticket") {
			$this->zw->SQL->query("UPDATE `{$this->zw->config['db_prefix']}tickets` SET adminviewstatus = '1' WHERE id = '$id'");
		}else if ($type == "reply") {

			if ($viewer == "user") {
				$this->zw->SQL->query("UPDATE `{$this->zw->config['db_prefix']}ticket_reply` SET userviewstatus = '1' WHERE id = '$id'");
			}else if ($viewer == "admin") {
				$this->zw->SQL->query("UPDATE `{$this->zw->config['db_prefix']}ticket_reply` SET adminviewstatus = '1' WHERE id = '$id'");
			}else{

			}
		}
	}

	function replies($id, $type) {
		$loggedinuuid = $this->zw->user_info['PrincipalID'];
		$q = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['db_prefix']}ticket_reply` WHERE tid = '$id' ORDER BY `id` DESC LIMIT 0,10");
		$n = $this->zw->SQL->num_rows($q);
		if ($n) {
			$return = "";
			while($r = $this->zw->SQL->fetch_array($q)) {
				$rid = $r['id'];
				$replyeruuid = $r['user_uuid'];
				$replyer = $this->zw->grid->uuid2name($replyeruuid);
				$body = $r['body'];
				$date = $this->zw->site->time2date($r['time']);
				$online = $this->zw->grid->online($replyeruuid);
				if ($online) {
				$onoff = "onlinedot.png";
				}else{
				$onoff = "offlinedot.png";
				}
				$isOnline = "<img src='".$this->zw->config['SiteAddress']."/img/".$onoff."' border='0'>";
				$return .= "
					<div class='row well'>
						<div class='col-md-4'>
						<B>".$replyer." ".$isOnline."</B><br>
						<small><B>Date:</B> ".$date."</small><br>
						<small><B>Reply #</B> ".$rid."</small>
						</div>
						<div class='col-md-8'>".$body."</div>
					</div>
				";
				if ($loggedinuuid != $replyeruuid) {
					$this->setviewed("reply", $rid, $type);
				}
			}
		}else{
			$return = "0";
		}
		return $return;
	}

	function purgetickets() {
		$now = time();
		$monthago = $now - 2419200;
		$tq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['db_prefix']}tickets` WHERE status = '1' AND time_closed < '$monthago'");
		while ($tr = $this->zw->SQL->fetch_array($tq)) {
			$tid = $tr['id'];
			$this->SQL->query("DELETE FROM `{$this->zw->config['db_prefix']}ticket_reply` WHERE tid = '$tid'");
			$this->SQL->query("DELETE FROM `{$this->zw->config['db_prefix']}tickets` WHERE id = '$tid'");
		}
	}
}
?>