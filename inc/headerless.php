<?php
if (!defined('ZW_IN_SYSTEM')) {
exit;
}
error_reporting(0);
session_start();
header('Content-Type: text/html; charset=iso-8859-1');

require_once('zw.class.php');
$zw = new zw();

date_default_timezone_set($zw->config['TimeZone']);

$ip = $_SERVER['REMOTE_ADDR'];
$thispage = $_SERVER['PHP_SELF'];

$now = time();
$fiveago = $now - 300;

$u = $zw->Security->make_safe($_GET['u']);

if ($u) {
	$ustrpos = strpos($u, ".");
	if ($ustrpos === false) {
		$page_title = $page_title." ".$firstname;
	}else{
		$uexplode = explode(".", $u);
		$firstname = $uexplode[0];
		$lastname = $uexplode[1];
		$page_title = $page_title." ".$firstname." ".$lastname;
	}
}

$gridname = $zw->config['GridName'];
$gridnick = $zw->config['GridNick'];
$site_address = $zw->config['SiteAddress'];
$site_banner = $zw->config['Banner'];
$site_logo = $zw->config['Logo'];
$gridmoney = $zw->config['GridMoney'];
$ip2webassets = $zw->config['WebAssets'];
$notextureuuid = $zw->config['NoPicUUID'];

$twitter = $zw->config['Twitter'];
$facebook = $zw->config['Facebook'];

$user_uuid = $zw->user_info['PrincipalID'];
$user_id = $user_uuid;
$os_user_info = $zw->grid->getosuser_by_uuid($user_uuid);
$user_first = $zw->user_info['FirstName'];
$user_last = $zw->user_info['LastName'];
?>
