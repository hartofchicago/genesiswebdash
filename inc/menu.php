<!-- top menu -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container-fluid">
  	<div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo $site_address; ?>/index.php"><?php echo $logo; ?>
		<?php
		if ($zw->grid->gridonline()) {
			echo "<img src='".$site_address."/img/onlinedot.png' border='0'>";
		}else{
			echo "<img src='".$site_address."/img/offlinedot.png' border='0'>";
		}
		?>
		</a>
    </div>
    <div class="collapse navbar-collapse" id="navbar-collapse-1">

		<!-- <li><a href='<?php echo $site_address; ?>/'>Example of a single menu link</a></li> -->
    	<ul class="nav navbar-nav">

    		<?php
    		$mq = $zw->SQL->query("SELECT * FROM `{$zw->config['db_prefix']}mainmenu` WHERE childof = '0' ORDER BY `sortby` ASC LIMIT 0,100");
    		while($mr = $zw->SQL->fetch_array($mq)) {
    			$mid = $mr['id'];
    			$mname = $mr['name'];
    			$murl = $mr['url'];
    			$msq = $zw->SQL->query("SELECT * FROM `{$zw->config['db_prefix']}mainmenu` WHERE childof = '$mid' ORDER BY `sortby` ASC LIMIT 0,100");
    			$msc = $zw->SQL->num_rows($msq);
    			if ($msc) {
    				echo "<li class='dropdown'>
    						<a href='#' class='dropdown-toggle' data-toggle='dropdown'>".$mname." <b class='caret'></b></a>
    							<ul class='dropdown-menu'>";
    							while ($msr = $zw->SQL->fetch_array($msq)) {
    								$msid = $msr['id'];
    								$msname = $msr['name'];
    								$msurl = $msr['url'];
                    if(strpos($msurl, "http://") !== false || strpos($msurl, "https://") !== false) {
                      $msaddy = $msurl;
                    }else{
                      $msaddy = $site_address."/".$msurl;
                    }
    								echo "<li><a href='".$msaddy."'>".$msname."</a></li>";
    							}
    							echo "</ul>
									</li>";
    			}else{
            if(strpos($murl, "http://") !== false || strpos($murl, "https://") !== false) {
              $maddy = $murl;
            }else{
              $maddy = $site_address."/".$murl;
            }
    				echo "<li><a href='".$maddy."'>".$mname."</a></li>";
    			}
    		}
    		?>
		</ul>
		<!-- This shouldnt be in a menu since its part of the system. -->
		<ul class="nav navbar-nav navbar-right">
			<?php
      if ($user_uuid) {
        $userproflink = str_replace(" ", ".", $user);
        $numofmsg = $zw->grid->check4msgs();
        $numoffr = $zw->grid->check4friendrequest();
        $tnumofnewreplies = $zw->tickets->check4replies("user");
        if ($tnumofnewreplies == "0" || !$tnumofnewreplies) {
          $tnumofnewreplies = "0";
          $ticoncolour = "default";
          $ticontcolour = "default";
        }else{
          $ticoncolour = "danger";
          $ticontcolour = "danger";
        }
        if ($numofmsg == "0" || !$numofmsg) {
          $numofmsg = "0";
          $iconcolour = "default";
          $iconmcolour = "default";
        }else{
          $iconcolour = "danger";
          $iconmcolour = "danger";
        }
        if ($numoffr == "0" || !$numoffr) {
          $numoffr = "0";
          $iconcolour = "default";
          $iconfcolour = "default";
        }else{
          $iconcolour = "danger";
          $iconfcolour = "danger";
        }
        $numofmsgfr = $numofmsg + $numoffr + $tnumofnewreplies;
        $hasmsgs = "<span class='label label-".$iconcolour."''>".$numofmsgfr."</span>";
      ?>
      <li><a href='<?php echo $site_address; ?>/buymoney.php'><?php echo $zw->config['GridMoney']." ".$balance; ?></a></li>
			<li class="dropdown">
				<a href='#' class="dropdown-toggle" data-toggle="dropdown"><?php echo $user." ".$hasmsgs; ?> <b class='caret'></b></a>
				<ul class="dropdown-menu">
					<li><a href='<?php echo $site_address; ?>/profile.php?u=<?php echo $userproflink;?>'>Profile</a></li>
          <li><a href='<?php echo $site_address; ?>/usersettings.php'>User Control Panel</a></li>
          <li><a href='<?php echo $site_address; ?>/myregions.php'>Your Regions</a></li>
          <li><a href='<?php echo $site_address; ?>/friendslist.php'>Friends List <span class="label label-<?php echo $iconfcolour; ?>"><?php echo $numoffr; ?></span></a></li>
          <li><a href='<?php echo $site_address; ?>/offlineim.php'>Offline Messages <span class="label label-<?php echo $iconmcolour; ?>"><?php echo $numofmsg; ?></span></a></li>
          <li><a href='<?php echo $site_address; ?>/tickets/mytickets.php'>My Tickets <span class="label label-<?php echo $ticontcolour; ?>"><?php echo $tnumofnewreplies; ?></span></a></li>
          <li><a href='<?php echo $site_address; ?>/transactions.php'>Transaction History</a></li>
          <li><a href='<?php echo $site_address; ?>/eventsmanager.php'>Events Manager</a></li>
          <li class="divider"></li>
          <li><a href='<?php echo $site_address; ?>/logout.php'>Logout</a></li>
				</ul>
			<li>
<?php
if ($zw->grid->isAdmin($user_uuid)) {
  $anumofnewtickets = $zw->tickets->check4tickets();
  $anumofnewreplies = $zw->tickets->check4replies("admin");
  $anumofnewticketsnreply = $anumofnewtickets + $anumofnewreplies;
  if ($anumofnewticketsnreply == "0" || !$anumofnewticketsnreply) {
    $anumofnewticketsnreply = "0";
    $aticoncolour = "default";
    $aticonmcolour = "default";
  }else{
    $aticoncolour = "danger";
    $aticonmcolour = "danger";
  }
  $athasmsgs = "<span class='label label-".$aticoncolour."''>".$anumofnewticketsnreply."</span>";
?>
      <li class="dropdown">
        <a href='#' class="dropdown-toggle" data-toggle="dropdown">Admin <?php echo $athasmsgs; ?><b class='caret'></b></a>
        <ul class="dropdown-menu">
          <li><a href='<?php echo $site_address; ?>/admin/settings.php'>Settings</a></li>
          <li><a href='<?php echo $site_address; ?>/admin/menubar.php'>Menu Bar</a></li>
          <li><a href='<?php echo $site_address; ?>/admin/news.php'>News</a></li>
          <li><a href='<?php echo $site_address; ?>/admin/regions.php'>Regions</a></li>
          <li><a href='<?php echo $site_address; ?>/admin/users.php'>Users</a></li>
          <li><a href='<?php echo $site_address; ?>/admin/styles.php'>Styles</a></li>
          <li><a href='<?php echo $site_address; ?>/admin/eletters.php'>Newsletter</a></li>
          <li><a href='<?php echo $site_address; ?>/admin/tickets.php'>Tickets <span class="label label-<?php echo $aticonmcolour; ?>"><?php echo $anumofnewticketsnreply; ?></span></a></li>
        </ul>
      </li>
<?php
}else{ // if user is admin
}
}else{ // if user is logged in
?>
			<li><a href='<?php echo $site_address; ?>/login.php?lp=<?php echo $thispage; ?>'>Login</a></li>
			<li><a href='<?php echo $site_address; ?>/register.php'>Register</a></li>
			<?php } ?>
		</ul>
    </div>
  </div>
</nav>
