<?php
if (!defined('ZW_IN_SYSTEM')) {
exit;	
}

class Users
{

var $zw;
	
	function Users(&$zw)
	{
		$this->zw = &$zw;
	}

	function validate_password($input) {
		if (strlen($input) <= $this->zw->config['max_password'] && strlen($input) >= $this->zw->config['min_password']) {
		    return true;
		}else{
		    return false;
		}
	}

	function validate_login() {
		if ($this->zw->Sessions->find_session()) {
		    return true;
		}else{
		    return false;
		}
	}

	function generate_password_salt() {
		$randomuuid = $this->zw->getNewUUID();
		$strrep = str_replace("-", "", $randomuuid);
		return md5($strrep);
	}

	function generate_password_hash($psswrd, $code) {
		return md5(md5($psswrd).":".$code);
	}

	function compare_passwords($input_password, $real_password, $code) {
        $input_hash = $this->generate_password_hash($input_password, $code);

		if ($input_hash == $real_password) {
		    return true;
		}else{
		    return false;
		}
	}

	function login($FirstName, $LastName, $pass, $remember) {
		if ($LastName == "") {
			$LastName = "Resident";
		}

		$q1 = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.UserAccounts WHERE FirstName = '$FirstName' AND LastName = '$LastName'");
		$n1 = $this->zw->SQL->num_rows($q1);
		if ($n1) {
			$r1 = $this->zw->SQL->fetch_array($q1);
			$userUUID = $r1['PrincipalID'];

			$q2 = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.auth WHERE UUID = '$userUUID'");
			$r2 = $this->zw->SQL->fetch_array($q2);
			$user_pass = $r2['passwordHash'];
			$user_code = $r2['passwordSalt'];

			$time = time();

			if ($this->validate_password($pass)) {
				if ($this->compare_passwords($pass, $user_pass, $user_code)) {
					if ($remember == "1") {
						$this->zw->Sessions->create_session($userUUID, "true");
					}else{
						$this->zw->Sessions->create_session($userUUID, "false");
					}
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function logout_user() {
	    $session_names = array('id', 'time', 'code');
	    $ses_uuid = $_SESSION[$this->zw->config['cookie_prefix'] . 'id'];
		if (isset($ses_code)) {
            $this->zw->SQL->query("DELETE FROM `{$this->zw->config['db_prefix']}sessions` WHERE id = '$ses_uuid'");
		}
        $_SESSION = array();

		if (isset($_COOKIE[session_name()])) {
		    setcookie(session_name(), '', time() - 42000, '/');
		}

		if (isset($_COOKIE[$this->zw->config['cookie_prefix'] . 'id'])) {
			foreach ($session_names as $value) {
			    setcookie($this->zw->config['cookie_prefix'] . $value, 0, time() - 3600, $this->zw->config['cookie_path'], $this->zw->config['cookie_domain'], false, false);
			}
		}
		return true;
	}

	function check_user_exist($first, $last) {
		if (!$first) {
			return false;
		}
		$q = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.UserAccounts WHERE FirstName = '$first' AND LastName = '$last'");
		$r = $this->zw->SQL->fetch_array($q);
		if ($r['PrincipalID']) {
			return true;
		}else{
			return false;
		}
	}

	function uuid_to_username($user_uuid) {
        $user_uuid = (is_numeric($user_uuid) && $user_uuid > 0) ? $user_uuid : 0;
        $result = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$user_uuid'");
        $row = $this->zw->SQL->fetch_array($result);
        $first = $row['FirstName'];
        $last = $row['LastName'];
        if (!$last) {
        	$return = $first;
        }else{
        	$return = $first." ".$last;
        }
        return $return;
	}

	function emailconfirmation($uuid, $email, $isnewuser) {
		$randomuuid = $this->zw->site->randcode($this->zw->config['APITokenLength']);
		$this->zw->SQL->query("INSERT INTO `{$this->zw->config['db_prefix']}emailconfirm` (uuid, email, code, isnewuser) VALUES ('$uuid', '$email', '$randomuuid', '$isnewuser')");
		$resetaddyer = $this->zw->config['SiteAddress']."/confirmemail.php?email=".$email."&code=".$randomuuid;
		$esubject = $this->zw->config['GridName']." email validation.";
		$emessage = "Please validate your email address for ".$this->zw->config['GridName']." by clicking the link below.<br>
		<a href='".$resetaddyer."'>".$resetaddyer."</a>";
		$this->zw->site->sendemail($email, $esubject, $emessage);
	}

	function emailconfirmed($email, $code) {
		$q = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['db_prefix']}emailconfirm` WHERE code = '$code' AND email = '$email'");
		$n = $this->zw->SQL->num_rows($q);
		if ($n) {
			$r = $this->zw->SQL->fetch_array($q);
			$uuid = $r['uuid'];
			$isnewuser = $r['isnewuser'];
			if ($isnewuser == "y") {
				$UserLevel = "0";
				$UserTitle = "Local User";
				$this->SQL->query("UPDATE `{$this->zw->config['robust_db']}`.UserAccounts SET UserLevel = '$UserLevel', UserTitle = '$UserTitle' WHERE PrincipalID = '$uuid'");
			}else if ($isnewuser == "n") {

			}
			$this->zw->SQL->query("DELETE FROM `{$this->zw->config['db_prefix']}emailconfirm` WHERE uuid = '$uuid'");
			return true;
		}else{
			return false;
		}
	}

	function register_user() {
		$first = $_POST['firstname'];
		$last = $_POST['lastname'];
		$pass = $_POST['password'];
		$cpass = $_POST['password_c'];
		$email = $_POST['email'];
		$referredby = $_POST['referredby'];

		if (!$last || $last == "Linden") {
			$last = "Resident";
		}
		if ($referredby == "NA") {
			$referredby = "";
		}
		
		if ($this->zw->config['security_image'] == "yes") {
			require_once('recaptchalib.php');
			$privatekey = $this->zw->config['ReCaptcha_Private_Key'];
			$resp = recaptcha_check_answer ($privatekey,
	                            $_SERVER["REMOTE_ADDR"],
	                            $_POST["recaptcha_challenge_field"],
	                            $_POST["recaptcha_response_field"]);
			if (!$resp->is_valid) {
				// What happens when the CAPTCHA was entered incorrectly
				return $this->zw->site->displayalert('ReCaptcha is wrong', "danger");
				die ("The reCAPTCHA wasn't entered correctly. Go back and try it again. (reCAPTCHA said: " . $resp->error . ")");
				$isago = false;
			}else{
				$isago = true;
			}
		}else if ($this->zw->config['security_image'] == "no") {
			$isago = true;
		}
		if ($isago) {
			// Your code here to handle a successful verification
			if ($this->check_user_exist($first, $last)) {
				// If avatar name already exist in the database we fail the registration. Dont need two Asshat Jockstrap's running around.
				return $this->zw->site->displayalert('User already exist', "danger");
			}else{
				if ($pass == $cpass) { // this makes sure that both password feilds are the same
					if ($this->validate_password($pass)) { // this makes sure the password entered is a valid length.
						$findme = '@';
						$echeck = strpos($email, $findme);
						if ($echeck !== false) {
							// now we can start processing the registration.
							$randomuuid = $this->zw->getNewUUID();
							$salt = $this->generate_password_salt();
							$hashedpass = $this->generate_password_hash($pass, $salt);
							$time = time();
							$pos = "<".$this->zw->config['Default_Pos'].">";
							$simname = $this->zw->config['Default_Sim'];
							if ($simname) {
								$simnametouuidq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.regions WHERE regionName = '$simname'");
								$simnametouuidr = $this->zw->SQL->fetch_array($simnametouuidq);
								$simuuid = $simnametouuidr['uuid'];
							}else{
								$simuuid = "00000000-0000-0000-0000-000000000000";
							}

							$activation_type = $this->zw->config['activation_type'];
							if ($activation_type == "0") {
								$UserLevel = "0";
								$UserTitle = "Local User";
							}else{
								$UserLevel = "-2";
								$UserTitle = "Pending";
							}
							$insert1 = $this->zw->SQL->query("INSERT INTO `{$this->zw->config['robust_db']}`.UserAccounts (PrincipalID, ScopeID, FirstName, LastName, Email, Created, UserLevel, UserTitle) VALUES ('$randomuuid', '00000000-0000-0000-0000-000000000000', '$first', '$last', '$email', '$time', '$UserLevel', '$UserTitle')");
							$insert2 = $this->zw->SQL->query("INSERT INTO `{$this->zw->config['robust_db']}`.auth (UUID, passwordHash, passwordSalt) VALUES ('$randomuuid', '$hashedpass', '$salt')");
							$insert3 = $this->zw->SQL->query("INSERT INTO `{$this->zw->config['robust_db']}`.GridUser (UserID, HomeRegionID, HomePosition, LastRegionID, LastPosition) VALUES ('$randomuuid', '$simuuid', '$pos', '$simuuid', '$pos')");
							if ($insert1 && $insert2 && $insert3) {
								if ($this->zw->Avatar->createinv($randomuuid)) {
									$isregistered = true;
								}else{
									$isregistered = false;
								}
								$postavi = $_POST['avi'];
								$avim = $this->zw->config['Default_Male'];
								$avif = $this->zw->config['Default_Female'];
								if ($postavi != "") {
									if ($postavi == "m" && $avim != "") {
										$this->zw->Avatar->createlook($randomuuid, $avim);
									}else if ($postavi == "f" && $avif != "") {
										$this->zw->Avatar->createlook($randomuuid, $avif);
									}else{
										// do nothing
									}
								}else if ($postavi == "") {
									// do nothing since no avi is set
								}
							}else{
								$isregistered = false;
							}
							if ($activation_type == "0" && $isregistered) {
								return $this->zw->site->displayalert('<strong>REGISTERED!</strong> Welcome to '.$this->zw->config['GridName'], "success");
							}else if ($activation_type == "1" && $isregistered) {
								$this->emailconfirmation($randomuuid, $email, "y");
								return $this->zw->site->displayalert('<strong>REGISTERED!</strong> However this grid requires you to confirm your email address.', "success");
							}else if ($activation_type == "2" && $isregistered) {
								$this->zw->api->sendmsg($first." ".$last." has joined ".$this->zw->config['GridName'], "yellow");
								return $this->zw->site->displayalert('<strong>REGISTERED!</strong> However this grid requires a admin to approve you.', "success");
							}else if ($activation_type >= "2") {
								return $this->zw->site->displayalert('Unable to figure out how to register you to the grid.', "danger");
							}else if ($isregistered == false) {
								return $this->zw->site->displayalert('ERROR in saving your data to the ZetamexWeb database.', "danger");
							}
						}else if ($echeck === false) {
							return $this->zw->site->displayalert('Incorrect email address', "danger");
						}
					}else{
						return $this->zw->site->displayalert('Password is incorrect', "danger");
					}
				}else{
					return $this->zw->site->displayalert('The two passwords you entered do not match with each other.', "danger");
				}
			}
		}
	}

	function get_zw_user($uuid) {
		$getuuidq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['db_prefix']}users` WHERE uuid = '$uuid'");
		$getuuidr = $this->zw->SQL->fetch_array($getuuidq);
		return $getuuidr;
	}

	function changepassword($currentpass, $newpass, $confirmpass) {
		if ($currentpass && $newpass && $confirmpass) {
			$loggedinuuid = $this->zw->user_info['PrincipalID'];
			$q1 = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.auth WHERE UUID = '$loggedinuuid'");
			$n1 = $this->zw->SQL->num_rows($q1);
			if ($n1) {
				$r1 = $this->zw->SQL->fetch_array($q1);
				$user_pass = $r1['passwordHash'];
				$user_code = $r1['passwordSalt'];
				if ($this->validate_password($currentpass)) {
					if ($this->compare_passwords($currentpass, $user_pass, $user_code)) {
						if ($newpass == $confirmpass) {
							$salt = $this->generate_password_salt();
							$hashedpass = $this->generate_password_hash($newpass, $salt);
							$time = time();
							$pupdate = $this->zw->SQL->query("UPDATE `{$this->zw->config['robust_db']}`.auth SET passwordHash = '$hashedpass', passwordSalt = '$salt' WHERE UUID = '$loggedinuuid'");
							if ($pupdate) {
								$getemailq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$loggedinuuid'");
								$getemailr = $this->zw->SQL->fetch_array($getemailq);
								$email = $getemailr['Email'];
								$firstname = $getemailr['FirstName'];
								$lastname = $getemailr['LastName'];
								$resetaddyer = $this->zw->config['SiteAddress']."/resetpassword.php";
								$esubject = "Your ".$this->zw->config['GridName']." password has been changed.";
								$emessage = "Your ".$this->zw->config['GridName']." password for ".$firstname." ".$lastname." has been changed.<br>
								If you did not do this then please visit <a href='".$resetaddyer."'>".$resetaddyer."</a> for a new temporary password.";
								$this->zw->site->sendemail($email, $esubject, $emessage);
								return true;
							}else{
								return false;
							}
						}else{
							return false;
						}
					}else{
						return false;
					}
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function resetpass($uuid) {
		if ($uuid != "") {
			$checkifexistq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$uuid'");
			$checkifexistn = $this->zw->SQL->num_rows($checkifexistq);
			if ($checkifexistn) {
				$checkifexistr = $this->zw->SQL->fetch_array($checkifexistq);
				$email = $checkifexistr['Email'];
				$FirstName = $checkifexistr['FirstName'];
				$LastName = $checkifexistr['LastName'];
				$randpass = $this->zw->site->randcode($this->zw->config['min_password']);
				$salt = $this->generate_password_salt();
				$hashedpass = $this->generate_password_hash($randpass, $salt);
				$time = time();
				$pupdate = $this->zw->SQL->query("UPDATE `{$this->zw->config['robust_db']}`.auth SET passwordHash = '$hashedpass', passwordSalt = '$salt' WHERE UUID = '$uuid'");
				if ($pupdate) {
					$resetaddy = $this->zw->config['SiteAddress']."/login.php";
					$esubject = "Your new ".$this->zw->config['GridName']." temporary password.";
					$emessage = "Your new ".$this->zw->config['GridName']." temporary password for <B>".$FirstName." ".$LastName."</B> is:<br>
					".$randpass."<br>
					Please visit <a href='".$resetaddy."'>".$resetaddy."</a> with your new temporary password.";
					$this->zw->site->sendemail($email, $esubject, $emessage);
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function synusers($display) {
		$totalsynced = 0;
		$synusersq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['robust_db']}`.UserAccounts");
		while ($synusersr = $this->zw->SQL->fetch_array($synusersq)) {
			$synuuid = $synusersr['PrincipalID'];
			$syngetq = $this->zw->SQL->query("SELECT * FROM `{$this->zw->config['db_prefix']}users` WHERE uuid = '$synuuid'");
			$syngetn = $this->zw->SQL->num_rows($syngetq);
			if (!$syngetn) {
				$synFirst = $synusersr['FirstName'];
				$synLast = $synusersr['LastName'];
				$synUserTitle = $synusersr['UserTitle'];
				if ($synUserTitle == "Banned") {
				}else{
					$synname = $synFirst." ".$synLast;
					if ($synUserTitle == "Local User") {
						$synstatus = "approved";
					}else if ($synUserTitle == "Pending") {
						$synstatus = "waiting";
					}
					$this->zw->SQL->query("INSERT INTO `{$this->zw->config['db_prefix']}users` (uuid, name, activationstatus) VALUES ('$synuuid','$synname','$synstatus')");
					++$totalsynced;
				}
			}
		}
		if ($display == "showcount") {
			return $this->zw->site->displayalert('Syncing users is complete.<br><B>Total Synced:</B> '.$totalsynced, "success");
		}else if ($display == "dontshow") {
		}
	}
}
?>
