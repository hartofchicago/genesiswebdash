<?php
if (!defined('ZW_IN_SYSTEM')) {
exit;	
}

class SQLite {

var $zw;

	function SQLite($server_name, $username, $password, $name, &$zw, $port = 0666) {
	$this->zw = &$zw;
    $this->server_name = $server_name;
    $this->username = $username;
    $this->password = $password;
    $this->name = $name;
    $this->port = $port;
	$this->connection = sqlite_open($this->server_name, $this->port, $sqliteerror);
		if (!$sqliteerror) {
		die("Unable to connect to a sqlite database");
		}
	}

	function result($q,$i,$d) {
	return sqlite_current($q,$i,$d);
	}

	function affected_rows() {
	return sqlite_changes($this->connection);
	}

	function fetch_row($result) {
	return sqlite_fetch_array($result, SQLITE_BOTH);	
	}

	function fetch_assoc($result) {
	return sqlite_fetch_all($result, SQLITE_BOTH);
	}

	function fetch_array($result) {
	return sqlite_fetch_array($result, SQLITE_BOTH);
	}

	function fetch_object($result) {
	return sqlite_fetch_object($result);
	}

	function free_result($result) {
		if ($this->last_query == $result) {
		$this->last_query = '';
		}

	return NULL;
	}

	function get_client_info() {
	return sqlite_libversion();
	}

	function insert_id() {
	return sqlite_last_insert_rowid();
	}

	function num_fields($result) {
	return sqlite_num_fields($result);
	}

	function num_rows($result) {
	return sqlite_num_rows($result);
	}

	function transaction($status = 'BEGIN') {
		switch (strtoupper($status)) {
			default:
			return true;
			break;
			case 'START':
			case 'START TRANSACTION':
			case 'BEGIN':
			return sqlite_query($this->connection, 'START TRANSACTION');
			break;
			case 'COMMIT':
			return sqlite_query($this->connection, 'COMMIT');
			break;
			case 'ROLLBACK':
			return sqlite_query($this->connection, 'ROLLBACK');
			break;
		}
	}

	function query($query) {
		if ($query != '') {
			$result = sqlite_query($this->connection, $query);
			return $result;
		}else{
			$result = sqlite_query($this->connection, '');
			return $result;
		}
	}

	function close() {
	return sqlite_close($this->connection);
	}
}
?>
