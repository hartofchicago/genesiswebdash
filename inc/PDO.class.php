<?php
if (!defined('ZW_IN_SYSTEM')) {
exit;	
}

class PDO {

var $zw;

	function PDO($server_name, $username, $password, $name, &$zw, $port = false) {
		$this->zw = &$zw;
	    $this->server_name = $server_name;
	    $this->username = $username;
	    $this->password = $password;
	    $this->name = $name;
	    $this->port = $port;
		$this->connection =  new PDO('mysql:host='.$this->server_name.';port='.$this->port.';dbname='.$this->name, $this->username, $this->password, array( PDO::ATTR_PERSISTENT => false));
	}

	function query($query) {
		return $this->connection->query($query, PDO::FETCH_ASSOC);
	}

	function fetch_array($query) {
		return $query->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_PRIOR);
	}

	function num_rows($query) {
		return $query->rowCount();
	}

}
?>