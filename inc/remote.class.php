<?php
//$RA = new RemoteAdmin('localhost', 9000, 'secret');
if (!defined('ZW_IN_SYSTEM')) {
exit;	
}

class remote {

var $zw;

	function remote(&$zw) {
		$this->zw = &$zw;
	}

	function sendmessage($command, $params) {
		$RA = $this->zw->RemoteAdmin->SendCommand($command, $params);
        if ($RA === FALSE) {
             return 'ERROR';
        }else{
        	$success = $RA['success'];
        	$error = $RA['error'];
        	if ($success) {
        		$return = "Successful<br>";
        	}else{
        		$return = "A error as occured: ".$error."<br>";
        	}
        	return $return;
        }
	}

	function admin_broadcast($msg) {
		$parameters = array('message' => $msg);
		return $this->sendmessage('admin_broadcast', $parameters);
	}

	function restartsim($regionID) {
		$parameters = array('regionID' => $regionID);
		return $this->sendmessage('admin_restart', $parameters);
	}
}
?>