<?php
if (!defined('ZW_IN_SYSTEM')) {
exit;	
}

class PostgreSQL {

var $zw;

	function PostgreSQL($server_name, $username, $password, $name, &$zw, $port = 5432) {
	$this->zw = &$zw;
    $this->server_name = $server_name;
    $this->username = $username;
    $this->password = $password;
    $this->name = $name;
    $this->port = $port;
    $constr = "host=".$this->server_name." port=".$this->port." dbname=".$this->name." user=".$this->username." password=".$this->password;
	$this->connection = pg_connect($constr);
		if (pg_connection_status($this->connection) === PGSQL_CONNECTION_BAD) {
		die("Unable to connect to a PostgreSQL database");
		}
	}

	function result($q,$i,$d) {
	return pg_result_seek($q,$i);
	}

	function affected_rows() {
	return pg_affected_rows($this->connection);
	}

	function fetch_row($result) {
	return pg_fetch_row($result);	
	}

	function fetch_assoc($result) {
	return pg_fetch_assoc($result);
	}

	function fetch_array($result) {
	return pg_fetch_array($result, PGSQL_BOTH);
	}

	function fetch_object($result) {
	return pg_fetch_object($result);
	}

	function free_result($result) {
		if ($this->last_query == $result) {
		$this->last_query = '';
		}

	return pg_free_result($result);
	}

	function get_client_info() {
	return pg_version($this->connection);
	}

	function insert_id() {
	return false;
	}

	function num_fields($result) {
	return pg_num_fields($result);
	}

	function num_rows($result) {
	return pg_num_rows($result);
	}

	function transaction($status = 'BEGIN') {
		switch (strtoupper($status)) {
			default:
			return true;
			break;
			case 'START':
			case 'START TRANSACTION':
			case 'BEGIN':
			return pg_query($this->connection, 'START TRANSACTION');
			break;
			case 'COMMIT':
			return pg_query($this->connection, 'COMMIT');
			break;
			case 'ROLLBACK':
			return pg_query($this->connection, 'ROLLBACK');
			break;
		}
	}

	function query($query) {
		if ($query != '') {
			$result = pg_query($this->connection, $query);
			return $result;
		}else{
			$result = pg_query($this->connection, '');
			return $result;
		}
	}

	function close() {
	return pg_close($this->connection);
	}
}
?>
