<?php
if (!defined('ZW_IN_SYSTEM')) {
exit;	
}

class hipchat {

	var $zw;

	function hipchat(&$zw) {
		$this->zw = &$zw;
		$this->HipChatToken = $this->zw->config['HipChatToken'];
		$this->publicroomid = $this->zw->config['HipChatRoomID'];
		$this->privateroomid = $this->zw->config['HipChatPrivateRoomID'];
		$this->supportnames = $this->zw->config['HipChatSupportNames'];
	}

	function send($service, $http_method = 'GET', $args = NULL) {
		$url = "https://api.hipchat.com/v2/".$service;
		$args['format'] = 'json';
    	$args['auth_token'] = $this->zw->config['HipChatToken'];
    	$post_data = null;
    	if ($http_method == 'GET') {
    		$url .= '?'.http_build_query($args);
    	}else if ($http_method == 'POST') {
    		$data = $args;
    	}
		$c = curl_init();
		curl_setopt($c, CURLOPT_URL, $url);
	    curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
	    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($c, CURLOPT_TIMEOUT, 15);
    	curl_setopt($c, CURLOPT_SSL_VERIFYPEER, true);
		if (is_array($data) && $http_method == 'POST') {
			curl_setopt($c, CURLOPT_POST, 1);
			curl_setopt($c, CURLOPT_POSTFIELDS, $data);
			curl_setopt($c, CURLOPT_HTTPHEADER, array("Content-Type: text/xml; charset=utf-8", "Expect: 100-continue"));
		}else if ($http_method == 'GET') {
			curl_setopt($c, CURLOPT_HTTPGET, true);
		}
		return curl_exec($c);
	}

	function sendinstallback($callbackurl, $data = array()) {
		$url = $callbackurl;
		$c = curl_init();
		curl_setopt($c, CURLOPT_URL, $url);
	    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_POSTFIELDS, $data);
		curl_setopt($c, CURLOPT_HTTPHEADER, array("Content-Type: text/xml; charset=utf-8", "Expect: 100-continue"));
		$response = curl_exec($c);
	}

	function getroom() {
		return $this->send("room/".$this->publicroomid."/", "GET");
	}

	function getroomusers() {
		return $this->send("room/".$this->publicroomid."/member/", "GET");
	}

	function sendmessage($message, $color, $notify = true, $message_format = "text") {
		$args = array(
	      'room_id' => $this->zw->config['HipChatPrivateRoomID'],
	      'from' => $this->zw->config['GridName'],
	      'message' => $message,
	      'notify' => (int)$notify,
	      'color' => $color,
	      'message_format' => $message_format
	    );
		return $this->send("room/".$this->zw->config['HipChatPrivateRoomID']."/notification", "POST", $args);
	}

	function ishipchatsupportstaff($names) {
		$supportnames = $this->zw->config['HipChatSupportNames'];
		$a = explode("|", $names);
		$b = explode(", ", $supportnames);
		$check = strpos($a, $b);
		if ($check !== false) {
			return true;
		}else{
			return false;
		}
	}

	function fetchLSLsupportroomstatus() {
		$members = $this->getroomusers();
		$json = json_decode($members, true);
		if ($json['is_guest_accessible'] == 1) {
			$url = $json['guest_access_url'];
			$lslsupportnames = "";
			$apiarraynames = "";
			$explodesupport = explode(", ", $this->supportnames);
			foreach ($json['participants'] as $items) {
				$hipchatuser = $items['name'];
				foreach ($explodesupport as $supportname) {
					if ($supportname == $hipchatuser) {
						$lslsupportnames .= "true";
					}else{
						$lslsupportnames .= "";
					}
				}
			}
			if ($lslsupportnames) {
				$lslname = true;
			}else{
				$lslname = false;
			}
			$lslapi = $url."=".$lslname;
		}else{
			$lslapi = "Room Not Public";
			$apiarray = array('ERROR' => 'Room not public');
		}
		return $lslapi;
	}

	function fetchroomstatus($api) {
		$members = $this->getroomusers();
		$json = json_decode($members, true);
		if ($json['is_guest_accessible'] == 1) {
			$url = $json['guest_access_url'];
			$lslname = "";
			$apiarraynames = "";
			foreach ($json['participants'] as $items) {
				$hipchatuser = $items['name'];
				$apiarraynames[] = array('Name' => $hipchatuser);
				$lslnames .= $hipchatuser."|";
			}
			$apiarray = array('URL' => $url, 'Users' => $apiarraynames);
			$lslnames .= "~~";
			$lslname = str_replace("|~~", "", $lslnames);
			$lslname = str_replace("~~", "", $lslname);
			$lslapi = $url."=".$lslname;
		}else{
			$lslapi = "Room Not Public";
			$apiarray = array('ERROR' => 'Room not public');
		}
		if ($api == "json") {
			$return = json_encode($apiarray);
		}
		if ($api == "xml") {
			$return = xmlrpc_encode($apiarray);
		}
		if ($api == "lsl") {
			$return = $lslapi;
		}
		if ($api == "internalcheck") {
			$return = $this->ishipchatsupportstaff($lslname);
		}
		if ($api == "html") {
			if ($lslname) {
				$supportstaff = $this->ishipchatsupportstaff($lslname);
				if ($supportstaff) {
					$return = $url;
				}else{
					$return = "";
				}
			}else{
				$return = "";
			}
		}
		return $return;
	}
}
?>