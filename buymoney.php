<?php
$page_title = "Buy Money";
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');
$money = $zw->config['Money'];
$gamount = $zw->Security->make_safe($_GET['amount']);
$gtype = $zw->Security->make_safe($_GET['type']);
if ($zw->grid->isAdmin($user_uuid)) { // change this to if ($user_uuid) when debug is done
//if ($user_uuid && $money == "true") {
	$submit = $zw->Security->make_safe($_POST['submit']);
	$amount = $zw->Security->make_safe($_POST['amount']);
	$PaymentType = $zw->Security->make_safe($_POST['PaymentType']);
	$uuid = $user_uuid;
	$now = time();
	if ($submit && is_numeric($amount)) {
		$PaymentType = strtolower($PaymentType);
		if ($PaymentType == "bitcoin") {
			echo $zw->payments->bitcoinbuy($amount);
		}
	}
	$paymentsexplode = explode(", ", $zw->config['PaymentType']);
	$paymentsselect = "";
	foreach ($paymentsexplode as $key) {
		if ($key != "") {
			if ($gtype == $key) {
				$gsel = "SELECTED";
			}else{
				$gsel = "";
			}
			$paymentsselect .= "<option value='".$key."' ".$gsel.">".$key."</option>";
		}
	}
echo "
<form method='post' action='' class='form-horizontal' role='form'>
  <div class='form-group'>
    	<label class='col-sm-2 control-label' for='inputAmount'>Amount in ".$zw->config['GridMoney']."</label>
    	<div class='col-sm-10'>
			<div class='input-group'>
				<span class='input-group-addon'>".$zw->config['GridMoney']."</span>
				<input type='number' name='amount' value='".$gamount."' class='form-control' id='inputAmount' placeholder='300'>
			</div>
		</div>
  </div>
  <div class='form-group'>
    	<label class='col-sm-2 control-label' for='inputPaymentMethod'>Payment Method</label>
    	<div class='col-sm-10'>
			<select name='PaymentType' class='form-control' id='inputPaymentMethod'>
				".$paymentsselect."
			</select>
		</div>
  </div>
  <div class='form-group'>
    	<label class='col-sm-2 control-label' for='inputSubmit'></label>
    	<div class='col-sm-10'>
			<input type='submit' name='submit' value='Submit' id='inputSubmit' class='btn btn-success'>
		</div>
  </div>
</form>";
}else if ($money == "false" || $money == "") {
echo $zw->site->displayalert("This grid has not setup a money system yet. Please check back later.", "danger");
}else if(!$user_uuid) {
echo $zw->site->displayalert("You need to be logged into the ".$zw->config['GridName']." website to buy money.", "danger");
} // ends if ($user_uuid)
include ('inc/footer.php');
?>