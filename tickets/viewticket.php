<?php
$page_title = "My Tickets";
define('ZW_IN_SYSTEM', true);
require_once('../inc/header.php');

$id = $zw->Security->make_safe($_GET['id']);
if ($user_uuid && $id) {

$send = $zw->Security->make_safe($_POST['send']);
if ($send == "Send Reply") {
	$replymsg = $zw->Security->make_safe($_POST['replymsg']);
	if ($replymsg != "") {
		$now = time();
		$i = $zw->SQL->query("INSERT INTO `{$zw->config['db_prefix']}ticket_reply` (tid, user_uuid, body, userviewstatus, adminviewstatus, time) VALUES ('$id', '$user_uuid', '$replymsg', '1', '0', '$now')");
		if ($i) {
			echo $zw->site->displayalert("Reply saved.", "success");
			$zw->api->sendmsg("New reply to ticket #".$id." from ".$zw->grid->uuid2name($user_uuid), $colour = "yellow");
		}else{
			echo $zw->site->displayalert("Unable to save reply. Please try later.", "danger");
		}
	}
}

echo "<a href='mytickets.php' class='btn btn-info'>Back to your ticket list</a><p>";

$tq = $zw->SQL->query("SELECT * FROM `{$zw->config['db_prefix']}tickets` WHERE id = '$id' AND user_uuid = '$user_uuid'");
$tn = $zw->SQL->num_rows($tq);
if ($tn) {
	$tr = $zw->SQL->fetch_array($tq);
	$subject = $tr['subject'];
	$body = $tr['body'];
	$dateopen = $zw->site->time2date($tr['time_created']);
	$username = $zw->grid->uuid2name($user_uuid);
	$arelastreplies = $zw->tickets->replies($id, "user");
	if ($arelastreplies != "0") {
		$lastreplies = $arelastreplies;
	}else{
		$lastreplies = "";
	}
	$online = $zw->grid->online($uuid);
	if ($online) {
	$onoff = "onlinedot.png";
	}else{
	$onoff = "offlinedot.png";
	}
	$isOnline = "<img src='".$site_address."/img/".$onoff."' border='0'>";
	echo "
	<div class='row well'>
		<div class='col-md-4'><B>".$username." ".$isOnline."</B><br><small><B>Date:</B> ".$dateopen."</small></div>
		<div class='col-md-8'><B>".$subject."</B><br>".$body."</div>
		<div class='clearfix visible-xs-block'></div>
	</div>
	<h3>Latest Replies<br><small><a href='#reply'>Create a reply</a></small></h3>
	".$lastreplies;
	if ($tr['status'] == "0") {
		echo "
		<div class='row well' id='reply'>
			<div class='col-md-4'>Replying as ".$username."</div>
			<div class='col-md-8'>
			<form method='post' action='viewticket.php?id=".$id."' class='form' role='form'>
				<textarea class='form-control' rows='8' name='replymsg'></textarea><br>
				<input type='submit' name='send' value='Send Reply' class='btn btn-success'>
			</form>
			</div>
		</div>
		";
	}else{
		$dateclose = $zw->site->time2date($tr['time_closed']);
		echo "
		<div class='row well'>
			<div class='col-md-12'><B>Ticket has been closed since </B> ".$dateclose."</div>
		</div>
		";
	}
}else{
	echo $zw->site->displayalert("You are not the one who created this ticket. Please go back and select one of your own tickets.", "danger");
	echo "<br><a href='mytickets.php' class='btn btn-info'>Back to your ticket list</a>";
}
}else if ($user_uuid && $id == "") {
	echo $zw->site->displayalert("Id number is empty. Please go back and select a ticket to view", "danger");
	echo "<br><a href='mytickets.php' class='btn btn-info'>Back to your ticket list</a>";
}else if ($user_uuid == "" && $id != "") {
	echo $zw->site->displayalert("You MUST be logged in to view this ticket.", "danger");
}
include ('../inc/footer.php');
?>