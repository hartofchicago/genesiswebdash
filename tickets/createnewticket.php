<?php
$page_title = "Create a New Ticket";
define('ZW_IN_SYSTEM', true);
require_once('../inc/header.php');
if ($user_uuid) {
$usersname = $zw->grid->uuid2name($user_uuid);
$send = $zw->Security->make_safe($_POST['send']);
if ($send == "Create Ticket") {
	$subject = $zw->Security->make_safe($_POST['subject']);
	$msg = $zw->Security->make_safe($_POST['msg']);
	if ($subject != "" && $msg != "") {
		$now = time();
		$i = $zw->SQL->query("INSERT INTO `{$zw->config['db_prefix']}tickets` (user_uuid, subject, body, time_created) VALUES ('$user_uuid', '$subject', '$msg', '$now')");
		if ($i) {
			$tq = $zw->SQL->query("SELECT * FROM `{$zw->config['db_prefix']}tickets` WHERE subject = '$subject' AND user_uuid = '$user_uuid'");
			$tr = $zw->SQL->fetch_array($tq);
			$tid = $tr['id'];
			echo $zw->site->displayalert("Ticket created.<br>You are ticket #".$tid, "success");
			$users = array();
			$site_admin_level = $zw->config['site_admin_level'];
			$adminq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE UserLevel = '$site_admin_level'");
			while($adminr = $zw->SQL->fetch_array($adminq)) {
				$PrincipalID = $adminr['PrincipalID'];
				if ($zw->grid->online($PrincipalID)) {
					$users[] = $PrincipalID;
				}
			}
			if (!$users) {
				$sendto = array("HipChat");
			}else{
				$sendto = array("HipChat", "LSL");
			}
			//echo $zw->api->sendmsg("New ZMW support ticket from ".$usersname, $sendto, "yellow", $users);
		}else{
			echo $zw->site->displayalert("Unable to create ticket. Please try later.", "danger");
		}
	}
}else{
	if ($zw->config['GridName'] == "ZetaWorlds") {
		echo $zw->site->displayalert("For ZetaWorlds support please submit a ticket at <a href='http://support.zetamex.com'>http://support.zetamex.com</a>", "danger");
	}else{
echo "
<form method='post' action='' class='form' role='form'>
<div class='table-responsive'>
<table class='table table-hover table-striped'>
<tbody>
	<tr>
		<td><B>Making a ticket as:</B></td>
		<td>".$usersname."</td>
	</tr>
	<tr>
		<td><B>Subject</B></td>
		<td><input type='text' name='subject' value='' class='form-control' placeholder='Subject of ticket'></td>
	</tr>
	<tr>
		<td><B>Message</B></td>
		<td><textarea class='form-control' rows='8' name='msg'></textarea></td>
	</tr>
	<tr>
		<td></td>
		<td><input type='submit' name='send' value='Create Ticket' class='btn btn-success'></td>
	</tr>
</tbody>
</table>
</div>
</form>";
	} // ends if ($zw->config['GridName'] == "ZetaWorlds")
} // ends if if ($send == "Create Ticket")

}else{ // ends if ($user_uuid)
	echo $zw->site->displayalert("You are not logged in.", "danger");
}
include ('../inc/footer.php');
?>