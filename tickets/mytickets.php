<?php
$page_title = "My Tickets";
define('ZW_IN_SYSTEM', true);
require_once('../inc/header.php');
if ($user_uuid) {
$aviuuid = $zw->Security->make_safe($_POST['aviuuid']);
if ($zw->config['GridName'] == "ZetaWorlds") {
	echo $zw->site->displayalert("For ZetaWorlds support please submit a ticket at <a href='http://support.zetamex.com'>http://support.zetamex.com</a>", "danger");
}else{
	echo "<a href='createnewticket.php' class='btn btn-info'>Create a New Ticket</a>";
}
echo "
<div class='table-responsive'>
<table class='table table-hover table-striped'>
<thead>
<tr>
<th>#</th>
<th>Subject</th>
<th>Status</th>
<th>Date Created</th>
<th>Date Closed</th>
<th>Last Reply</th>
<th>Last Reply Date</th>
</tr>
</thead>
<tbody>
";
$tq = $zw->SQL->query("SELECT * FROM `{$zw->config['db_prefix']}tickets` WHERE user_uuid = '$user_uuid' ORDER BY `id` DESC LIMIT 0,100");
$tn = $zw->SQL->num_rows($tq);
if ($tn) {
	while ($tr = $zw->SQL->fetch_array($tq)) {
		$id = $tr['id'];
		$subject = $tr['subject'];
		$dateopen = $zw->site->time2date($tr['time_created']);
		if ($tr['status'] == "0") {
			$status = "<p class='text-success'>Open</p>";
			$dateclosed = "";
			$trcolor = "";
		}else if ($tr['status'] == "1") {
			$status = "<p class='text-danger'>Closed</p>";
			$dateclosed = $zw->site->time2date($tr['time_closed']);
			$trcolor = "danger";
		}
		$lastreply = $zw->tickets->lastreply($id);
		if ($lastreply != "0") {
			$replyuuid = $lastreply['user_uuid'];
			if ($replyuuid != $user_uuid) {
				$replystatus = "<p class='text-danger'>Admin Reply</p>";
				$repr = "admin";
			}else{
				$replystatus = "<p class='text-primary'>User Reply</p>";
				$repr = "user";
			}
			$lastreplydate = $zw->site->time2date($lastreply['time']);
		}else{
			$lastreplydate = "";
		}
		if ($trcolor == "") {
			if ($repr == "admin") {
				$trcolor = "warning";
			}else if ($repr == "user") {
				$trcolor = "info";
			}
		}
		echo "
		<tr class='".$trcolor."'>
			<td><B>".$id."</B></td>
			<td><a href='viewticket.php?id=".$id."'>".$subject."</a></td>
			<td>".$status."</td>
			<td>".$dateopen."</td>
			<td>".$dateclosed."</td>
			<td>".$replystatus."</td>
			<td>".$lastreplydate."</td>
		</tr>
		";
	}
}

echo "
</tbody>
</table>
</div>";
}else{
	echo $zw->site->displayalert("You are not logged in.", "danger");
}
include ('../inc/footer.php');
?>