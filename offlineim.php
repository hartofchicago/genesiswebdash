<?php
$page_title = "Offline Messages";
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');

$postmsgid = $zw->Security->make_safe($_POST['postmsgid']);
$delmsg = $zw->Security->make_safe($_POST['delmsg']);
$submit = $zw->Security->make_safe($_POST['submit']);

$totaloffset = "500"; // number of results to display
if ($user_uuid) {
	if ($postmsgid) {
		if ($delmsg == "Delete Message") {
			$delmsgq = $zw->SQL->query("DELETE FROM `{$zw->config['robust_db']}`.im_offline WHERE ID = '$postmsgid'");
			if ($delmsgq) {
				echo $zw->site->displayalert("Message deleted!", "success");
			}else{
				echo $zw->site->displayalert("Message was not deleted!", "danger");
			}
		}
	}
	if ($submit == "Send Message") {
		$aviuuid = $zw->Security->make_safe($_POST['aviuuid']);
		$postmsg = $zw->Security->make_safe($_POST['postmsg']);
		$postingmsg = "ZMW Message from ".$user.":\n".$postmsg;
		$data = "MSG=".$postingmsg."=".$aviuuid;
		$zw->lsl->send2server($data);
	}
?>
<h2>Offline Messages</h2>
<small>**Deleting a message will cause it to not pop up in world when you login**</small>
<div class="table-responsive">
<table class="table table-hover table-bordered table-striped">
<tbody>
<?php
$offlineimq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.im_offline WHERE PrincipalID = '$user_uuid' ORDER BY `ID` DESC LIMIT 0, $totaloffset");
$offlineimn = $zw->SQL->num_rows($offlineimq);
if ($offlineimn) {
	while($offlineimr = $zw->SQL->fetch_array($offlineimq)) {
		$msgid = $offlineimr['ID'];
		$XMLMessage = $offlineimr['Message'];
		$XMLParsed = new SimpleXMLElement($XMLMessage);
		$FromUUID = $XMLParsed->fromAgentID;
		$XMLMessaged = $XMLParsed->message;
		$FromName = $XMLParsed->fromAgentName; // uncomment if using the d4os offline IM module.
		if ($offlineimr['TMStamp']) {
			$TimeStamper = $offlineimr['TMStamp'];
		}else{
			$XMLTime = $XMLParsed->timestamp;
			$TimeStamper = $zw->site->time2date($XMLTime);
		}

		$FromGroup = $XMLParsed->fromGroup;
		if ($FromGroup == "true") {
			$FromGName = $zw->grid->groupid2name($FromUUID);
			$msgexploded = explode("|", $XMLMessaged);
			$xmlsubject = $msgexploded[0];
			$xmlmsg = $msgexploded[1];
			$msg = "<B>Group:</B> ".$FromGName."<br><B>Subject:</B> ".$xmlsubject."<br>".$xmlmsg;
			$sendmsgbutton = "";
			$sendmsgmodel = "";
		}else{
			$msg = $XMLMessaged;
			$sendmsgbutton = "<button class='btn btn-primary btn-sm' data-toggle='modal' data-target='#msgModal'>Send a message</button>";
$sendmsgmodel = "<div class='modal fade' id='msgModal' tabindex='-1' role='dialog' aria-labelledby='msgModalLabel' aria-hidden='true'>
  <div class='modal-dialog'>
    <div class='modal-content'>
      <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
        <h4 class='modal-title' id='msgModalLabel'>Send ".$FromName." a message</h4>
      </div>
      <div class='modal-body'>
      	<form method='post' action='' class='form' role='form'>
		<input type='hidden' name='aviuuid' value='".$FromUUID."'>
        <input type='text' name='postmsg' value='' class='form-control' placeholder='Send a message'>
		<input type='submit' name='submit' value='Send Message' class='btn btn-info btn-sm'>
		</form>
      </div>
      <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
      </div>
    </div>
  </div>
</div>";
		}
$msg = $zw->text->read_mycode($msg);
echo "
<tr><td>
<B>From:</B> ".$FromName."<br>
<B>Date:</B> ".$TimeStamper."<br>
  ".$msg."<br>
<form method='post' action='' class='form-horizontal' role='form'>
<input type='hidden' name='postmsgid' value='".$msgid."'>
<input type='submit' name='delmsg' value='Delete Message' class='btn btn-danger btn-sm'>
</form>
".$sendmsgbutton."
".$sendmsgmodel."
</td></tr>

";
	}
}else{
	echo "<tr><td>Sorry! No offline messages or group notices for you at this time</td></tr>";
}

echo "</tbody>
</table>
</div>
";
}else if (!$user_uuid) {
} // ends if ($user_uuid)

include('inc/footer.php');
?>