<?php
define('ZW_IN_SYSTEM', true);
require_once('../inc/headerless.php');

$t = $zw->Security->make_safe($_GET['t']);

if ($t == "fetch") {
	echo $zw->hipchat->fetchLSLsupportroomstatus();
}else if ($t == "supportrequest") {
	$UUID = $zw->Security->make_safe($_GET['uuid']);
	$sim = $zw->Security->make_safe($_GET['sim']);
	$touchername = $zw->grid->uuid2name($UUID);
	$msg = $touchername." has requested assistance from in world from ".$sim." region.";
	$zw->hipchat->sendmessage($msg, "yellow", false, "text");
	echo $UUID."=Someone will assist you as soon as humanly possible.";
}


include ('inc/footer.php');
?>