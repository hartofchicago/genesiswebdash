<?php
$page_title = "Events Manager";
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');

if ($user_uuid) {

function process_region_type_flags($flags)
{
    if ($flags == "0")
        $terms = "PG";
    if ($flags == "1")
        $terms = "Mature";
    if ($flags == "2")
        $terms = "Adult";
    return $terms;
}

if ($zw->Security->make_safe($_POST['save']) == "Save New Event") {
	$name = $zw->Security->make_safe($_POST['name']);
	$desc = $zw->Security->make_safe($_POST['desc']);
	$position = $zw->Security->make_safe($_POST['sim']);
	$category = $zw->Security->make_safe($_POST['category']);
	$maturity = $zw->Security->make_safe($_POST['maturity']);
	$date = $zw->Security->make_safe($_POST['date']);
	$duration = $zw->Security->make_safe($_POST['duration']);
	$coveramount = $zw->Security->make_safe($_POST['coveramount']);
	$sim = $zw->Security->make_safe($_POST['sim']);
	$location = $zw->Security->make_safe($_POST['location']);
	if (!$coveramount || $coveramount == "0") {
		$covercharge = "0";
		$coveramount = "0";
	}else{
		$covercharge = "1";
		$coveramount = $coveramount;
	}
	$LocExplode = explode(",", $location);
	$ParcelX = $LocExplode[0];
	$ParcelY = $LocExplode[1];
	$ParcelZ = $LocExplode[2];
	$GlobalPos = "<".$ParcelX.",".$ParcelY.",".$ParcelZ.">";
	$dateastime = strtotime($date);
	$saveq = $zw->SQL->query("INSERT INTO `{$zw->config['search_db']}`.events (owneruuid, name, creatoruuid, category, description, dateUTC, duration, covercharge, coveramount, simname, globalPos, eventflags) VALUES ('$user_uuid', '$name', '$user_uuid', '$category', '$desc', '$dateastime', '$duration', '$covercharge', '$coveramount', '$sim', '$GlobalPos', '$maturity')");
	if ($saveq) {
		echo "New event created";
	}else{
		echo "Unable to save new event";
	}
}else if ($zw->Security->make_safe($_POST['edit']) == "Edit Event") {
	$eventid = $zw->Security->make_safe($_POST['eventid']);
	$name = $zw->Security->make_safe($_POST['name']);
	$desc = $zw->Security->make_safe($_POST['desc']);
	$position = $zw->Security->make_safe($_POST['sim']);
	$category = $zw->Security->make_safe($_POST['category']);
	$maturity = $zw->Security->make_safe($_POST['maturity']);
	$date = $zw->Security->make_safe($_POST['date']);
	$duration = $zw->Security->make_safe($_POST['duration']);
	$coveramount = $zw->Security->make_safe($_POST['coveramount']);
	$sim = $zw->Security->make_safe($_POST['sim']);
	$location = $zw->Security->make_safe($_POST['location']);
	if (!$coveramount || $coveramount == "0") {
		$covercharge = "0";
		$coveramount = "0";
	}else{
		$covercharge = "1";
		$coveramount = $coveramount;
	}
	$LocExplode = explode(",", $location);
	$ParcelX = $LocExplode[0];
	$ParcelY = $LocExplode[1];
	$ParcelZ = $LocExplode[2];
	$GlobalPos = "<".$ParcelX.",".$ParcelY.",".$ParcelZ.">";
	$dateastime = strtotime($date);
	$editq = $zw->SQL->query("UPDATE `{$zw->config['search_db']}`.events SET owneruuid = '$user_uuid', name = '$name', creatoruuid = '$user_uuid', category = '$category', description = '$desc', dateUTC = '$dateastime', duration = '$duration', covercharge = '$covercharge', coveramount = '$coveramount', simname = '$sim', globalPos = '$GlobalPos', eventflags = '$maturity' WHERE eventid = '$eventid'");
	if ($editq) {
		echo "Event updated";
	}else{
		echo "Unable to update event";
	}
}else if ($zw->Security->make_safe($_POST['deleteevent']) == "Delete") {
	$eventid = $zw->Security->make_safe($_POST['eventid']);
	$deleteevent = $zw->SQL->query("DELETE FROM `{$zw->config['search_db']}`.events WHERE eventid = '$eventid'");
	if ($deleteevent) {
		echo "Event deleted";
	}else{
		echo "Unable to delete event";
	}
}else{
}
?>
	<div class='table-responsive'>
		<table class='table table-striped table-bordered table-hover'>
			<thead>
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Category</th>
					<th>Price</th>
					<th>Region</th>
					<th>Date</th>
					<th>Duration</th>
					<th>Maturity</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
<?php
	$result = $zw->SQL->query("SELECT * FROM `{$zw->config['search_db']}`.events WHERE owneruuid = '$user_uuid' ORDER BY `dateUTC` DESC LIMIT 0, 25");
	$num = $zw->SQL->num_rows($result);
	if ($num) {
		while ($row = $zw->SQL->fetch_array($result))
		{
			$eventid = $row['eventid'];
			$name = $row['name'];
			$desc = $row['description'];
			$dateUTC = $row['dateUTC'];
			$duration = $row['duration'];
			$simname = $row['simname'];
			$globalPos = $row['globalPos'];
			$eventflags = $row['eventflags'];
			$covercharge = $row['covercharge'];
			$coveramount = $row['coveramount'];

			$flags = process_region_type_flags($eventflags);

			if ($covercharge == "0") {
				$price = "FREE";
			}else if ($covercharge == "1") {
				$price = $zw->config['GridMoney'].$coveramount;
			}

			$desc = substr($desc, 0, 100);
			$desc = htmlspecialchars($desc, ENT_QUOTES);

			$date = date("M d Y g:ia T",$dateUTC);
			$length = $duration;

			if ($row['category'] == 0)     $category = "Any";
			if ($row['category'] == 18)    $category = "Discussion";
	        if ($row['category'] == 19)    $category = "Sports";
	        if ($row['category'] == 20)    $category = "Live Music";
	        if ($row['category'] == 22)    $category = "Commercial";
	        if ($row['category'] == 23)    $category = "Nightlife/Entertainment";
	        if ($row['category'] == 24)    $category = "Games/Contests";
	        if ($row['category'] == 25)    $category = "Pageants";
	        if ($row['category'] == 26)    $category = "Education";
	        if ($row['category'] == 27)    $category = "Arts and Culture";
	        if ($row['category'] == 28)    $category = "Charity/Support Groups";
	        if ($row['category'] == 29)    $category = "Miscellaneous";

	        if ($duration == "10") $dura = "10 Minutes";
	        if ($duration == "20") $dura = "20 Minutes";
	        if ($duration == "25") $dura = "25 Minutes";
	        if ($duration == "30") $dura = "30 Minutes";
	        if ($duration == "45") $dura = "45 Minutes";
	        if ($duration == "60") $dura = "1 Hour";
	        if ($duration == "90") $dura = "1.5 Hours";
	        if ($duration == "120") $dura = "2 Hours";
	        if ($duration == "150") $dura = "2.5 Hours";
	        if ($duration == "180") $dura = "3 Hours";
	        if ($duration == "1440") $dura = "All Day";

			echo "
					<tr>
						<td>".$name."</td>
						<td>".$desc."</td>
						<td>".$category."</td>
						<td>".$zw->config['GridMoney']." ".$price."</td>
						<td>".$simname."</td>
						<td>".$date."</td>
						<td>".$dura."</td>
						<td>".$flags."</td>
						<td>
							<form method='post' action='' class='form-inline' role='form'>
								<input type='hidden' name='eventid' value='".$eventid."'>
								<input type='submit' name='editevent' value='Edit' class='btn btn-primary btn-sm'>
								<input type='submit' name='deleteevent' value='Delete' class='btn btn-danger btn-sm'>
							</form>
						</td>
					</tr>
			";
		}
	}else{
		echo "
					<tr>
						<td></td>
						<td>You have not yet created any events</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
			";
	}
?>
			</tbody>
		</table>
	</div>
<form method='post' action='' class="form-horizontal" role="form">
<?php
if ($zw->Security->make_safe($_POST['editevent']) == "Edit") {
	$eventid = $zw->Security->make_safe($_POST['eventid']);
	$editq = $zw->SQL->query("SELECT * FROM `{$zw->config['search_db']}`.events WHERE owneruuid = '$user_uuid' AND eventid = '$eventid'");
	$row2 = $zw->SQL->fetch_array($editq);
	$name = $row2['name'];
	$desc = $row2['description'];
	$dateUTC = $row2['dateUTC'];
	$duration = $row2['duration'];
	$category = $row2['category'];
	$coveramount = $row2['coveramount'];
	$simname = $row2['simname'];
	$globalPos = $row2['globalPos'];
	$eventflags = $row2['eventflags'];
	$editq->close();
	echo "<input type='hidden' name='eventid' value='".$eventid."'>";
	$buttons = "<input type='submit' name='edit' value='Edit Event' class='btn btn-success'>";
	$event_day = date('j', $dateUTC);
	$event_month = date('n', $dateUTC);
	$event_year = date('Y', $dateUTC);
	$time_hour = date('G', $dateUTC);
	$time_minutes = date('s', $dateUTC);

	$globalPos = str_replace("<", "", $globalPos);
	$globalPos = str_replace(">", "", $globalPos);
}else{
	$name = "";
	$desc = "";
	$duration = "";
	$category = "0";
	$coveramount = "0";
	$simname = "";
	$globalPos = "";
	$eventflags = "";
	$buttons = "<input type='submit' name='save' value='Save New Event' class='btn btn-success'>";
	$event_day = date('j');
	$event_month = date('n');
	$event_year = date('Y');
	$time_hour = date('G');
	$time_minutes = date('s');
}
?>
  <div class="form-group">
    <label for="inputName" class="col-sm-2 control-label">Name</label>
    <div class="col-sm-10">
      <input type="text" name="name" value="<?php echo $name; ?>" class="form-control" id="inputName" placeholder="Name">
    </div>
  </div>
  <div class="form-group">
    <label for="inputDesc" class="col-sm-2 control-label">Description</label>
    <div class="col-sm-10">
      <textarea name="desc" class="form-control" rows="5" id="inputDesc" placeholder="Description"><?php echo $desc; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <label for="inputSim" class="col-sm-2 control-label">Region and Position</label>
    <div class="col-sm-10">
    	<input type='text' name='sim' value='<?php echo $simname; ?>' id="inputSim" class='form-control' placeholder='Region Name'>
    	<input type='text' name='location' value='<?php echo $globalPos; ?>' id="inputSim" class='form-control' placeholder='128,128,25'>
    </div>
  </div>
  <div class="form-group">
    <label for="inputCategory" class="col-sm-2 control-label">Category</label>
    <div class="col-sm-10">
      <select name="category" class="form-control" id="inputCategory">
      	<option value="0" <?php if ($category == "0")echo "SELECTED"; ?>>Any</option>
      	<option value="18" <?php if ($category == "18")echo "SELECTED"; ?>>Discussion</option>
      	<option value="19" <?php if ($category == "19")echo "SELECTED"; ?>>Sports</option>
      	<option value="20" <?php if ($category == "20")echo "SELECTED"; ?>>Live Music</option>
      	<option value="22" <?php if ($category == "22")echo "SELECTED"; ?>>Commercial</option>
      	<option value="23" <?php if ($category == "23")echo "SELECTED"; ?>>Nightlife/Entertainment</option>
      	<option value="24" <?php if ($category == "24")echo "SELECTED"; ?>>Games/Contests</option>
      	<option value="25" <?php if ($category == "25")echo "SELECTED"; ?>>Pageants</option>
      	<option value="26" <?php if ($category == "26")echo "SELECTED"; ?>>Education</option>
      	<option value="27" <?php if ($category == "27")echo "SELECTED"; ?>>Arts and Culture</option>
      	<option value="28" <?php if ($category == "28")echo "SELECTED"; ?>>Charity/Support Groups</option>
      	<option value="29" <?php if ($category == "29")echo "SELECTED"; ?>>Miscellaneous</option>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="inputMaturity" class="col-sm-2 control-label">Maturity</label>
    <div class="col-sm-10">
      <select name="maturity" class="form-control" id="inputMaturity">
      	<option value="0" <?php if ($eventflags == "0")echo "SELECTED"; ?>>PG</option>
      	<option value="1" <?php if ($eventflags == "1")echo "SELECTED"; ?>>Mature</option>
      	<option value="2" <?php if ($eventflags == "2")echo "SELECTED"; ?>>Adult</option>
      </select>
    </div>
  </div>
  <div class="form-group">
  	<label for="inputDate" class="col-sm-2 control-label">Date and Time</label>
    <div class="col-sm-10">
    	<?php $daterr = $event_year."-".$event_month."-".$event_day." ".$time_hour.":".$time_minutes; ?>
	  	<input type="text" id="inputDate" name="date" value="<?php echo $daterr; ?>" class="form-control form_datetime">
		<script type="text/javascript">
		    $(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});
		</script>    
	  </div>
  	</div>
  <div class="form-group">
    <label for="inputDuration" class="col-sm-2 control-label">Duration</label>
    <div class="col-sm-10">
      <select name="duration" class="form-control" id="inputDuration">
      	<option value="10" <?php if ($duration == "10")echo "SELECTED"; ?>>10 Minutes</option>
      	<option value="15" <?php if ($duration == "15")echo "SELECTED"; ?>>15 Minutes</option>
      	<option value="20" <?php if ($duration == "20")echo "SELECTED"; ?>>20 Minutes</option>
      	<option value="25" <?php if ($duration == "25")echo "SELECTED"; ?>>25 Minutes</option>
      	<option value="30" <?php if ($duration == "30")echo "SELECTED"; ?>>30 Minutes</option>
      	<option value="45" <?php if ($duration == "45")echo "SELECTED"; ?>>45 Minutes</option>
      	<option value="60" <?php if ($duration == "60")echo "SELECTED"; ?>>1 Hour</option>
      	<option value="90" <?php if ($duration == "90")echo "SELECTED"; ?>>1.5 Hours</option>
      	<option value="120" <?php if ($duration == "120")echo "SELECTED"; ?>>2 Hours</option>
      	<option value="150" <?php if ($duration == "150")echo "SELECTED"; ?>>2.5 Hours</option>
      	<option value="180" <?php if ($duration == "180")echo "SELECTED"; ?>>3 Hours</option>
      	<option value="1440" <?php if ($duration == "1440")echo "SELECTED"; ?>>All day</option>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="inputCover" class="col-sm-2 control-label">Cover Charge</label>
    <div class="col-sm-10">
    	<div class='input-group'>
			<span class='input-group-addon'><?php echo $zw->config['GridMoney']; ?></span>
      		<input type="number" name="coveramount" value="<?php echo $coveramount; ?>" class="form-control" id="inputCover">
      	</div>
      <br><small>Leave it to 0 if event is free</small>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <?php echo $buttons; ?>
    </div>
  </div>
</form>
<?php
} // if ($user_uuid)
include ('inc/footer.php');
?>