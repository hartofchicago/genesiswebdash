<?php
define('ZW_IN_SYSTEM', true);
require_once('inc/headerless.php');

$t = $zw->Security->make_safe($_GET['t']);
$u = $zw->Security->make_safe($_GET['u']);

if ($t == "usercounts") {
$onlineq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.GridUser WHERE Online = 'TRUE'");
$online = $zw->SQL->num_rows($onlineq);

$totalq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts");
$totalc = $zw->SQL->num_rows($totalq);

$monthago = $now - 2592000;
$latestq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.GridUser WHERE Login > '$monthago'");
$latestc = $zw->SQL->num_rows($latestq);

$regionq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.regions");
$regionc = $zw->SQL->num_rows($regionq);
echo $online."~".$totalc."~".$latestc."~".$regionc;
}

if ($t == "destinations") {
	$destecho = array("");
	$destq = $zw->SQL->query("SELECT * FROM `{$zw->config['search_db']}`.popularplaces ORDER BY `name` ASC LIMIT 0,10");
	while ($destr = $zw->SQL->fetch_array($destq)) {
		$destname = $destr['name'];
		$destecho .= $destname."~";
	}
	echo $destecho;
}

if ($t == "profile") {
	$userexplode = explode(" ", $u);
	$first = $userexplode[0];
	$last = $userexplode[1];
	if (!$last) {
		$last = "Resident";
	}
	$uq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE FirstName = '$first' AND LastName = '$last'");
	$ur = $zw->SQL->fetch_array($uq);
	$uuid = $ur['PrincipalID'];
	$profq = $zw->SQL->query("SELECT * FROM `{$zw->config['profile_db']}`.userprofile WHERE useruuid = '$uuid'");
	$r = $zw->SQL->fetch_array($profq);

	$partner = $r['profilePartner'];
	$url = $r['profileURL'];
	$want2mask = $r['profileWantToMask'];
	$want2text = $r['profileWantToText'];
	$skillsmask = $r['profileSkillsMask'];
	$skillstext = $r['profileSkillsText'];
	$lang = $r['profileLanguages'];
	$flpic = $r['profileImage'];
	$fakeaboutme = $r['profileAboutText'];
	$rlpic = $r['profileFirstImage'];
	$realaboutme = $r['profileFirstText'];
	$uname = $first." ".$last;
	if (!$partner || $partner == "00000000-0000-0000-0000-000000000000") {
		$partner = "No one";
	}
	echo $uuid."~".$flpic."~".$fakeaboutme."~".$rlpic."~".$realaboutme."~".$partner."~".$url."~".$uname;
}

// For examples for this API system please see zwapilsl.lsl included with zw
?>