<?php
$page_title = "ELetters";
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');

$subject = $zw->Security->make_safe($_GET['subject']);
if ($subject) {
  $where = "WHERE subject = '$subject'";
}else{
  $where = "ORDER BY `id` ASC LIMIT 0,5";
}
$q = $zw->SQL->query("SELECT * FROM `{$zw->config['db_prefix']}newsletter` $where");
$n = $zw->SQL->num_rows($q);
if ($n) {
  while ($r = $zw->SQL->fetch_array($q)) {
    $who = $zw->grid->uuid2name($r['useruuid']);
    $qsubject = $r['subject'];
    $message = $r['message'];
    $date = $zw->site->time2date($r['time']);
  echo "
  <div class='row'>
    <div class='col-md-6'>
      <div class='well'>
        <h2>".$qsubject."<br><small>Posted by ".$who." on ".$date."</small></h2>
        ".$message."
      </div>
    </div>
  </div>
  ";
  }
}else{
  echo "
  <div class='row'>
    <div class='col-md-6'>
      <div class='well'>
        No eLetters yet from this grid.
      </div>
    </div>
  </div>
  ";
}

include ('inc/footer.php');
?>