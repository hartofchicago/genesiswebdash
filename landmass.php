<?php
$page_title = "Land Mass";
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');

$api = $zw->Security->make_safe($_GET['api']);

$defsize = "256";
$defsqm = "65536";

$sizextotal = "";
$sizeytotal = "";
$sizetotal = "";
$totalsingleregions = "";
$sizeq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.regions");
$totalregions = $zw->SQL->num_rows($sizeq);
while($sizer = $zw->SQL->fetch_array($sizeq)) {
	$x = $sizer['sizeX'];
	$y = $sizer['sizeY'];
	$sizextotal += $x;
	$sizeytotal += $y;
	$xytotal = $x * $y;
	$sizetotal += $xytotal;
	if ($xytotal >= $defsqm) {
		$totalsingleregions += $xytotal / $defsqm;
	}else if($xytotal == $defsqm) {
		++$totalsingleregions;
	}
}

$region256countq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.regions WHERE sizeX = '$defsize' AND sizeY = '$defsize'");
$region256count = $zw->SQL->num_rows($region256countq);

$regionvarcountq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.regions WHERE sizeX > '$defsize' AND sizeY > '$defsize'");
$regionvarcount = $zw->SQL->num_rows($regionvarcountq);

if ($api) {
	if ($api == "totalsingleregions") {
		echo $totalsingleregions;
	}
	if ($api == "totalregions") {
		echo $totalregions;
	}
	if ($api == "region256count") {
		echo $region256count;
	}
	if ($api == "regionvarcount") {
		echo $regionvarcount;
	}
	if ($api == "sizetotal") {
		echo $sizetotal;
	}
	$apiarray = array('TotalSingleRegions' => $totalsingleregions, 'TotalRegions' => $totalregions, 'Total256Regions' => $region256count, 'TotalVarRegions' => $regionvarcount, 'LandMass' => $sizetotal);
	if ($api == "json") {
		echo json_encode($apiarray);
	}
	if ($api == "xml") {
		echo xmlrpc_encode($apiarray);
	}
	if ($api == "lsl") {
		echo number_format($totalregions)."=".number_format($totalsingleregions)."=".number_format($region256count)."=".number_format($regionvarcount)."=".number_format($sizetotal);
	}
}else{
	$totalsingleregionswording = "<a href='#' id='tooltip' data-toggle='tooltip' title='Total region count, counting total land in terms of regular size regions.'>(".number_format($totalsingleregions).")</a>";
	echo "
	<p>
	<h2>Land Mass Count</h2>
	<!-- <B>(WIP, WORK IN PROGESS MEANING NUMBERS MAY NOT BE CORRECT)</B><br> -->
	This grid has <B>".number_format($totalregions)."</B> regions ".$totalsingleregionswording.".<br>
	There are <B>".number_format($region256count)."</B> regular size regions.<br>
	There are <B>".number_format($regionvarcount)."</B> var regions.<br>
	Total land mass: <B>".number_format($sizetotal)."</B> square meters.<br>
	</p>
	";
}

include ('inc/footer.php');
?>