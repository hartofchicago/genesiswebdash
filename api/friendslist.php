<?php
define('ZW_IN_SYSTEM', true);
require_once('../inc/headerless.php');

$api = $zw->Security->make_safe($_GET['api']);
$token = $zw->Security->make_safe($_GET['token']);
$istokenvalid = $zw->api->istokenvalid($token);

if ($api && $istokenvalid) {
	$uid = $zw->api->token2user($token);
	$approvalq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.Friends WHERE PrincipalID = '$uid'");
	$approvaln = $zw->SQL->num_rows($approvalq);
	$fapiarray = "";
	$lslapiarray1 = "";
	if ($approvaln) {
		while($approvalr = $zw->SQL->fetch_array($approvalq)) {
			$PrincipalID = $approvalr['Friend'];
			$Requestername = $zw->grid->uuid2name($PrincipalID);
			$Online = $zw->grid->online($PrincipalID);
			if ($Online == 1) {
				$isOnline = "true";
			}else{
				$isOnline = "false";
			}
			$fapiarray[] = array('FriendName' => $Requestername, 'isOnline' => $isOnline);
			$lslapiarray1 .= $Requestername."=".$isOnline."|";
		}
	}else{
		$fapiarray = array('FriendList' => 'empty');
		$lslapiarray1 = "FriendList=Empty|";
	}
	$apiarray = array('FriendsList' => $fapiarray);
	if ($api == "Friend") {
		echo $Requestername;
	}
	if ($api == "OnlineStatus") {
		echo $Online;
	}
	if ($api == "json") {
		echo json_encode($apiarray);
	}
	if ($api == "xml") {
		echo xmlrpc_encode($apiarray);
	}
	if ($api == "lsl") {
		$lslapiarray1 .= "~~";
		$lslapiarray = str_replace("|~~", "", $lslapiarray1);
		echo $lslapiarray;
	}
}else{
	if ($istokenvalid == 0) {
		$tokenstatus = "invalid";
	}else if ($istokenvalid == 1) {
		$tokenstatus = "valid";
	}
	$apiarray = array('ERROR' => 'One or more feilds are required', 'Token Status' => $tokenstatus);
	echo json_encode($apiarray);
}
?>