<?php
$page_title = "User Count";
define('ZW_IN_SYSTEM', true);
require_once('../inc/header.php');

$api = $zw->Security->make_safe($_GET['api']);

$onlineq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.Presence WHERE RegionID != '{$zw->nullkey}'");
$online = $zw->SQL->num_rows($onlineq);
while ($onliner = $zw->SQL->fetch_array($onlineq)) {
	$onlineuser = $onliner['UserID'];
	$usercheckq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$onlineuser'");
	$usercheckn = $zw->SQL->num_rows($usercheckq);
	if ($usercheckn) {
		$thisgrid++;
	}else{
		$hgvisiters++;
	}
}

$mostcountcheckq = $zw->SQL->query("SELECT * FROM `{$zw->config['db_prefix']}mostusers` ORDER BY `count` DESC LIMIT 0,1");
$mostcountcheckr = $zw->SQL->fetch_array($mostcountcheckq);
$lastcount = $mostcountcheckr['count'];
$lastcounttime = $mostcountcheckr['time'];
$lastcountdate = $zw->site->time2date($lastcounttime);
if (strlen($online) == 1) {
	$stronline = "0".$online;
}else{
	$stronline = $online;
}
if ($lastcount == $stronline) {
	$zw->SQL->query("UPDATE `{$zw->config['db_prefix']}mostusers` SET time = '$now' WHERE count = '$stronline'");
}else if ($lastcount <= $online) {
	$zw->SQL->query("INSERT INTO `{$zw->config['db_prefix']}mostusers` (count, time) VALUES ('$stronline', '$now')");
}

$totalq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts");
$totaluser = $zw->SQL->num_rows($totalq);

$monthago = $now - 2592000;
$latestq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.GridUser WHERE Login > '$monthago' AND Logout > '$monthago'");
$latest = 0;
while ($latestr = $zw->SQL->fetch_array($latestq)) {
	$checkuuid = $latestr['UserID'];
	$checkuserq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$checkuuid'");
	$checkusern = $zw->SQL->num_rows($checkuserq);
	if ($checkusern) {
		$latest++;
	}
}

if ($api) {
	if ($api == "online") {
		echo $online;
	}
	if ($api == "hgvisiters") {
		echo $hgvisiters;
	}
	if ($api == "totalusers") {
		echo $totaluser;
	}
	if ($api == "moston") {
		echo $lastcount." on ".$lastcountdate;
	}
	$apiarray = array('Online' => $online, 'HGVisitors' => $hgvisiters, 'TotalUsers' => $totaluser, 'Most On' => $lastcount." on ".$lastcountdate);
	if ($api == "json") {
		echo json_encode($apiarray);
	}
	if ($api == "xml") {
		echo xmlrpc_encode($apiarray);
	}
	if ($api == "lsl") {
		echo number_format($online)."=".number_format($hgvisiters)."=".number_format($totaluser)."=".number_format($lastcount)."\n".$lastcountdate;
	}
}else{
	echo "
	<p>
	<h2>User Count</h2>
	<!-- <B>(WIP, WORK IN PROGESS MEANING NUMBERS MAY NOT BE CORRECT)</B><br> -->
	This grid has <B>".number_format($online)."</B> users online.<br>
	There are <B>".number_format($hgvisiters)."</B> HG Visitors.<br>
	There are <B>".number_format($totaluser)."</B> registered users.<br>
	There are <B>".number_format($latest)."</B> active past 30 days.<br>
	Most users on was <B>".number_format($lastcount)."</B> on ".$lastcountdate.".<br>
	</p>
	";
}

include ('../inc/footer.php');
?>