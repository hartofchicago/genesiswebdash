<?php
$page_title = "User Count";
define('ZW_IN_SYSTEM', true);
require_once('../inc/header.php');

$api = $zw->Security->make_safe($_GET['api']);

// checks to see if the login server aka Robust.exe is online
$isgridonline = $zw->grid->gridonline();
if ($isgridonline) {
	$gridonline = "Online";
}else{
	$gridonline = "Offline";
}

/* Start User Count */
$onlineq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.Presence WHERE RegionID != '{$zw->nullkey}'");
$online = $zw->SQL->num_rows($onlineq);
while ($onliner = $zw->SQL->fetch_array($onlineq)) {
	$onlineuser = $onliner['UserID'];
	$usercheckq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$onlineuser'");
	$usercheckn = $zw->SQL->num_rows($usercheckq);
	if ($usercheckn) {
		$thisgrid++;
	}else{
		$hgvisiters++;
	}
}

$mostcountcheckq = $zw->SQL->query("SELECT * FROM `{$zw->config['db_prefix']}mostusers` ORDER BY `count` DESC LIMIT 0,1");
$mostcountcheckr = $zw->SQL->fetch_array($mostcountcheckq);
$lastcount = $mostcountcheckr['count'];
$lastcounttime = $mostcountcheckr['time'];
$lastcountdate = $zw->site->time2date($lastcounttime);
if (strlen($online) == 1) {
	$stronline = "0".$online;
}else{
	$stronline = $online;
}
if ($lastcount == $stronline) {
	$zw->SQL->query("UPDATE `{$zw->config['db_prefix']}mostusers` SET time = '$now' WHERE count = '$stronline'");
}else if ($lastcount <= $online) {
	$zw->SQL->query("INSERT INTO `{$zw->config['db_prefix']}mostusers` (count, time) VALUES ('$stronline', '$now')");
}

$totalq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts");
$totaluser = $zw->SQL->num_rows($totalq);

$monthago = $now - 2592000;
$latestq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.GridUser WHERE Login > '$monthago' AND Login != '0'");
$latestlocal = 0;
$latesthg = 0;
while ($latestr = $zw->SQL->fetch_array($latestq)) {
	$checkuuid = $latestr['UserID'];
	$checkuserq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$checkuuid'");
	$checkusern = $zw->SQL->num_rows($checkuserq);
	if ($checkusern) {
		$latestlocal++;
	}else{
		$latesthg++;
	}
}
$hoursago = $now - 86400;
$latest24q = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.GridUser WHERE Login > '$hoursago' AND Login != '0'");
$latest24c = 0;
while ($latest24r = $zw->SQL->fetch_array($latest24q)) {
	$check24uuid = $latest24r['UserID'];
	$checkuser24q = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.UserAccounts WHERE PrincipalID = '$check24uuid'");
	$checkuser24n = $zw->SQL->num_rows($checkuser24q);
	if ($checkuser24n) {
		$latest24c++;
	}
}
/* End User Count */
/* Start Region count */
$defsize = "256";
$defsqm = "65536";

$sizextotal = "";
$sizeytotal = "";
$sizetotal = "";
$totalsingleregions = "";
$sizeq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.regions");
$totalregions = $zw->SQL->num_rows($sizeq);
while($sizer = $zw->SQL->fetch_array($sizeq)) {
	$x = $sizer['sizeX'];
	$y = $sizer['sizeY'];
	$sizextotal += $x;
	$sizeytotal += $y;
	$xytotal = $x * $y;
	$sizetotal += $xytotal;
	if ($xytotal >= $defsqm) {
		$totalsingleregions += $xytotal / $defsqm;
	}else if($xytotal == $defsqm) {
		++$totalsingleregions;
	}
}

$region256countq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.regions WHERE sizeX = '$defsize' AND sizeY = '$defsize'");
$region256count = $zw->SQL->num_rows($region256countq);

$regionvarcountq = $zw->SQL->query("SELECT * FROM `{$zw->config['robust_db']}`.regions WHERE sizeX > '$defsize' AND sizeY > '$defsize'");
$regionvarcount = $zw->SQL->num_rows($regionvarcountq);
/* End Region count */
$html = "<h2>Grid Status</h2>
The grid is <B>".$gridonline."</B><br>
<p>
	<B>User Status Count</B><br>
	This grid has <B>".number_format($online)."</B> users online.<br>
	There are <B>".number_format($hgvisiters)."</B> HG Visitors.<br>
	There are <B>".number_format($totaluser)."</B> registered users.<br>
	There are <B>".number_format($latest24c)."</B> active past 24 hours.<br>
	There are <B>".number_format($latestlocal)."</B> active past 30 days.<br>
	There are <B>".number_format($latesthg)."</B> active HG Visits past 30 days.<br>
	Most users on was <B>".number_format($lastcount)."</B> on ".$lastcountdate.".<br>
</p>
<p>
	<B>Region Status Count</B><br>
	There are <B>".number_format($totalregions)."</B> regions.<br>
	There are <B>".number_format($totalsingleregions)."</B> legacy regions.<br>
	<small>Legacy Regions numbers are counted as if ALL regions including var's were just single 256 by 256 meter regions.</small><br>
	There are <B>".number_format($region256count)."</B> 256 regions.<br>
	There are <B>".number_format($regionvarcount)."</B> varregions.<br>
	Total of <B>".number_format($sizetotal)."</B> square meters of land.<br>
</p>";
if ($api) {
	if ($api == "gridstate") {
		echo $gridonline;
	}
	if ($api == "online") {
		echo $online;
	}
	if ($api == "hgvisiters") {
		echo $hgvisiters;
	}
	if ($api == "totalusers") {
		echo $totaluser;
	}
	if ($api == "active24") {
		echo $latest24c;
	}
	if ($api == "active30") {
		echo $latestlocal;
	}
	if ($api == "hgactive30") {
		echo $latesthg;
	}
	if ($api == "moston") {
		echo $lastcount." on ".$lastcountdate;
	}
	if ($api == "regions") {
		echo $totalregions;
	}
	if ($api == "legacyregions") {
		echo $totalsingleregions;
	}
	if ($api == "256regions") {
		echo $region256count;
	}
	if ($api == "varregions") {
		echo $regionvarcount;
	}
	if ($api == "totalsize") {
		echo $sizetotal;
	}
	$apiarray = array('GridState' => $gridonline, 
		'Online' => $online, 
		'HGVisitors' => $hgvisiters, 
		'TotalUsers' => $totaluser, 
		'Active24' => $latest24c, 
		'Active30' => $latestlocal,
		'HGActive30' => $latesthg,
		'Most On' => $lastcount." on ".$lastcountdate,
		'Regions' => $totalregions,
		'LegacyRegions' => $totalsingleregions,
		'256Regions' => $region256count,
		'VarRegions' => $regionvarcount, 
		'TotalSize' => $sizetotal);
	if ($api == "json") {
		echo json_encode($apiarray);
	}
	if ($api == "xml") {
		echo xmlrpc_encode($apiarray);
	}
	if ($api == "lsl") {
		echo $gridonline."=".number_format($online)."=".number_format($hgvisiters)."=".number_format($totaluser)."=".number_format($latest24c)."=".number_format($latestlocal)."=".number_format($latesthg)."=".number_format($lastcount)."\n".$lastcountdate."=".number_format($totalregions)."=".number_format($totalsingleregions)."=".number_format($region256count)."=".number_format($regionvarcount)."=".number_format($sizetotal);
	}
	if ($api == "html") {
		echo $html;
	}
}else{
	echo $html;
	if ($zw->grid->isAdmin($user_uuid)) {
?>
	<p>
		You may retreive these numbers in other formats by putting a ?api= in the address bar next to the page name followed by the type of format you want.<br>
		Examples:<br>
		gridstatus.php?api=json will display these numbers in json format.<br>
		gridstatus.php?api=xml will display these numbers in xml format.<br>
		gridstatus.php?api=lsl will display these numbers in lsl friendly format.<br>
		Or can retreive individual status from the following simple list<br>
		gridstatus.php?api=gridstate<br>
		gridstatus.php?api=online<br>
		gridstatus.php?api=hgvisiters<br>
		gridstatus.php?api=totalusers<br>
		gridstatus.php?api=active24<br>
		gridstatus.php?api=active30<br>
		gridstatus.php?api=hgactive30<br>
		gridstatus.php?api=moston<br>
		gridstatus.php?api=regions<br>
		gridstatus.php?api=legacyregions<br>
		gridstatus.php?api=256regions<br>
		gridstatus.php?api=varregions<br>
		gridstatus.php?api=totalsize
	</p>
	<p>
		<B>LSL Breakdown</B><br>
		Is the login online=Users Currently Online=HG Visitors=Total users=Past 30 days active by local residents=Past 30 days active by HG Visitors=Most On followed by the day=Total Regions=Total Legacy Regions=Total Single Regions=Region Var Count=Total size in square meters<br>
		Fetching and parsing this with a in world script is super easy, here is a example<br>
		<textarea class="form-control" rows="10">string url = "<?php echo $site_address; ?>/api/gridstatus.php?api=lsl";
key httper;
default
{
    on_rez(integer s) {
        llResetScript();
    }
    state_entry() {
        httper = llHTTPRequest(url, [HTTP_METHOD, "GET"], "");
        llSetTimerEvent(300.0); // refreshes every 5 minutes to reduce lag. 5 minutes = 300.0 sec
    }
    http_response(key request_id, integer status, list metadata, string body) {
        if (request_id == httper) {
            list apilist = llParseString2List(llUnescapeURL(body), ["="], []);
            string gridstatus = llList2String(apilist, 0);
            string usersonline = llList2String(apilist, 1);
            string hgvisitors = llList2String(apilist, 2);
            string totalusers = llList2String(apilist, 3);
            string past24 = llList2String(apilist, 4);
            string past30local = llList2String(apilist, 5);
            string past30hg = llList2String(apilist, 6);
            string moston = llList2String(apilist, 7);
            string totalregions = llList2String(apilist, 8);
            string totallegacyregions = llList2String(apilist, 9);
            string total256 = llList2String(apilist, 10);
            string totalvar = llList2String(apilist, 11);
            string totalsize = llList2String(apilist, 12);
            string text = "However you want to display above onto a prim as a texture";
            string CommandList = "";
            CommandList = osMovePen(CommandList, 10, 10);
            CommandList = osDrawText(CommandList, text);
            osSetDynamicTextureData("", "vector", CommandList, "width:256,height:256", 0);
        }
    }
    timer() {
        httper = llHTTPRequest(url, [HTTP_METHOD, "GET"], "");
    }
}
		</textarea>
	</p>
<?php
	} // ends if ($zw->grid->isAdmin($user_uuid))
}
include ('../inc/footer.php');
?>