<?php
define('ZW_IN_SYSTEM', true);
require_once('../inc/headerless.php');

$api = $zw->Security->make_safe($_GET['api']);
if ($api) {
	$isenroll = true;
	$offset_result = $zw->SQL->query("SELECT FLOOR(RAND() * COUNT(*)) AS `offset` FROM `{$zw->config['group_db']}`.os_groups_groups WHERE OpenEnrollment = '$isenroll'");
	$offset_row = $zw->SQL->fetch_object($offset_result);
	$offset = $offset_row->offset;
	$fapiarray = "";
	$lslapiarray1 = "";
	$q = $zw->SQL->query("SELECT * FROM `{$zw->config['group_db']}`.os_groups_groups WHERE OpenEnrollment = '$isenroll' LIMIT $offset,1");
	while ($r = $zw->SQL->fetch_array($q)) {
		$GroupID = $r['GroupID'];
		$Name = $r['Name'];
		$Texture = $r['InsigniaID'];
		$fapiarray[] = array('GroupID' => $GroupID, 'Name' => $Name, 'Texture' =>$Texture);
		$lslapiarray1 .= $GroupID."=".$Name."=".$Texture."|";
	}
	$apiarray = array('GroupList' => $fapiarray);
	if ($api == "json") {
		echo json_encode($apiarray);
	}
	if ($api == "xml") {
		echo xmlrpc_encode($apiarray);
	}
	if ($api == "lsl") {
		$lslapiarray1 .= "~~";
		$lslapiarray = str_replace("|~~", "", $lslapiarray1);
		echo $lslapiarray;
	}
}else{
	echo "ERROR: Unknown!";
}
?>