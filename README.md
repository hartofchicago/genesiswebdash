ZetamexWeb - BETA 1.3 CLOTHS!
==========
1.3
- CLOTHS! Yes i finally fixed the long awaited bug.
Those who register can choose female or male and the system will copy that set avatar's outfit to the new user.
Example: I put some nice cloths on Female Avi and put her name in the zmw settings under Default Female,
John Sheppard comes along and selects Female for a default avi look, logs in and is wearing the same outfit, including shape, as Female Avi.
Will work on this more to display the Default avi's profile pic so new comers can see what the cloths look like.
- Also did alot of other work since 1.2.4 so please refer to the commit comments for more details.
- Database SQL has changed slightly, renamed MoneyIP to just Money and its a true or false option now.
Tim and I notice that DSL/NSL removed the required IP address to UUID's of avatars, they are just now UUID's so did changes to support this.
1.2
- TICKETS. This is a basic setup right now but should work simular as FreshDesk i hope.
- I lost track with everything else so please refer to my commit comments up to 1.1.
- 1.2.1 - Added a number notification of new tickets and replies to both user and admin
- 1.2.1 - Added a expiry time of 48 hours (2 days) for reset request codes.
- 1.2.1a - Created a in world server as a new idea for better communications between ZMW and your grid.
- 1.2.2 - Alot of touchups, fixes and some extra new features. Now with Money support.
- 1.2.3 - Added support for BitCoin for in world currency buying. Made the admin settings more orangized.
- 1.2.4 - Reworked the login to be FirstName and LastName in their own textbox.
-- Touched up tickets, money, the api, and afew others.
-- Dropped OAuth again because i just cant figure that one out but it is needed for HipChat.
-- Cleaned up the createlook function in Avatar.class.php but still not used or tested. Test at ur own risk.
First sample of this is with the new ticket system by alerting all admins of a new ticket.
LSL and SQL field for the settings table are included.
1.1
- New api system. Dropping support of OAuth2 and creating my own system.
(API system is still a Work In Progress).
- Generate a api token from the User Control Panel (UCP).
- Created a friendslist.php page that takes full advantage of the new api system.
- Can now edit some in world profile stuff from the UCP.
- 1.0.1 addressed some issues with resetting password. Now griefers cant reset other's passwords.
Users are sent a email to confirm the reset first before their avatar's password is reset.
- welcome.php and other status display pages got some touch ups to further more avoid the "your faking numbers" complaints.
- More support money modules.
- Gave users ability to select their own site style from the styles available to the website.
This allows users to use a different style then what the website is set to.
- This update does have alot of database changes so please compare with the prevous zmw.sql to see what is different.
1.0
- Yes ZetamexWeb is now in beta.
- Registration is done but does not yet support avatar customization.
- Forgot password and change password also now working.

==========

Website cms for Opensim

ZMW will be a Content Management System website for Opensim grids.

Packed with opensim website goodies.

Those goodies are be... (Except for those with Not Yet Developed or NYD)

Register

Login with avatar name

Forum <- Not Yet Developed

Friends list

Group list

Chat with in world friends and groups from this site (hopefully) <- Not Yet Developed

web search - search for anything in world with ease

profiles - edit your in world profile right from the website

Admin area to help better manage their grid...
Console <- NYD

Easily create regions and set their prim and agent limits <- NYD

Kick / Ban users <- NYD

Give money to users - API system with paypal integration will hopefully be available <- Hookup with paypal not done yet

More ideas coming soon, thats just what i have thought of for now.
