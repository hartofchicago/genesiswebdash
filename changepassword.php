<?php
$page_title = "Change Password";
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');

if ($user_uuid) {
$currentpass = $zw->Security->make_safe($_POST['currentpass']);
$newpass = $zw->Security->make_safe($_POST['newpass']);
$confirmpass = $zw->Security->make_safe($_POST['confirmpass']);
$submit = $zw->Security->make_safe($_POST['submit']);
  if ($submit == "Change Password") {
    if ($currentpass && $newpass && $confirmpass) {
      if ($zw->Users->changepassword($currentpass, $newpass, $confirmpass)) {
        echo $zw->site->displayalert("Password has been changed.", "success");
      }else{
        echo $zw->site->displayalert("Unable to change password. Please see a grid admin for a reset.", "danger");
      }
    }
  }
?>
<h3>Change Password</h3>
<form class="form-horizontal" method="post" action="" role="form">
  <div class="form-group">
    <label for="inputCurrentPassword" class="col-sm-2 control-label">Current Password</label>
    <div class="col-sm-10">
      <input type="password" name="currentpass" id="inputCurrentPassword" class="form-control" placeholder="Current Password">
    </div>
  </div>
  <div class="form-group">
    <label for="inputNewPassword" class="col-sm-2 control-label">New Password</label>
    <div class="col-sm-10">
      <input type="password" name="newpass" id="inputNewPassword" class="form-control" placeholder="New Password">
    </div>
  </div>
  <div class="form-group">
    <label for="inputConfirmNewPassword" class="col-sm-2 control-label">Confirm New Password</label>
    <div class="col-sm-10">
      <input type="password" name="confirmpass" id="inputConfirmNewPassword" class="form-control" placeholder="Confirm New Password">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-10">
      <input type="submit" name="submit" value="Change Password" class="btn btn-success">
    </div>
  </div>
</form>
<?php
}
include ('inc/footer.php');
?>