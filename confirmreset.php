<?php
$page_title = "Confirm Password Reset";
$hide_sidebars = true;
define('ZW_IN_SYSTEM', true);
require_once('inc/header.php');

$reset = $zw->Security->make_safe($_GET['reset']);
$uuid = $zw->Security->make_safe($_GET['uuid']);
$now = time();
if (!$user_uuid) {
	if ($reset && $uuid) {
		$checkcodeq = $zw->SQL->query("SELECT * FROM `{$zw->config['db_prefix']}resetcode` WHERE uuid = '$uuid' AND code = '$reset'");
		$checkcoden = $zw->SQL->num_rows($checkcodeq);
		if ($checkcoden) {
			$checkcoder = $zw->SQL->fetch_array($checkcodeq);
			$expiry = $checkcoder['expiry'];
			if ($now <= $expiry) {
				$zw->SQL->query("DELETE FROM `{$zw->config['db_prefix']}resetcode` WHERE uuid = '$uuid' AND code = '$reset'");
				if ($zw->Users->resetpass($uuid)) {
					$getuser = $zw->grid->getosuser_by_uuid($uuid);
					$emailaddy = $getuser['Email'];
					echo $zw->site->displayalert("Password has been reset. Please check your email address (".$emailaddy.") for your new temporary password", "success");
				}else{
					echo $zw->site->displayalert("Unable to reset password. Please see a grid admin for a reset.", "danger");
				}
			}else if ($now >= $expiry) {
				echo $zw->site->displayalert("Your code has expired. Please request a new reset.", "danger");
			}
		}else{
			echo $zw->site->displayalert("Invalid code and/or account id.", "danger");
		}
	}else{ // else if ($reset && $uuid)
		echo $zw->site->displayalert("Invalid response.", "danger");
	} // end if ($reset && $uuid)
}else if ($user_uuid) {
	echo $zw->site->displayalert("You are already logged in. Please logout to reset your password or use the User Control Panel to change your password.", "danger");
}
include ('inc/footer.php');
?>