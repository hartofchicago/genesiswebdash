<?php
define('ZW_IN_SYSTEM', true);
require_once('inc/headerless.php');
$HipChatToken = $zw->config['HipChatToken'];
$publicroomid = $zw->config['HipChatRoomID'];
$privateroomid = $zw->config['HipChatPrivateRoomID'];
$supportnames = $zw->config['HipChatSupportNames'];

$GridName = $zw->config['GridName'];
$cookieexplode = explode(".", $zw->config['cookie_domain']);
$domkey = $cookieexplode[1].".".$cookieexplode[0].".zmw";
?>
{
  "name": "<?php echo $GridName; ?> ZMW",
  "description": "A web CMS for Opensim",
  "key": "<?php echo $domkey; ?>",
  "links": {
    "homepage": "<?php echo $site_address; ?>",
    "self": "<?php echo $site_address; ?>/hipchatcapabilities.php"
  },
  "capabilities": {
    "hipchatApiConsumer": {
      "scopes": [
        "send_notification"
      ]
    }
  }
}